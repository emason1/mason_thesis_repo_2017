#! /usr/bin/python

import numpy as np


base_dir = '/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/'
lib_dir = '/home/lizzi/workspace/wells_lab/2012_library'


#   Read in expression data
#   Build dictionary of average probe-level expression

control_expression_filename = open (base_dir+'output/normalisation/data/iPS_fib_expression_data.txt', 'r')

head = control_expression_filename.readline()
head = head.strip()
head = head.split()
head.insert(0, "gene")

control_expression = {}

line = control_expression_filename.readline()
while line:
    line = line.strip()
    line = line.split()
    key = line[0]
    expr = line[1:len(line)]
    value = [float(x) for x in expr]
    control_expression[key] = value
    line = control_expression_filename.readline()

#   Build dictionary of fold change values (between Fib and iPS for pluripotency)

fold_change_filename = open (base_dir+'input/differential_expression/fibvsiPSCom.txt', 'r')

header = fold_change_filename.readline()
header = header.strip()
header = header.split()

fc_dict = {}

line = fold_change_filename.readline()
while line:
    line = line.split('\t')
    key = line[1]
    value = line[5]
    fc_dict[key] = value
    line = fold_change_filename.readline()

#   CREATE MAPPING DICTIONARY

mapping_filename = open (lib_dir+'/HT12_probe_to_gene_ens63_50bp_matches_only/HT12_mapping_file.txt', 'r')
mapping_file = mapping_filename.readlines()

mapping = []
for line in mapping_file:
	line = line.strip('\n')
	line = line.split('\t')
	mapping.append(line)

mapping_filename = open (lib_dir+'/HT12_probe_to_gene_ens63_50bp_matches_only/HT12_mapping_file.txt', 'r')

probe_sets_dict = {}

line = mapping_filename.readline()

while line:
    line = line.strip()
    line = line.split()
    key = line[2]
    probe = line[0]
    if probe in control_expression:
        if key not in probe_sets_dict:
            probe_sets_dict[key] = [probe]
        else:
            if probe not in probe_sets_dict[key]:
                probe_sets_dict[key].append(probe)
    
    line = mapping_filename.readline()

#   Map the expression file

mapped_data = {}
aberrant_probes = []
one_to_one = 0
one_to_two = 0


for i in control_expression:
    found = False
    for j in mapping:
        if i == j[0] and j[2] not in mapped_data:
            found = True
            ps = probe_sets_dict[j[2]]
            
            if len(ps) == 1:
                key = j[2]
                data = control_expression[i]
                mapped_data[key] = data
                one_to_one = one_to_one + 1
			
            elif len(ps) == 2:
                p1 = ps[0]
                p2 = ps[1]
                m1 = np.median(control_expression[p1])
                m2 = np.median(control_expression[p2])
                if m1 >= m2:
                    key = j[2]
                    data = control_expression[p1]
                    mapped_data[key] = data
                    one_to_two = one_to_two + 1
                else:
                    key = j[2]
                    data = control_expression[p2]
                    mapped_data[key] = data
                    one_to_two = one_to_two + 1
            
            elif len(ps) >= 3:
                fc = []
                for probe in ps:
                    fc.append(abs(float(fc_dict[probe])))
                m = max(fc)
                p = fc.index(m)
                r = ps[p]
                key = j[2]
                data = control_expression[r]
                mapped_data[key] = data
            elif found == False:
                aberrant_probes.append(i)

                
f = open(base_dir+"output/mapped_expression_data/mapped_expression_dataset.csv", "w")

for i in mapped_data.items():
    gene = i[0]
    expression = i[1]
    f.write("%s,%s\n" % (gene, expression))

f = open(base_dir+"output/mapped_expression_data/header.csv", "w")

for i in head:
    f.write("%s\n" % (i))

f = open(base_dir+"output/mapped_expression_data/unmapped_probe_sets.csv", "w")

for i in aberrant_probes:
    probe_set = i
    f.write("%s\n" % (probe_set))
