#! /usr/bin/Rscript

library(stats)

base_dir <- '/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/'
input_dir <- paste(base_dir,'output/mapped_expression_data/',sep="")
output_dir <- paste(base_dir,'output/',sep="")

dat <- paste(input_dir,'mapped_expression_dataset.csv',sep="")
cols <- paste(input_dir,'header.csv',sep="")

val.dat <- paste(input_dir,'mapped_expression_validation_dataset.csv',sep="")
val.cols <- paste(input_dir,'header_validation.csv',sep="")

ips.val.dat <- '/home/lizzi/workspace/wells_lab/analysis/2012_12_vitale_validation/output/mapped_expression_data/IPS_mapped_expression_dataset.csv'
ips.val.cols <- '/home/lizzi/workspace/wells_lab/analysis/2012_12_vitale_validation/output/mapped_expression_data/IPS_header.csv'

hes.val.dat <- '/home/lizzi/workspace/wells_lab/analysis/2012_12_vitale_validation/output/mapped_expression_data/HES_mapped_expression_dataset.csv'
hes.val.cols <- '/home/lizzi/workspace/wells_lab/analysis/2012_12_vitale_validation/output/mapped_expression_data/HES_header.csv'

plurinet.file <- '/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/input/pathways/plurinet_degree.txt'

bind <- paste(base_dir,'input/pathways/my_bind_network.csv',sep="")
bind_clique <- paste(base_dir,'input/pathways/bind_clique.txt',sep="")

#   Load files

expression_data <- read.table(dat, header = F, sep = ",")
cnames <- read.table(cols, header = F, sep = ",")
cnames <- as.character(cnames$V1)
colnames(expression_data) <- cnames
rnames <- as.character(expression_data$gene)
rownames(expression_data) <- rnames

if ( identical(rnames, as.character(rownames(expression_data))) ) {
    expression_data <- expression_data[,-1]
}

val_expression_data <- read.table(val.dat, header = F, sep = ",")
cnames <- read.table(val.cols, header = F, sep = ",")
cnames <- as.character(cnames$V1)
colnames(val_expression_data) <- cnames
rnames <- as.character(val_expression_data$gene)
rownames(val_expression_data) <- rnames

if ( identical(rnames, as.character(rownames(val_expression_data))) ) {
    val_expression_data <- val_expression_data[,-1]
}


ips_data <- read.table(ips.val.dat, header = F, sep = ",")
cnames <- read.table(ips.val.cols, header = F, sep = ",")
cnames <- as.character(cnames$V1)
colnames(ips_data) <- cnames
rnames <- as.character(ips_data$gene)
rownames(ips_data) <- rnames

if ( identical(rnames, as.character(rownames(ips_data))) ) {
    ips_data <- ips_data[,-1]
}

hes_data <- read.table(hes.val.dat, header = F, sep = ",")
cnames <- read.table(hes.val.cols, header = F, sep = ",")
cnames <- as.character(cnames$V1)
colnames(hes_data) <- cnames
rnames <- as.character(hes_data$gene)
rownames(hes_data) <- rnames

if ( identical(rnames, as.character(rownames(hes_data))) ) {
    hes_data <- hes_data[,-1]
}


############################################################################
#   NETWORK CONSTRUCTION

correlation.matrix <- t(expression_data)
correlation <- cor(correlation.matrix, method = 'pearson')

matrixFromList <- function(listX) t(sapply(listX, function(x, n) c(x, rep(NA, n))[1:n], n = max(sapply(listX, length)))) 
cnames <- c("GENE_A", "GENE_B", "PEARSON", "EDGE_TYPE")

#   These cutoffs discover a network with 2600 edges
cutoff <- 0.995
neg.cutoff <- -0.995
temp <- list()
cols <- colnames(correlation)
rows <- rownames(correlation)

for ( i in 1:nrow(correlation) ) {
    for ( j in 1:ncol(correlation) ) {
        if ( i != j ) {
            if ( correlation[i, j] >= cutoff ) {
                temp[[length(temp) + 1]]= c(rows[i], cols[j], correlation[i,j], "pos")
            } else { if ( correlation[i, j] <= neg.cutoff ) {
                temp[[length(temp) + 1]]= c(rows[i], cols[j], correlation[i,j], "neg")
                }
			}
        }
	}
}

#   Format the network according to Cytoscape
network_1_with_duplicates <- matrixFromList(temp)
network_1_nodes <- t(apply(network_1_with_duplicates[,1:2], 1, sort))
network_1 <- unique(cbind(network_1_nodes, network_1_with_duplicates[,3:4]))
colnames(network_1) <- cnames

network_nodes <- sort(unique(as.character(network_1[,1:2])))

write.table(network_1, file=paste(output_dir,'cytoscape_input_files/correlation_network_ens63.txt', sep=""),  quote = FALSE, row.names=FALSE, col.names=TRUE, sep="\t")
write.table(network_nodes, file=paste(output_dir,'cytoscape_input_files/correlation_network_node_list_ens63.txt', sep=""),  quote = FALSE, row.names=FALSE, col.names=TRUE, sep="\t")



########################################################################
# Process the datasets
########################################################################

#   Calculate mean and coefficient of variation for the Briggs iPS dataset

co.var <- function(x)(100*sd(x)/mean(x))

ips <- expression_data[,13:30]
ips.iso <- ips[,c("CiPSC1", "CiPSC2", "CiPSC3", "CiPSC10", "CiPSC11", "CiPSC12", "CiPSC13", "CiPSC14", "CiPSC15")]

ips$coVar <- apply(ips, 1, co.var)
ips.covar <- ips$coVar
ips$Mean <- apply(ips[,1:18], 1, mean)
ips.mean <- ips$Mean
ips.exprs <- ips

ips.iso$coVar <- apply(ips.iso, 1, co.var)
ips.iso.covar <- ips.iso$coVar
ips.iso$Mean <- apply(ips.iso[,1:9], 1, mean)
ips.iso.mean <- ips.iso$Mean
ips.iso.exprs <- ips.iso

reorder.by.network <- as.character(network_nodes)

ips <- ips[rownames(ips) %in% network_nodes,]
ips <- ips[reorder.by.network,]

ips.iso <- ips.iso[rownames(ips.iso) %in% network_nodes,]
ips.iso <- ips.iso[reorder.by.network,]

#   Input the BIND network, degree of connectivity, covar and mean

bind_data <- read.table(bind, header = T, sep = "\t")
rnames <- as.character(bind_data$Bind_Gene)
rownames(bind_data) <- rnames
reorder.bind <- rnames

bind_ips <- ips[rownames(ips) %in% rnames,]
bind_ips <- bind_ips[reorder.bind,]

b <- cbind(bind_data, bind_ips$coVar, bind_ips$Mean)
cnames <- c("Bind_Degree", "Bind_coVar", "Bind_Mean")
colnames(b) <- cnames

if ( identical(rownames(b), as.character(b$Bind_Gene)) ) {
    b <- b[,-1]
}
b <- b[rowSums(is.na(b))==0, ]


#   Calculate mean and coefficient of variation in the Vitale datasets

ips_low <- ips_data[,6:9]
ips_low$coVar <- apply(ips_low, 1, co.var)
ips_low.covar <- ips_low$coVar
ips_low$Mean <- apply(ips_low[,1:4], 1, mean)
ips.low.mean <- ips_low$Mean
ips.low.exprs <- ips_low
ips_low <- ips_low[rownames(ips_low) %in% network_nodes,]
ips_low <- ips_low[reorder.by.network,]
ips_low <- ips_low[rowSums(is.na(ips_low))==0, ]

ips_high <- ips_data[,1:5]
ips_high$coVar <- apply(ips_high, 1, co.var)
ips_high.covar <- ips_high$coVar
ips_high$Mean <- apply(ips_high[,1:5], 1, mean)
ips.high.mean <- ips_high$Mean
ips.high.exprs <- ips_high
ips_high <- ips_high[rownames(ips_high) %in% network_nodes,]
ips_high <- ips_high[reorder.by.network,]
ips_high <- ips_high[rowSums(is.na(ips_high))==0, ]


hes_data$coVar <- apply(hes_data, 1, co.var)
hes_covar <- hes_data$coVar
hes_data$Mean <- apply(hes_data[,1:3], 1, mean)
hes_Mean <- hes_data$Mean
hes.exprs <- hes_data
hes_data <- hes_data[rownames(hes_data) %in% network_nodes,]
hes_data <- hes_data[reorder.by.network,]
hes_data <- hes_data[rowSums(is.na(hes_data))==0, ]


#   Calculate mean and cov for P-fractions validation datasets
p67.exprs <- val_expression_data[,7:12]
p67.exprs$coVar <-apply(p67.exprs, 1, co.var)
p67.exprs.coVar <- p67.exprs$coVar
p67.exprs$Mean <- apply(p67.exprs[,1:6], 1, mean)
p67.exprs.mean <- p67.exprs$Mean

p7 <- p67.exprs[,4:6]
p7$coVar <- apply(p7, 1, co.var)
p7$Mean <- apply(p7[,1:3], 1, mean)
p7 <- p7[order(p7$coVar),]

p4 <- val_expression_data[,1:3]
p4$coVar <- apply(p4, 1, co.var)
p4$Mean <- apply(p4[,1:3], 1, mean)
p4 <- p4[order(p4$coVar),]

p5 <- val_expression_data[,4:6]
p5$coVar <- apply(p5, 1, co.var)
p5$Mean <- apply(p5[,1:3], 1, mean)
p5 <- p5[order(p5$coVar),]

p6 <- val_expression_data[,7:9]
p6$coVar <- apply(p6, 1, co.var)
p6$Mean <- apply(p6[,1:3], 1, mean)
p6 <- p6[order(p6$coVar),]

all_pfract <- val_expression_data
all_pfract$coVar <- apply(all_pfract, 1, co.var)
all_pfract$Mean <- apply(all_pfract[,1:12], 1, mean)

hes <- val_expression_data[rownames(val_expression_data) %in% network_nodes,]

hes.p45 <- hes[,1:6]
hes.p45$coVar <-apply(hes.p45, 1, co.var)
hes.p45.coVar <- hes.p45$coVar
hes.p45$Mean <- apply(hes.p45[,1:6], 1, mean)
hes.p45.mean <- hes.p45$Mean

hes.p67 <- hes[,7:12]
hes.p67$coVar <-apply(hes.p67, 1, co.var)
hes.p67.coVar <- hes.p67$coVar
hes.p67$Mean <- apply(hes.p67[,1:6], 1, mean)
hes.p67.mean <- hes.p67$Mean


p45 <- hes[,1:6]
p67 <- hes[,7:12]

p45$coVar <- apply(p45, 1, co.var)
p45 <- p45[reorder.by.network,]
p45 <- p45[rowSums(is.na(p45))==0, ]

p67$coVar <- apply(p67, 1, co.var)
p67 <- p67[reorder.by.network,]
p67 <- p67[rowSums(is.na(p67))==0, ]

########################################################################
#   Density plots and boxplots for all probes mean and variances
########################################################################

pretty.col <- rainbow(10)

#   All probes CoV
postscript(file=paste(output_dir,"network_analysis/cov_analysis/box_plot_all_datasets_all_probes_CoV.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(ips.low.exprs$coVar, ips.high.exprs$coVar, ips.exprs$coVar, ips.iso.exprs$coVar, hes.exprs$coVar, all_pfract$coVar, col=c(pretty.col), names=c("iPS low\n (Vitale)", "iPS_high\n (Vitale)", "iPS\n (Briggs)", "iPS\n (isogenic)", "hES\n (Vitale)", "hES P-fractions\n (Hough)"), ylim=c(0,85), ylab="Coefficient of variation", xlab="Dataset", main="Coefficient of variation for all probes\n in each dataset")
dev.off()


#   P fractions Mean
postscript(file=paste(output_dir,"network_analysis/mean_analysis/box_plot_p_fractions_all_probes_mean.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(all_pfract$Mean, p4$Mean, p5$Mean, p6$Mean, p7$Mean, col=c("#003380", "#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"), names=c("hES all samples\n (Hough)", "hES P4\n (Hough)", "hES P5\n (Hough)", "hES P6\n (Hough)", "hES P7\n (Hough)"), ylim=c(0,20), ylab="Coefficient of variation", xlab="Dataset", main="Mean Log(2) expression for all probes\n in P-fractions dataset")
dev.off()

#   P fractions CoV
postscript(file=paste(output_dir,"network_analysis/cov_analysis/box_plot_p_fractions_all_probes_CoV.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(all_pfract$coVar, p4$coVar, p5$coVar, p6$coVar, p7$coVar, col=c("#003380", "#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"), names=c("hES all samples\n (Hough)", "hES P4\n (Hough)", "hES P5\n (Hough)", "hES P6\n (Hough)", "hES P7\n (Hough)"), ylim=c(0,20), ylab="Coefficient of variation", xlab="Dataset", main="Coefficient of variation for all probes\n in P-fractions dataset")
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/density_plot_p_fractions_all_probes_CoV.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
plot(density(log2(all_pfract$coVar)), main="Variance reflects homogeneity of a population", ylim=range(0, 0.5), xlab="Log 2 CoV", ylab="Density", lty=1, lwd=4, col="#003380")
    lines(density(log2(p4$coVar)), lty=1, lwd=4, col='#0055D4')
    lines(density(log2(p5$coVar)), lty=1, lwd=4, col='#5599FF')
    lines(density(log2(p6$coVar)), lty=1, lwd=4, col='#AACCFF')
    lines(density(log2(p7$coVar)), lty=1, lwd=4, col='#D7E3F4')
    legend(x=3, y=0.4, c("All samples", "P4", "P5", "P6", "P7"), lty=c(1, 1, 1, 1, 1), lwd=(4), col=c("#003380", "#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"), text.col=c("#003380", "#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()


postscript(file=paste(output_dir,"network_analysis/cov_analysis/box_plot_p_fractions_network_nodes_CoV.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(p4[rownames(p4) %in% network_nodes,"coVar"], p5[rownames(p5) %in% network_nodes,"coVar"], p6[rownames(p6) %in% network_nodes,"coVar"], p7[rownames(p7) %in% network_nodes,"coVar"], all_pfract[rownames(all_pfract) %in% network_nodes,"coVar"], col=c(pretty.col), names=c("hES P4\n (Hough)", "hES P5\n (Hough)", "hES P6\n (Hough)", "hES P7\n (Hough)", "hES all samples\n (Hough)"), ylim=c(0,20), ylab="Coefficient of variation", xlab="Dataset", main="Coefficient of variation for network nodes\n in P-fractions dataset")
dev.off()

#   All probes mean
postscript(file=paste(output_dir,"network_analysis/mean_analysis/box_plot_all_datasets_all_probes_Mean.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(ips.low.exprs$Mean, ips.high.exprs$Mean, ips.exprs$Mean, ips.iso.exprs$Mean, hes.exprs$Mean, all_pfract$Mean, col=c(pretty.col), names=c("iPS low\n (Vitale)", "iPS_high\n (Vitale)", "iPS\n (Briggs)", "iPS\n (isogenic)", "hES\n (Vitale)", "hES P-fractions\n (Hough)"), ylim=c(0,25), ylab="Mean log2 expression", xlab="Dataset", main="Mean expression of all probes\n in each dataset")
dev.off()


########################################################################
#   Calculate the degree of connectivity in the co-expression network per network node in each dataset
########################################################################

all_nodes_duplicates <- sort(as.character(network_1[,1:2]))

nodes_degree <- vector()

for ( i in network_nodes ){
    degree <- 0
    for ( j in all_nodes_duplicates ){
        if ( i == j ){
            degree <- degree + 1
        }
    }
    nodes_degree <- c(nodes_degree, degree)
}

ips$Degree <- nodes_degree
ips.iso$Degree <- nodes_degree

degree_of_connectivity <- matrixFromList(nodes_degree)

hes$Degree <- ips[rownames(ips) %in% rownames(hes),"Degree"]
p45$Degree <- ips[rownames(ips) %in% rownames(p45),"Degree"]
p67$Degree <- ips[rownames(ips) %in% rownames(p67),"Degree"]
hes_data$Degree <- ips[rownames(ips) %in% rownames(hes_data),"Degree"]
ips_low$Degree <- ips[rownames(ips) %in% rownames(ips_low),"Degree"]
ips_high$Degree <- ips[rownames(ips) %in% rownames(ips_high),"Degree"]

#   Order data by the coVar column

ips <- ips[order(ips$coVar),]
ips.iso <- ips.iso[order(ips.iso$coVar),]
p45 <- p45[order(p45$coVar),]
p67 <- p67[order(p67$coVar),]
ips_low <- ips_low[order(ips_low$coVar),]
ips_high <- ips_high[order(ips_high$coVar),]
hes_data <- hes_data[order(hes_data$coVar),]

########################################################################
#   Test cov/degree relationship in Plurinet as independent PPI network
########################################################################

#   For Briggs iPS data
plurinet_degree <- read.table(plurinet.file, header = T, sep = "\t")
rownamespluri <- as.character(plurinet_degree$ID)
rownames(plurinet_degree) <- rownamespluri
plurinet_exprs_data <- ips.exprs[rownames(ips.exprs) %in% plurinet_degree$ID,]
plurinet_degree <- plurinet_degree[rownames(plurinet_degree) %in% rownames(plurinet_exprs_data),]
plurinet_degree <- plurinet_degree[rownames(plurinet_exprs_data),]
plurinet_degree <- cbind(plurinet_degree, plurinet_exprs_data$Mean, plurinet_exprs_data$coVar)
colnames(plurinet_degree) <- c("ID", "Degree", "Mean", "coVar")
plurinet_degree <- plurinet_degree[order(plurinet_degree$coVar, -(plurinet_degree$Degree)),]

qCoV <- quantile(plurinet_degree$coVar)
density.CoV <- density(plurinet_degree$coVar)

low.line <-  1.8230398
mid.line <-  4.2639292 

postscript(file=paste(output_dir,"network_analysis/cov_analysis/gaussian_cov_plurinet_iPS.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density.CoV, main="CoV density plot for Plurinet\n in iPS (Briggs) dataset", xlab="CoV", ylab="Density", lty=1, lwd=4, col="black")
		abline(v = low.line, col = "red", lty=1, lwd=4)
		abline(v = mid.line, col = "red", lty=1, lwd=4)
	dev.off()

pluri.low.var <- plurinet_degree[1:64,]
pluri.mid.var <- plurinet_degree[65:191,]
pluri.high.var <- plurinet_degree[192:255,]

write.table(pluri.low.var, file=paste(output_dir,'ontology_analysis/low_var_plurinet_iPS_Briggs.txt', sep=""),  quote = FALSE, sep="\t")
write.table(pluri.high.var, file=paste(output_dir,'ontology_analysis/high_var_plurinet_iPS_Briggs.txt', sep=""),  quote = FALSE, sep="\t")


#   Prepare the density distributions for each variance percentile
#   A Gaussian kernel density estimator (density function in R using Gaussian method to select bandwidth) was used to produce degree distribution density plots
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Density_plot_plurinet_covar_degree_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(pluri.low.var$Degree), main="Plurinet degree density plots for variance bins\n in iPS dataset", ylim=range(c(0, 0.3)), xlab="Degree", ylab="Density", lty=1, lwd=4, col="red")
		lines(density(pluri.mid.var$Degree), lty=1, lwd=4, col="gray")
        lines(density(pluri.high.var$Degree), lty=1, lwd=4, col="deepskyblue")
        legend(x=20, y=0.27, c("Low Variance", "Mid Variance", "High Variance"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
	dev.off()

#   Use a non-parametric Kolmogorov-Smirnov test used to test for significant differences between degree distributions
test <- ks.test(pluri.low.var$Degree, pluri.high.var$Degree)
#   D = 0.2031, p-value = 0.1426

#   use a t-test to test for significant differences between high and low variance groups
degree.t <- t.test(pluri.low.var$Degree, pluri.high.var$Degree, var.equal=TRUE)
#   t = 1.5701, df = 126, p-value = 0.1189

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_plurinet_covar_degree_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(pluri.low.var$Degree, pluri.mid.var$Degree, pluri.high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Plurinet degree box plots for variance bins\n in iPS dataset", xlab="CoV percentile", ylab="Degree")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
pluri.wilcox.test <- wilcox.test(pluri.low.var$Degree, pluri.high.var$Degree)
#  p.val = 0.0130409


#   Sub-cellular localisation
cytoplasmic <- read.table("/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/input/pathways/pluri_cytoplasmic.csv", header = F, sep = "\t")
nucleus <- read.table("/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/input/pathways/pluri_nucleus.csv", header = F, sep = "\t")

colnames(cytoplasmic) <- c("Gene", "Location")
colnames(nucleus) <- c("Gene", "Location")


cyto <- as.character(cytoplasmic[,"Gene"])
nuc <- as.character(nucleus[,"Gene"])

loc <- rbind(cytoplasmic, nucleus)
rownames(loc) <- loc[,"Gene"]

loc <- loc[rownames(loc) %in% rownames(plurinet_degree),]
loc.order <- as.character(rownames(loc))
pluri.loc <- plurinet_degree[loc.order,]
loc.matrix <- cbind(pluri.loc, loc$Location)
colnames(loc.matrix) <- c("ID","Degree", "Mean", "coVar", "Location")

#   High and low var
# 0%         25% SAL4    50%        75% MYC
# 0.6758847  1.8085600  2.4926143  4.2777055 

loc.matrix <- loc.matrix[order(loc.matrix$coVar),]

low.var <- loc.matrix[1:59,]
mid.var <- loc.matrix[60:172,]
high.var <- loc.matrix[173:231,]

#    Observed and expected
C <- 0
N <- 0
for ( i in low.var$Location ){
	if ( i == "C" ){
		C <- C + 1
		}else{
			if ( i == "N" ){
				N <- N + 1
            }
		}
}
degree.freq.obs <- c(C, N)
degree.freq.exp <- c(0.5, 0.5)
goodness.of.fit.Degree <- chisq.test(x=degree.freq.obs, p=degree.freq.exp, correct=FALSE, rescale.p=FALSE, simulate.p.value=FALSE)
#   X-squared = 6.1186, df = 1, p-value = 0.01338

C <- 0
N <- 0
for ( i in high.var$Location ){
	if ( i == "C" ){
		C <- C + 1
		}else{
			if ( i == "N" ){
				N <- N + 1
            }
		}
}
degree.freq.obs <- c(C, N)
degree.freq.exp <- c(0.5, 0.5)
goodness.of.fit.Degree <- chisq.test(x=degree.freq.obs, p=degree.freq.exp, correct=FALSE, rescale.p=FALSE, simulate.p.value=FALSE)
#   X-squared = 3.8136, df = 1, p-value = 0.05084

pluri.cyto <- plurinet_degree[rownames(plurinet_degree) %in% cyto,]
pluri.nuc <- plurinet_degree[rownames(plurinet_degree) %in% nuc,]

localization.pluri <- t.test(pluri.cyto$coVar, pluri.nuc$coVar)
#   Welch's 2 sample T test (t = 1.7985, df = 155.514, p-value = 0.07404)
localization.pluri.wilcox.test <- wilcox.test(pluri.cyto$coVar, pluri.nuc$coVar)
#   W = 7074, p-value = 0.08093



#   For ISOGENIC Briggs iPS data
plurinet_degree <- read.table(plurinet.file, header = T, sep = "\t")
rownamespluri <- as.character(plurinet_degree$ID)
rownames(plurinet_degree) <- rownamespluri
plurinet_exprs_data <- ips.iso.exprs[rownames(ips.iso.exprs) %in% plurinet_degree$ID,]
plurinet_degree <- plurinet_degree[rownames(plurinet_degree) %in% rownames(plurinet_exprs_data),]
plurinet_degree <- plurinet_degree[rownames(plurinet_exprs_data),]
plurinet_degree <- cbind(plurinet_degree, plurinet_exprs_data$Mean, plurinet_exprs_data$coVar)
colnames(plurinet_degree) <- c("ID", "Degree", "Mean", "coVar")
plurinet_degree <- plurinet_degree[order(plurinet_degree$coVar, -(plurinet_degree$Degree)),]

qCoV <- quantile(plurinet_degree$coVar)
density.CoV <- density(plurinet_degree$coVar)

low.line <-  1.5951920
mid.line <-  4.2594338

postscript(file=paste(output_dir,"network_analysis/cov_analysis/gaussian_cov_plurinet_iPS_iso.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density.CoV, main="CoV density plot for Plurinet\n in isogenic iPS (Briggs) dataset", xlab="CoV", ylab="Density", lty=1, lwd=4, col="black")
		abline(v = low.line, col = "red", lty=1, lwd=4)
		abline(v = mid.line, col = "red", lty=1, lwd=4)
	dev.off()

pluri.low.var <- plurinet_degree[1:64,]
pluri.mid.var <- plurinet_degree[65:191,]
pluri.high.var <- plurinet_degree[192:255,]

write.table(pluri.low.var, file=paste(output_dir,'ontology_analysis/low_var_plurinet_iPS_iso_Briggs.txt', sep=""),  quote = FALSE, sep="\t")
write.table(pluri.high.var, file=paste(output_dir,'ontology_analysis/high_var_plurinet_iPS_iso_Briggs.txt', sep=""),  quote = FALSE, sep="\t")


#   Prepare the density distributions for each variance percentile
#   A Gaussian kernel density estimator (density function in R using Gaussian method to select bandwidth) was used to produce degree distribution density plots
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Density_plot_plurinet_covar_degree_ips_iso.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(pluri.low.var$Degree), main="Plurinet degree density plots for variance bins\n in isogenic iPS dataset", ylim=range(c(0, 0.3)), xlab="Degree", ylab="Density", lty=1, lwd=4, col="red")
		lines(density(pluri.mid.var$Degree), lty=1, lwd=4, col="gray")
        lines(density(pluri.high.var$Degree), lty=1, lwd=4, col="deepskyblue")
        legend(x=20, y=0.27, c("Low Variance", "Mid Variance", "High Variance"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
	dev.off()

#   Use a non-parametric Kolmogorov-Smirnov test used to test for significant differences between degree distributions
test <- ks.test(pluri.low.var$Degree, pluri.high.var$Degree)
#   D = 0.1875, p-value = 0.2106

#   use a t-test to test for significant differences between high and low variance groups
degree.t <- t.test(pluri.low.var$Degree, pluri.high.var$Degree, var.equal=TRUE)
#   t = 1.5008, df = 126, p-value = 0.1359

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_plurinet_covar_degree_ips_isogenic.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(pluri.low.var$Degree, pluri.mid.var$Degree, pluri.high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Plurinet degree box plots for variance bins\n in iPS dataset", xlab="CoV percentile", ylab="Degree")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
pluri.wilcox.test <- wilcox.test(pluri.low.var$Degree, pluri.high.var$Degree)
#  W = 2529, p-value = 0.01922


#   Sub-cellular localisation
cytoplasmic <- read.table("/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/input/pathways/pluri_cytoplasmic.csv", header = F, sep = "\t")
nucleus <- read.table("/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/input/pathways/pluri_nucleus.csv", header = F, sep = "\t")

colnames(cytoplasmic) <- c("Gene", "Location")
colnames(nucleus) <- c("Gene", "Location")


cyto <- as.character(cytoplasmic[,"Gene"])
nuc <- as.character(nucleus[,"Gene"])

loc <- rbind(cytoplasmic, nucleus)
rownames(loc) <- loc[,"Gene"]

loc <- loc[rownames(loc) %in% rownames(plurinet_degree),]
loc.order <- as.character(rownames(loc))
pluri.loc <- plurinet_degree[loc.order,]
loc.matrix <- cbind(pluri.loc, loc$Location)
colnames(loc.matrix) <- c("ID","Degree", "Mean", "coVar", "Location")

#   High and low var
# 0%         25% SAL4    50%        75% MYC
# 0.6758847  1.8085600  2.4926143  4.2777055 

loc.matrix <- loc.matrix[order(loc.matrix$coVar),]

low.var <- loc.matrix[1:59,]
mid.var <- loc.matrix[60:172,]
high.var <- loc.matrix[173:231,]

#    Observed and expected
C <- 0
N <- 0
for ( i in low.var$Location ){
	if ( i == "C" ){
		C <- C + 1
		}else{
			if ( i == "N" ){
				N <- N + 1
            }
		}
}
degree.freq.obs <- c(C, N)
degree.freq.exp <- c(0.5, 0.5)
goodness.of.fit.Degree <- chisq.test(x=degree.freq.obs, p=degree.freq.exp, correct=FALSE, rescale.p=FALSE, simulate.p.value=FALSE)
#   X-squared = 6.1186, df = 1, p-value = 0.01338

C <- 0
N <- 0
for ( i in high.var$Location ){
	if ( i == "C" ){
		C <- C + 1
		}else{
			if ( i == "N" ){
				N <- N + 1
            }
		}
}
degree.freq.obs <- c(C, N)
degree.freq.exp <- c(0.5, 0.5)
goodness.of.fit.Degree <- chisq.test(x=degree.freq.obs, p=degree.freq.exp, correct=FALSE, rescale.p=FALSE, simulate.p.value=FALSE)
#   X-squared = 3.8136, df = 1, p-value = 0.05084

pluri.cyto <- plurinet_degree[rownames(plurinet_degree) %in% cyto,]
pluri.nuc <- plurinet_degree[rownames(plurinet_degree) %in% nuc,]

localization.pluri <- t.test(pluri.cyto$coVar, pluri.nuc$coVar)
#   Welch's 2 sample T test (t = 1.7985, df = 155.514, p-value = 0.07404)
localization.pluri.wilcox.test <- wilcox.test(pluri.cyto$coVar, pluri.nuc$coVar)
#   W = 7074, p-value = 0.08093


#   For Vitale hES samples
plurinet_degree <- read.table(plurinet.file, header = T, sep = "\t")
rownamespluri <- as.character(plurinet_degree$ID)
rownames(plurinet_degree) <- rownamespluri
plurinet_exprs_data <- hes.exprs[rownames(hes.exprs) %in% plurinet_degree$ID,]
plurinet_degree <- plurinet_degree[rownames(plurinet_degree) %in% rownames(plurinet_exprs_data),]
plurinet_degree <- plurinet_degree[rownames(plurinet_exprs_data),]
plurinet_degree <- cbind(plurinet_degree, plurinet_exprs_data$Mean, plurinet_exprs_data$coVar)
colnames(plurinet_degree) <- c("ID", "Degree", "Mean", "coVar")
plurinet_degree <- plurinet_degree[order(plurinet_degree$coVar, -(plurinet_degree$Degree)),]

qCoV <- quantile(plurinet_degree$coVar)
density.CoV <- density(plurinet_degree$coVar)

low.line <-   0.90790529
mid.line <-  2.93624103

postscript(file=paste(output_dir,"network_analysis/cov_analysis/gaussian_cov_plurinet_hES.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density.CoV, main="CoV density plot for Plurinet\n in hES (Vitale) dataset", xlab="CoV", ylab="Density", lty=1, lwd=4, col="black")
		abline(v = low.line, col = "red", lty=1, lwd=4)
		abline(v = mid.line, col = "red", lty=1, lwd=4)
	dev.off()


pluri.low.var <- plurinet_degree[1:65,]
pluri.mid.var <- plurinet_degree[66:193,]
pluri.high.var <- plurinet_degree[194:258,]

#   Prepare the density distributions for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Density_plot_plurinet_covar_degree_hes.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(pluri.low.var$Degree), main="Plurinet degree density plots for variance bins\n in hES dataset", ylim=range(c(0, 0.3)), xlab="Degree", ylab="Density", lty=1, lwd=4, col="red")
		lines(density(pluri.mid.var$Degree), lty=1, lwd=4, col="gray")
        lines(density(pluri.high.var$Degree), lty=1, lwd=4, col="deepskyblue")
        legend(x=20, y=0.27, c("Low Variance", "Mid Variance", "High Variance"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
	dev.off()
#   Use a non-parametric Kolmogorov-Smirnov test used to test for significant differences between degree distributions

test <- ks.test(pluri.low.var$Degree, pluri.high.var$Degree,)
#   D = 0.2, p-value = 0.1485

degree.t <- t.test(pluri.low.var$Degree, pluri.high.var$Degree, var.equal=TRUE)
#   t = 2.2292, df = 128, p-value = 0.02754

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_plurinet_covar_degree_hes.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(pluri.low.var$Degree, pluri.mid.var$Degree, pluri.high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Plurinet degree box plots for variance bins\n in hES dataset", xlab="CoV percentile", ylab="Degree")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
pluri.wilcox.test <- wilcox.test(pluri.low.var$Degree, pluri.high.var$Degree)
#  p.val = 0.02793


pluri.cyto <- plurinet_degree[rownames(plurinet_degree) %in% cyto,]
pluri.nuc <- plurinet_degree[rownames(plurinet_degree) %in% nuc,]

localization.pluri <- t.test(pluri.cyto$coVar, pluri.nuc$coVar)
#   Welch 2 sample T test (t = -1.0334, df = 226.406, p-value = 0.3025)
localization.pluri.wilcox.test <- wilcox.test(pluri.cyto$coVar, pluri.nuc$coVar)
#   W = 6329, p-value = 0.8189


#   For Hough P67 samples
plurinet_degree <- read.table(plurinet.file, header = T, sep = "\t")
rownamespluri <- as.character(plurinet_degree$ID)
rownames(plurinet_degree) <- rownamespluri
plurinet_exprs_data <- p67.exprs[rownames(p67.exprs) %in% plurinet_degree$ID,]
plurinet_degree <- plurinet_degree[rownames(plurinet_degree) %in% rownames(plurinet_exprs_data),]
plurinet_degree <- plurinet_degree[rownames(plurinet_exprs_data),]
plurinet_degree <- cbind(plurinet_degree, plurinet_exprs_data$Mean, plurinet_exprs_data$coVar)
colnames(plurinet_degree) <- c("ID", "Degree", "Mean", "coVar")
plurinet_degree <- plurinet_degree[order(plurinet_degree$coVar, -(plurinet_degree$Degree)),]

pluri.low.var <- plurinet_degree[1:63,]
pluri.mid.var <- plurinet_degree[64:185,]
pluri.high.var <- plurinet_degree[186:249,]

#   Prepare the density distributions for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Density_plot_plurinet_covar_degree_p67.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(pluri.low.var$Degree), main="Plurinet degree density plots for variance bins\n in P67 dataset", ylim=range(c(0, 0.3)), xlab="Degree", ylab="Density", lty=1, lwd=4, col="red")
		lines(density(pluri.mid.var$Degree), lty=1, lwd=4, col="gray")
        lines(density(pluri.high.var$Degree), lty=1, lwd=4, col="deepskyblue")
        legend(x=10, y=0.27, c("Low Variance", "Mid Variance", "High Variance"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
	dev.off()
#   Use a non-parametric Kolmogorov-Smirnov test used to test for significant differences between degree distributions
test <- ks.test(pluri.low.var$Degree, pluri.high.var$Degree)
#   D = 0.1662, p-value = 0.3446

#   Independent samples T-test (equal variances)
degree.t <- t.test(pluri.low.var$Degree, pluri.high.var$Degree, var.equal=TRUE)
#   t = -0.1996, df = 125, p-value = 0.8421

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_plurinet_covar_degree_p67.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(pluri.low.var$Degree, pluri.mid.var$Degree, pluri.high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Plurinet degree box plots for variance bins\n in P67 dataset", xlab="CoV percentile", ylab="Degree")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
pluri.wilcox.test <- wilcox.test(pluri.low.var$Degree, pluri.high.var$Degree)
#  p.val = 0.1871

#   For Hough P7 samples
plurinet_degree <- read.table(plurinet.file, header = T, sep = "\t")
rownamespluri <- as.character(plurinet_degree$ID)
rownames(plurinet_degree) <- rownamespluri
plurinet_exprs_data <- p7[rownames(p7) %in% plurinet_degree$ID,]
plurinet_degree <- plurinet_degree[rownames(plurinet_degree) %in% rownames(plurinet_exprs_data),]
plurinet_degree <- plurinet_degree[rownames(plurinet_exprs_data),]
plurinet_degree <- cbind(plurinet_degree, plurinet_exprs_data$Mean, plurinet_exprs_data$coVar)
colnames(plurinet_degree) <- c("ID", "Degree", "Mean", "coVar")
plurinet_degree <- plurinet_degree[order(plurinet_degree$coVar, -(plurinet_degree$Degree)),]

pluri.low.var <- plurinet_degree[1:63,]
pluri.mid.var <- plurinet_degree[64:185,]
pluri.high.var <- plurinet_degree[186:249,]

write.table(pluri.low.var, file=paste(output_dir,'ontology_analysis/low_var_plurinet_P7_Hough.txt', sep=""),  quote = FALSE, sep="\t")
write.table(pluri.high.var, file=paste(output_dir,'ontology_analysis/high_var_plurinet_P7_Hough.txt', sep=""),  quote = FALSE, sep="\t")


#   Prepare the density distributions for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Density_plot_plurinet_covar_degree_p7.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(pluri.low.var$Degree), main="Plurinet degree density plots for variance bins\n in P7 dataset", ylim=range(c(0, 0.3)), xlab="Degree", ylab="Density", lty=1, lwd=4, col="red")
		lines(density(pluri.mid.var$Degree), lty=1, lwd=4, col="gray")
        lines(density(pluri.high.var$Degree), lty=1, lwd=4, col="deepskyblue")
        legend(x=10, y=0.27, c("Low Variance", "Mid Variance", "High Variance"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
	dev.off()
#   Use a non-parametric Kolmogorov-Smirnov test used to test for significant differences between degree distributions
test <- ks.test(pluri.low.var$Degree, pluri.high.var$Degree)
#   D = 0.0888, p-value = 0.9638

#   T-test
degree.t <- t.test(pluri.low.var$Degree, pluri.high.var$Degree, var.equal=TRUE)
#   t = -0.3199, df = 125, p-value = 0.7496

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_plurinet_covar_degree_p7.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(pluri.low.var$Degree, pluri.mid.var$Degree, pluri.high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Plurinet degree box plots for variance bins\n in P7 phenotype", xlab="CoV percentile", ylab="Degree")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
pluri.wilcox.test <- wilcox.test(pluri.low.var$Degree, pluri.high.var$Degree)
#  p.val = 0.4248


#   For Hough P4 samples
plurinet_degree <- read.table(plurinet.file, header = T, sep = "\t")
rownamespluri <- as.character(plurinet_degree$ID)
rownames(plurinet_degree) <- rownamespluri
plurinet_exprs_data <- p4[rownames(p4) %in% plurinet_degree$ID,]
plurinet_degree <- plurinet_degree[rownames(plurinet_degree) %in% rownames(plurinet_exprs_data),]
plurinet_degree <- plurinet_degree[rownames(plurinet_exprs_data),]
plurinet_degree <- cbind(plurinet_degree, plurinet_exprs_data$Mean, plurinet_exprs_data$coVar)
colnames(plurinet_degree) <- c("ID", "Degree", "Mean", "coVar")
plurinet_degree <- plurinet_degree[order(plurinet_degree$coVar, -(plurinet_degree$Degree)),]

pluri.low.var <- plurinet_degree[1:63,]
pluri.mid.var <- plurinet_degree[64:185,]
pluri.high.var <- plurinet_degree[186:249,]

write.table(pluri.low.var, file=paste(output_dir,'ontology_analysis/low_var_plurinet_P4_Hough.txt', sep=""),  quote = FALSE, sep="\t")
write.table(pluri.high.var, file=paste(output_dir,'ontology_analysis/high_var_plurinet_P4_Hough.txt', sep=""),  quote = FALSE, sep="\t")

#   Prepare the density distributions for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Density_plot_plurinet_covar_degree_p4.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(pluri.low.var$Degree), main="Plurinet degree density plots for variance bins\n in P4 dataset", ylim=range(c(0, 0.3)), xlab="Degree", ylab="Density", lty=1, lwd=4, col="red")
		lines(density(pluri.mid.var$Degree), lty=1, lwd=4, col="gray")
        lines(density(pluri.high.var$Degree), lty=1, lwd=4, col="deepskyblue")
        legend(x=10, y=0.27, c("Low Variance", "Mid Variance", "High Variance"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
	dev.off()
#   Use a non-parametric Kolmogorov-Smirnov test used to test for significant differences between degree distributions
test <- ks.test(pluri.low.var$Degree, pluri.high.var$Degree)
#   D = 0.0469, p-value = 1

degree.t <- t.test(pluri.low.var$Degree, pluri.high.var$Degree, var.equal=TRUE)
#   t = -0.8928, df = 125, p-value = 0.3737

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_plurinet_covar_degree_p4.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(pluri.low.var$Degree, pluri.mid.var$Degree, pluri.high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Plurinet degree box plots for variance bins\n in P4 phenotype", xlab="CoV percentile", ylab="Degree")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
pluri.wilcox.test <- wilcox.test(pluri.low.var$Degree, pluri.high.var$Degree)
#  p.val = 0.5786


#   For Vitale iPS_high samples
plurinet_degree <- read.table(plurinet.file, header = T, sep = "\t")
rownamespluri <- as.character(plurinet_degree$ID)
rownames(plurinet_degree) <- rownamespluri
plurinet_exprs_data <- ips.high.exprs[rownames(ips.high.exprs) %in% plurinet_degree$ID,]
plurinet_degree <- plurinet_degree[rownames(plurinet_degree) %in% rownames(plurinet_exprs_data),]
plurinet_degree <- plurinet_degree[rownames(plurinet_exprs_data),]
plurinet_degree <- cbind(plurinet_degree, plurinet_exprs_data$Mean, plurinet_exprs_data$coVar)
colnames(plurinet_degree) <- c("ID", "Degree", "Mean", "coVar")
plurinet_degree <- plurinet_degree[order(plurinet_degree$coVar, -(plurinet_degree$Degree)),]

pluri.low.var <- plurinet_degree[1:65,]
pluri.mid.var <- plurinet_degree[66:193,]
pluri.high.var <- plurinet_degree[194:258,]

#   Prepare the density distributions for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Density_plot_plurinet_covar_degree_iPS_high.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(pluri.low.var$Degree), main="Plurinet degree density plots for variance bins\n in Vitale iPS_high", ylim=range(c(0, 0.3)), xlab="Degree", ylab="Density", lty=1, lwd=4, col="red")
		lines(density(pluri.mid.var$Degree), lty=1, lwd=4, col="gray")
        lines(density(pluri.high.var$Degree), lty=1, lwd=4, col="deepskyblue")
        legend(x=10, y=0.27, c("Low Variance", "Mid Variance", "High Variance"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
	dev.off()
#   Use a non-parametric Kolmogorov-Smirnov test used to test for significant differences between degree distributions
test <- ks.test(pluri.low.var$Degree, pluri.high.var$Degree)
#   D = 0.1538, p-value = 0.4252

degree.t <- t.test(pluri.low.var$Degree, pluri.high.var$Degree, var.equal=TRUE)
#   t = 1.628, df = 128, p-value = 0.106

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_plurinet_covar_degree_iPS_high.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(pluri.low.var$Degree, pluri.mid.var$Degree, pluri.high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Plurinet degree box plots for variance bins\n in Vitale iPS_high phenotype", xlab="CoV percentile", ylab="Degree")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
pluri.wilcox.test <- wilcox.test(pluri.low.var$Degree, pluri.high.var$Degree)
#  p.val = 0.07212

#   For Vitale iPS_low samples
plurinet_degree <- read.table(plurinet.file, header = T, sep = "\t")
rownamespluri <- as.character(plurinet_degree$ID)
rownames(plurinet_degree) <- rownamespluri
plurinet_exprs_data <- ips.low.exprs[rownames(ips.low.exprs) %in% plurinet_degree$ID,]
plurinet_degree <- plurinet_degree[rownames(plurinet_degree) %in% rownames(plurinet_exprs_data),]
plurinet_degree <- plurinet_degree[rownames(plurinet_exprs_data),]
plurinet_degree <- cbind(plurinet_degree, plurinet_exprs_data$Mean, plurinet_exprs_data$coVar)
colnames(plurinet_degree) <- c("ID", "Degree", "Mean", "coVar")
plurinet_degree <- plurinet_degree[order(plurinet_degree$coVar, -(plurinet_degree$Degree)),]

pluri.low.var <- plurinet_degree[1:65,]
pluri.mid.var <- plurinet_degree[66:193,]
pluri.high.var <- plurinet_degree[194:258,]

#   Prepare the density distributions for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Density_plot_plurinet_covar_degree_iPS_low.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(pluri.low.var$Degree), main="Plurinet degree density plots for variance bins\n in Vitale iPS_low", ylim=range(c(0, 0.3)), xlab="Degree", ylab="Density", lty=1, lwd=4, col="red")
		lines(density(pluri.mid.var$Degree), lty=1, lwd=4, col="gray")
        lines(density(pluri.high.var$Degree), lty=1, lwd=4, col="deepskyblue")
        legend(x=10, y=0.27, c("Low Variance", "Mid Variance", "High Variance"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
	dev.off()
#   Use a non-parametric Kolmogorov-Smirnov test used to test for significant differences between degree distributions
test <- ks.test(pluri.low.var$Degree, pluri.high.var$Degree)
#   D = 0.1231, p-value = 0.7085

#   T-test (independent samples, equal variances)
degree.t <- t.test(pluri.low.var$Degree, pluri.high.var$Degree, var.equal=TRUE)
#   t = 0.4287, df = 128, p-value = 0.6688

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_plurinet_covar_degree_iPS_low.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(pluri.low.var$Degree, pluri.mid.var$Degree, pluri.high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Plurinet degree box plots for variance bins\n in Vitale iPS_low phenotype", xlab="CoV percentile", ylab="Degree")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
pluri.wilcox.test <- wilcox.test(pluri.low.var$Degree, pluri.high.var$Degree)
#  p.val = 0.1988


######################################################################################################################################################################################################
P4 <- p4[rownames(p4) %in% network_nodes,]
P5 <- p5[rownames(p5) %in% network_nodes,]
P6 <- p6[rownames(p6) %in% network_nodes,]
P7 <- p7[rownames(p7) %in% network_nodes,]
All.P <- all_pfract[rownames(all_pfract) %in% network_nodes,]

#   Boxplots of all CoV values in coexpression network
postscript(file=paste(output_dir,"network_analysis/cov_analysis/cov_boxplots_network_nodes.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    boxplot(ips_low$coVar, ips_high$coVar, ips$coVar, ips.iso$coVar, hes_data$coVar, P4$coVar, P7$coVar, col=c(pretty.col), names=c("iPS low\n (Vitale)", "iPS_high\n (Vitale)", "iPS\n (Briggs)", "iPS\n (isogenic)", "hES\n (Vitale)", "hES P4\n (Hough)", "hES P7\n (Hough)"), ylim=c(0,80), ylab="Coefficient of variation", xlab="Dataset", main="Coefficient of variation\n for all network nodes in each dataset")
dev.off()
#   Boxplots of all Mean values in coexpression network 
postscript(file=paste(output_dir,"network_analysis/mean_analysis/mean_boxplots_network_nodes.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    boxplot(ips_low$Mean, ips_high$Mean, ips$Mean, ips.iso$Mean, hes_data$Mean, P4$Mean, P7$Mean, col=c(pretty.col), names=c("iPS low\n (Vitale)", "iPS_high\n (Vitale)", "iPS\n (Briggs)", "iPS\n (isogenic)", "hES\n (Vitale)", "hES P4\n (Hough)", "hES P7\n (Hough)"), ylim=c(0,25), ylab="Mean (log2) expression", xlab="Dataset", main="Coefficient of variation\n for all network nodes in each dataset")
dev.off()
#   Density plots of all CoV values in coexpression network

postscript(file=paste(output_dir,"network_analysis/cov_analysis/cov_densityplots_network_nodes.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
plot(density(log2(ips_low$coVar)), main="CoV Network Nodes", ylim=range(0, 0.5), xlab="Log 2 CoV", ylab="Density", lty=1, lwd=4, col="#FF0000FF")
    lines(density(log2(ips_high$coVar)), lty=1, lwd=4, col='#FF9900FF')
    lines(density(log2(ips$coVar)), lty=1, lwd=4, col='#CCFF00FF')
    lines(density(log2(ips.iso$coVar)), lty=1, lwd=4, col='#33FF00FF')
    lines(density(log2(hes_data$coVar)), lty=1, lwd=4, col='#00FF66FF')
    lines(density(log2(P4$coVar)), lty=1, lwd=4, col='#00FFFFFF')
    lines(density(log2(P7$coVar)), lty=1, lwd=4, col='#0066FFFF')
    legend(x=5, y=0.5, c("iPS low", "iPS_high", "iPS", "iPS isogenic", "hES", "hES P4", "hES P7"), lty=c(1, 1, 1, 1, 1), lwd=(4), col=c("#FF0000FF", "#FF9900FF", "#CCFF00FF", "#33FF00FF", "#00FF66FF", "#00FFFFFF", "#0066FFFF"), text.col=c("#FF0000FF", "#FF9900FF", "#CCFF00FF", "#33FF00FF", "#00FF66FF", "#00FFFFFF", "#0066FFFF"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/cov_densityplots_network_nodes_pfractions.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
plot(density(log2(All.P$coVar)), main="CoV Network Nodes", xlab="Log 2 CoV", ylab="Density", lty=1, lwd=4, col="red")
    lines(density(log2(P5$coVar)), lty=1, lwd=4, col='#FF9900FF')
    lines(density(log2(P6$coVar)), lty=1, lwd=4, col= '#CCFF00FF')
    lines(density(log2(P4$coVar)), lty=1, lwd=4, col='#00FFFFFF')
    lines(density(log2(P7$coVar)), lty=1, lwd=4, col='#0066FFFF')
    legend(x=3, y=0.4, c("All Pfract", "P4", "P5", "P6", "P7"), lty=c(1, 1, 1, 1, 1), lwd=(4), col=c("red", "#FF9900FF", '#CCFF00FF', "#00FFFFFF", "#0066FFFF"), text.col=c("red", "#FF9900FF", '#CCFF00FF', "#00FFFFFF", "#0066FFFF"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/cov_densityplots_network_nodes_iPS.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    plot(density(log2(ips$coVar)), main="CoV Network Nodes iPS", xlab="Log 2 CoV", ylab="Density", lty=1, lwd=4, col="red")
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/cov_densityplots_network_nodes_iPS_iso.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    plot(density(log2(ips.iso$coVar)), main="CoV Network Nodes iPS isogenic", xlab="Log 2 CoV", ylab="Density", lty=1, lwd=4, col="red")
dev.off()


########################################################################
#   Degree by looking at Network
########################################################################
#   For the BIND network
clique <- read.table(bind_clique, header=F, sep="\t")
clique <- as.character(clique[,1])

#   For the STRING network
string.clique <- read.table(paste(base_dir,"output/cytoscape_input_files/string_clique.txt",sep=""), header=F, sep="\t")
s.c <- as.character(string.clique[,1])
string.disjoint <- read.table(paste(base_dir,"output/cytoscape_input_files/string_disjoint.txt",sep=""), header=F, sep="\t")
s.d <- as.character(string.disjoint[,1])
s.all <- c(s.c,s.d)

#   Identify the disconnected/hubb/periphery

dis <- paste(base_dir,"output/cytoscape_input_files/disconnected_march.csv",sep="")
hub <- paste(base_dir,"output/cytoscape_input_files/hub_march.csv",sep="")
disconnected <- read.table(dis, header=T, sep="\t")
hubb <- read.table(hub, header=T, sep="\t")

dis.nodes <- as.character(disconnected[,1])
hubb.nodes <- as.character(hubb[,1])
nodes <- c(dis.nodes,hubb.nodes)


#   IPS (Briggs) dataset
#   Use dec as generic function and just change dataset and filenames each time

dec <- ips

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

C <- dec[rownames(dec) %in% clique,]
Dis <- dec[!(rownames(dec) %in% clique),]

SC <- dec[rownames(dec) %in% s.c,]
SD <- dec[rownames(dec) %in% s.d,]
SL <- dec[!(rownames(dec)) %in% s.all,]


#   Write each list out to file for GO analysis
write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/iPS_Briggs/periphery_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/iPS_Briggs/disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/iPS_Briggs/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")


postscript(file = paste(output_dir,"network_analysis/degree_analysis/iPS_Briggs/CoV_Boxplot_by_network_location_iPS.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylab="Coefficient of variation", xlab="Location in network", main="iPS coefficient of variation\n for location in network")
dev.off()

postscript(file = paste(output_dir,"network_analysis/degree_analysis/iPS_Briggs/CoV_Boxplot_by_network_location_iPS_BIND_NETWORK.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(Dis$coVar, C$coVar, col=c("red", "deepskyblue"), names=c("Disjoint\n N=1095", "Clique\n N=55"), ylab="Coefficient of variation", xlab="Location in network", main="iPS coefficient of variation\n for location in BIND network")
dev.off()

postscript(file = paste(output_dir,"network_analysis/degree_analysis/iPS_Briggs/CoV_Boxplot_by_network_location_iPS_STRING_NETWORK.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(SD$coVar, SL$coVar, SC$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disjoint\n N=44", "Leaf\n N=1003", "Clique\n N=103"), ylab="Coefficient of variation", xlab="Location in network", main="iPS coefficient of variation\n for location in STRING network")
dev.off()

#   Mann-Whitney-Wilcoxin test (non-parametric: differences in distribution and sample size)

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

DC.test <- wilcox.test(Dis$coVar, C$coVar)

DL.test <- wilcox.test(SD$coVar, SL$coVar)
DC.test <- wilcox.test(SD$coVar, SC$coVar)
CL.test <- wilcox.test(SC$coVar, SL$coVar)


#   DP p.val = 0.0009642
#   PH p.val = 6.619e-13
#   DH p.val = 0.002347

#   DC p.val = 2.533e-05

#   DL p.val = 0.00323
#   DC p.val = 6.688e-08
#   CL p.val =  2.461e-07

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/iPS_Briggs/CoV_Density_by_network_location_iPS.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(log2(D$coVar), kernel = c("gaussian")), main="Density plots for CoV of all co-expression network nodes\n in iPS Briggs dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(log2(P$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="gray")
		lines(density(log2(H$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="deepskyblue")
        legend(x=25, y=0.25, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 3.849e-08

#   ISOGENIC iPS

dec <- ips.iso

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

#   Write each list out to file for GO analysis
write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/iPS_Briggs/periphery_iPS_iso.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/iPS_Briggs/disconnected_iPS_iso.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/iPS_Briggs/hubb_iPS_iso.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")


postscript(file = paste(output_dir,"network_analysis/degree_analysis/iPS_Briggs/CoV_Boxplot_by_network_location_iPS_iso.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylab="Coefficient of variation", xlab="Location in network", main="iPS coefficient of variation\n for location in network")
dev.off()

#   Mann-Whitney-Wilcoxin test (non-parametric: differences in distribution and sample size)

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = W = 27137.5, p-value = 0.0005741
#   PH p.val = W = 5607, p-value = 7.824e-11
#   DH p.val = W = 57138, p-value = 0.00101

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/iPS_Briggs/CoV_Density_by_network_location_iPS_iso.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(log2(D$coVar), kernel = c("gaussian")), main="Density plots for CoV of all co-expression network nodes\n in isogenic iPS Briggs dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(log2(P$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="gray")
		lines(density(log2(H$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="deepskyblue")
        legend(x=25, y=0.25, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 6.172e-07

#   P4 and P7 together

dec <- p4[rownames(p4) %in% network_nodes,]
P.4 <- dec[!(rownames(dec) %in% nodes),]
D.4 <- dec[rownames(dec) %in% dis.nodes,]
H.4 <- dec[rownames(dec) %in% hubb.nodes,]
dec <- p7[rownames(p7) %in% network_nodes,]
D.7 <- dec[rownames(dec) %in% dis.nodes,]
H.7 <- dec[rownames(dec) %in% hubb.nodes,]

postscript(file=paste(output_dir,"network_analysis/degree_analysis/hES_P4_Hough/CoV_Density_by_network_location_P4_P7.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(log2(D.4$coVar)), kernel = c("gaussian"), ylim=range(0, 0.5), xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(log2(H.4$coVar)), kernel = c("gaussian"), lty=1, lwd=4, col="deepskyblue")
		lines(density(log2(D.7$coVar)), kernel = c("gaussian"), lty=1, lwd=4, col="orange")
		lines(density(log2(H.7$coVar)), kernel = c("gaussian"), lty=1, lwd=4, col="darkblue")
        legend(x=2, y=0.5, c("Disjoint P4", "Disjoint P7", "Clique P4", "Clique P7"), lty=c(1, 1, 1), lwd=(4), col=c("red", "orange", "deepskyblue", "darkblue"), text.col=c("red", "orange", "deepskyblue", "darkblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D.4$coVar, D.7$coVar)
#   Dijoint group P4-P7 p.val 2.2e-16

test <- ks.test(H.4$coVar, H.7$coVar)
#   Clique group P4-P7 p.val 2.997e-09


#   P4 phenotype (Hough)

dec <- p4[rownames(p4) %in% network_nodes,]
P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

p4.p <- P
p4.d <- D
p4.h <- H

#   Write each list out to file for GO analysis
write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/hES_P4_Hough/periphery_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/hES_P4_Hough/disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/hES_P4_Hough/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")


postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_P4_Hough/degree_by_network_location_p4.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=863", "Periphery\n N=72", "Hubb\n N=91"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="hES P4 coefficient of variation\n for location in network")
dev.off()

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = 0.001026
#   PH p.val = 0.2348
#   DH p.val = 1.496e-08

postscript(file=paste(output_dir,"network_analysis/degree_analysis/hES_P4_Hough/CoV_Density_by_network_location_P4.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(D$coVar), kernel = c("gaussian"), main="Density plots for mean expression of all probes\n in P4 Hough dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(P$coVar), kernel = c("gaussian"), lty=1, lwd=4, col="gray")
		lines(density(H$coVar), kernel = c("gaussian"), lty=1, lwd=4, col="deepskyblue")
        legend(x=6, y=0.4, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 2.791e-08

#   All P-factions samples (Hough)
dec <- all_pfract[rownames(all_pfract) %in% network_nodes,]

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

apf.p <- P
apf.d <- D
apf.h <- H

write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/hES_P7_Hough/all_p_fract_periphery_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/hES_P7_Hough/all_p_fract_disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/hES_P7_Hough/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")

postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_P7_Hough/degree_by_network_location_all_p_fract.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="hES all fractions coefficient of variation\n for location in network")
dev.off()

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = 9.255e-09
#   PH p.val = 0.1465
#   DH p.val = 1.289e-13

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/hES_P7_Hough/CoV_Density_by_network_location_all_pfract.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(D$coVar), kernel = c("gaussian"), main="Density plots for mean expression of all probes\n in all P fractions Hough dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(P$coVar), kernel = c("gaussian"), lty=1, lwd=4, col="gray")
		lines(density(H$coVar), kernel = c("gaussian"), lty=1, lwd=4, col="deepskyblue")
        legend(x=6, y=0.4, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 

#   P7 samples (Hough)
dec <- p7[rownames(p7) %in% network_nodes,]

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

p7.p <- P
p7.d <- D
p7.h <- H

write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/hES_P7_Hough/periphery_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/hES_P7_Hough/disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/hES_P7_Hough/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")

postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_P7_Hough/degree_by_network_location_p7.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="hES P7 coefficient of variation\n for location in network")
dev.off()

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = 0.0189
#   PH p.val = 0.2994
#   DH p.val = 0.2328

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/hES_P7_Hough/CoV_Density_by_network_location_P7.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(D$coVar), kernel = c("gaussian"), main="Density plots for mean expression of all probes\n in P7 Hough dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(P$coVar), kernel = c("gaussian"), lty=1, lwd=4, col="gray")
		lines(density(H$coVar), kernel = c("gaussian"), lty=1, lwd=4, col="deepskyblue")
        legend(x=6, y=0.4, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 0.2287

#   P5

dec <- p5[rownames(p5) %in% network_nodes,]

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

p5.p <- P
p5.d <- D
p5.h <- H

postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_P5_Hough/degree_by_network_location_p5.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="hES P7 coefficient of variation\n for location in network")
dev.off()

#   P6

dec <- p6[rownames(p6) %in% network_nodes,]

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

p6.p <- P
p6.d <- D
p6.h <- H

postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_P6_Hough/degree_by_network_location_p6.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="hES P7 coefficient of variation\n for location in network")
dev.off()

#   Clique/Leaf/Disjoint boxplots
postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_P4_Hough/clique_boxplots.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(p4.h$coVar, p5.h$coVar, p6.h$coVar, p7.h$coVar, col=c("deepskyblue", "deepskyblue", "deepskyblue", "deepskyblue"), names=c("P4", "P5", "P6", "P7"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="Clique coefficient of variation\n for each P fraction")
dev.off()

p4p5.test <- wilcox.test(p4.h$coVar, p5.h$coVar)
p5p6.test <- wilcox.test(p5.h$coVar, p6.h$coVar)
p6p7.test <- wilcox.test(p6.h$coVar, p7.h$coVar)
p4p7.test <- wilcox.test(p4.h$coVar, p7.h$coVar)


postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_P4_Hough/disjoint_boxplots.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(p4.d$coVar, p5.d$coVar, p6.d$coVar, p7.d$coVar, col=c("red", "red", "red", "red"), names=c("P4", "P5", "P6", "P7"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="Disjoint coefficient of variation\n for each P fraction")
dev.off()

p4p5.test <- wilcox.test(p4.d$coVar, p5.d$coVar)
p5p6.test <- wilcox.test(p5.d$coVar, p6.d$coVar)
p6p7.test <- wilcox.test(p6.d$coVar, p7.d$coVar)
p4p7.test <- wilcox.test(p4.d$coVar, p7.d$coVar)


postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_P4_Hough/leaf_boxplots.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(p4.p$coVar, p5.p$coVar, p6.p$coVar, p7.p$coVar, col=c("gray", "gray", "gray", "gray"), names=c("P4", "P5", "P6", "P7"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="Leaf coefficient of variation\n for each P fraction")
dev.off()

p4p5.test <- wilcox.test(p4.p$coVar, p5.p$coVar)
p5p6.test <- wilcox.test(p5.p$coVar, p6.p$coVar)
p6p7.test <- wilcox.test(p6.p$coVar, p7.p$coVar)
p4p7.test <- wilcox.test(p4.p$coVar, p7.p$coVar)

#   iPS_low phenotype (Vitale)

dec <- ips_low

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/iPS_low_Vitale/periphery_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/iPS_low_Vitale/disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/iPS_low_Vitale/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")

postscript(file = paste(output_dir,"network_analysis/degree_analysis/iPS_low_Vitale/degree_by_network_location_vitale_IPS_low.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylab="Coefficient of variation", xlab="Location in network", main="iPS low (Vitale) coefficient of variation\n for location in network")
dev.off()

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = 2.636e-13
#   PH p.val = 0.7009
#   DH p.val = 3.03e-16

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/iPS_low_Vitale/CoV_Density_by_network_location_iPS_low.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(log2(D$coVar), kernel = c("gaussian")), main="Density plots for mean expression of all probes\n in iPS_low Vitale dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(log2(P$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="gray")
		lines(density(log2(H$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="deepskyblue")
        legend(x=30, y=0.1, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 1.243e-14


#   iPS_high (Vitale) phenotype

dec <- ips_high

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/iPS_high_Vitale/periphery_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/iPS_high_Vitale/disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/iPS_high_Vitale/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")

postscript(file = paste(output_dir,"network_analysis/degree_analysis/iPS_high_Vitale/degree_by_network_location_vitale_IPS_high.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylab="Coefficient of variation", xlab="Location in network", main="iPS high (Vitale) coefficient of variation\n for location in network")
dev.off()

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = 1.834e-07
#   PH p.val = 0.0001151
#   DH p.val = 0.1941

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/iPS_high_Vitale/CoV_Density_by_network_location_iPS_high.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(log2(D$coVar), kernel = c("gaussian")), main="Density plots for mean expression of all probes\n in iPS_high Vitale dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(log2(P$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="gray")
		lines(density(log2(H$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="deepskyblue")
        legend(x=30, y=0.1, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 0.009821

dec <- hes_data

P <- dec[!(rownames(dec) %in% nodes),4:5]
D <- dec[rownames(dec) %in% dis.nodes,4:5]
H <- dec[rownames(dec) %in% hubb.nodes,4:5]

write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/hES_Vitale/periphery_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/hES_Vitale/disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/hES_Vitale/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")

postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_Vitale/degree_by_network_location_vitale_HES.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=963", "Periphery\n N=73", "Hubb\n N=97"), ylab="Coefficient of variation", xlab="Location in network", main="hES (Vitale) coefficient of variation\n for location in network")
dev.off()

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = 0.0001619
#   PH p.val = 0.0001623
#   DH p.val =  0.5609

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/hES_Vitale/CoV_Density_by_network_location_hES_Vitale.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(log2(D$coVar), kernel = c("gaussian")), main="Density plots for mean expression of all probes\n in hES Vitale dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(log2(P$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="gray")
		lines(density(log2(H$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="deepskyblue")
        legend(x=30, y=0.2, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value =  0.1735


########################################################################
#
#   Genes that change across different datasets
#
########################################################################

p6 <- p6[rownames(p6) %in% network_nodes,]
p5 <- p5[rownames(p5) %in% network_nodes,]


#   Low variance/clique
#   OCT4
oct4.average <- c(p4["POU5F1","Mean"], p5["POU5F1","Mean"], p6["POU5F1","Mean"], p7["POU5F1","Mean"])
oct4.cov <- c(p4["POU5F1","coVar"], p5["POU5F1","coVar"], p6["POU5F1","coVar"], p7["POU5F1","coVar"])
oct4.average.ips <- c(ips_low["POU5F1","Mean"], ips_high["POU5F1","Mean"], ips["POU5F1","Mean"], hes_data["POU5F1","Mean"])
oct4.cov.ips <- c(ips_low["POU5F1","coVar"], ips_high["POU5F1","coVar"], ips["POU5F1","coVar"], hes_data["POU5F1","coVar"])


postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_pou5f1.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(oct4.average, main="Average expression of POU5F1", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_pou5f1.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(oct4.cov, main="Variance in expression of POU5F1", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_pou5f1_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(oct4.average.ips, main="Average expression of POU5F1", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_pou5f1_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(oct4.cov.ips, main="Variance in expression of POU5F1", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

#   CLDN6
cldn6.average <- c(p4["CLDN6","Mean"], p5["CLDN6","Mean"], p6["CLDN6","Mean"], p7["CLDN6","Mean"])
cldn6.cov <- c(p4["CLDN6","coVar"], p5["CLDN6","coVar"], p6["CLDN6","coVar"], p7["CLDN6","coVar"])
cldn6.average.ips <- c(ips_low["CLDN6","Mean"], ips_high["CLDN6","Mean"], ips["CLDN6","Mean"], hes_data["CLDN6","Mean"])
cldn6.cov.ips <- c(ips_low["CLDN6","coVar"], ips_high["CLDN6","coVar"], ips["CLDN6","coVar"], hes_data["CLDN6","coVar"])

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_cldn6.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(cldn6.average, main="Average expression of CLDN6", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_cldn6.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(cldn6.cov, main="Variance in expression of CLDN6", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_cldn6_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(cldn6.average.ips, main="Average expression of CLDN6", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_cldn6_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(cldn6.cov.ips, main="Variance in expression of CLDN6", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()


#   SOX2
sox2.average <- c(p4["SOX2","Mean"], p5["SOX2","Mean"], p6["SOX2","Mean"], p7["SOX2","Mean"])
sox2.cov <- c(p4["SOX2","coVar"], p5["SOX2","coVar"], p6["SOX2","coVar"], p7["SOX2","coVar"])
sox2.average.ips <- c(ips_low["SOX2","Mean"], ips_high["SOX2","Mean"], ips["SOX2","Mean"], hes_data["SOX2","Mean"])
sox2.cov.ips <- c(ips_low["SOX2","coVar"], ips_high["SOX2","coVar"], ips["SOX2","coVar"], hes_data["SOX2","coVar"])

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_sox2.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(sox2.average, main="Average expression of SOX2", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_sox2.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(sox2.cov, main="Variance in expression of SOX2", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_sox2_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(sox2.average.ips, main="Average expression of SOX2", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_sox2_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(sox2.cov.ips, main="Variance in expression of SOX2", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

#   DNMT3B
dnmt3b.average <- c(p4["DNMT3B","Mean"], p5["DNMT3B","Mean"], p6["DNMT3B","Mean"], p7["DNMT3B","Mean"])
dnmt3b.cov <- c(p4["DNMT3B","coVar"], p5["DNMT3B","coVar"], p6["DNMT3B","coVar"], p7["DNMT3B","coVar"])
dnmt3b.average.ips <- c(ips_low["DNMT3B","Mean"], ips_high["DNMT3B","Mean"], ips["DNMT3B","Mean"], hes_data["DNMT3B","Mean"])
dnmt3b.cov.ips <- c(ips_low["DNMT3B","coVar"], ips_high["DNMT3B","coVar"], ips["DNMT3B","coVar"], hes_data["DNMT3B","coVar"])


postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_dnmt3b.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(dnmt3b.average, main="Average expression of DNMT3B", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_dnmt3b.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(dnmt3b.cov, main="Variance in expression of DNMT3B", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_dnmt3b_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(dnmt3b.average.ips, main="Average expression of DNMT3B", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_dnmt3b_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(dnmt3b.cov.ips, main="Variance in expression of DNMT3B", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

#   High variance/disjoint
MST1.average <- c(p4["MST1","Mean"], p5["MST1","Mean"], p6["MST1","Mean"], p7["MST1","Mean"])
MST1.cov <- c(p4["MST1","coVar"], p5["MST1","coVar"], p6["MST1","coVar"], p7["MST1","coVar"])

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_MST1.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(MST1.average, main="Average expression of MST1", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_MST1.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(MST1.cov, main="Variance in expression of MST1", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

CDK11A.average <- c(p4["CDK11A","Mean"], p5["CDK11A","Mean"], p6["CDK11A","Mean"], p7["CDK11A","Mean"])
CDK11A.cov <- c(p4["CDK11A","coVar"], p5["CDK11A","coVar"], p6["CDK11A","coVar"], p7["CDK11A","coVar"])

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_CDK11A.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(CDK11A.average, main="Average expression of CDK11A", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_CDK11A.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(CDK11A.cov, main="Variance in expression of MST1", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()


GPR64.average <- c(p4["GPR64","Mean"], p5["GPR64","Mean"], p6["GPR64","Mean"], p7["GPR64","Mean"])
GPR64.cov <- c(p4["GPR64","coVar"], p5["GPR64","coVar"], p6["GPR64","coVar"], p7["GPR64","coVar"])

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_GPR64.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(GPR64.average, main="Average expression of GPR64", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_GPR64.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(GPR64.cov, main="Variance in expression of GPR64", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()


KCNG3.average <- c(p4["KCNG3","Mean"], p5["KCNG3","Mean"], p6["KCNG3","Mean"], p7["KCNG3","Mean"])
KCNG3.cov <- c(p4["KCNG3","coVar"], p5["KCNG3","coVar"], p6["KCNG3","coVar"], p7["KCNG3","coVar"])

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_KCNG3.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(KCNG3.average, main="Average expression of KCNG3", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_KCNG3.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(KCNG3.cov, main="Variance in expression of KCNG3", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()


#############################################################################################################################################################################################################
#   ARCHIVED
#   Degree by Network topology

hub <- paste(base_dir,"output/network_analysis/degree_analysis/hubb.txt",sep="")
hubb <- read.table(hub, header=T, sep="\t")
hubb.nodes <- as.character(hubb[,1])

dec <- ips
D <- dec[!(rownames(dec) %in% hubb.nodes),c(19, 21)]
H <- dec[rownames(dec) %in% hubb.nodes,c(19, 21)]

postscript(file = paste(output_dir,"network_analysis/plots/degree_by_cytoscape_connectivity.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, H$coVar, col=c(pretty.col), names=c("Disconnected\n N=982", "Hubb\n N=168"), ylab="Coefficient of variation", xlab="Classified degree of connectivity", main="iPS coefficient of variation\n for each degree of connectivity")
dev.off()
#   Mann-Whitney-Wilcoxin test (non-parametric: differences in distribution and sample size)
DH.test <- wilcox.test(D$coVar, H$coVar)
#   DH p.val = 0.791

d <- D$coVar
h <- H$coVar
test <- ks.test(d, h)
#   p.value = 0.0002273629
#   Kolmogorov-Smirnov test used to test CoV distributions of core and disconnected network nodes

postscript(file=paste(output_dir,"network_analysis/cov_analysis/Cov_density_network_topology_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(d), ylab= "Density", ylim= c(range(0, 0.25)), xlab="Coefficient of Variation", lty=1, lwd=4, col="red")
		lines(density(h), lty=1, lwd=4, col="purple")
        legend(x=20, y=0.2, c("Disconnected from core", "Network core"), lty=c(1, 1), lwd=(4), col=c("red", "purple"), text.col=c("red", "purple"))
	dev.off()



#   Write each list out to file for GO analysis
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")


#   Degree by network topology with 2 validation datasets

#   P-fractions data

dec <- p45
D <- dec[!(rownames(dec) %in% hubb.nodes),7:8]
H <- dec[rownames(dec) %in% hubb.nodes,7:8]

postscript(file = paste(output_dir,"network_analysis/plots/degree_by_cytoscape_connectivity_p45.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, H$coVar, col=c(pretty.col), names=c("Disconnected\n N=865", "Hubb\n N=161"), ylim=c(0,30), ylab="Coefficient of variation", xlab="Classified degree of connectivity", main="p45 coefficient of variation\n for each degree of connectivity")
dev.off()
DH.test <- wilcox.test(D$coVar, H$coVar)
#   DH p.val = 3.522361e-13

d <- D$coVar
h <- H$coVar
test <- ks.test(d, h)
#   p.value = 1.003864e-12

postscript(file=paste(output_dir,"network_analysis/cov_analysis/Cov_density_network_topology_p45.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(d), ylab= "Density", ylim= c(range(0, 0.4)), xlab="Coefficient of Variation", lty=1, lwd=4, col="red")
		lines(density(h), lty=1, lwd=4, col="purple")
        legend(x=8, y=0.3, c("Disconnected from core", "Network core"), lty=c(1, 1), lwd=(4), col=c("red", "purple"), text.col=c("red", "purple"))
	dev.off()


dec <- p67
D <- dec[!(rownames(dec) %in% hubb.nodes), c(7, 8)]
H <- dec[rownames(dec) %in% hubb.nodes, c(7, 8)]

postscript(file = paste(output_dir,"network_analysis/plots/degree_by_cytoscape_connectivity_p67.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, H$coVar, col=c(pretty.col), names=c("Disconnected\n N=865", "Hubb\n N=161"), ylim=c(0,30), ylab="Coefficient of variation", xlab="Classified degree of connectivity", main="p67 coefficient of variation\n for each degree of connectivity")
dev.off()
DH.test <- wilcox.test(D$coVar, H$coVar)
#   DH p.val = 5.322e-11

d <- D$coVar
h <- H$coVar
test <- ks.test(d, h)
#   p.value = 4.660393e-10
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Cov_density_network_topology_p67.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(d), ylab= "Density", ylim= c(range(0, 0.8)), xlab="Coefficient of Variation", lty=1, lwd=4, col="red")
		lines(density(h), lty=1, lwd=4, col="purple")
        legend(x=5, y=0.6, c("Disconnected from core", "Network core"), lty=c(1, 1), lwd=(4), col=c("red", "purple"), text.col=c("red", "purple"))
	dev.off()


#   Degree by network topology with Vitale datasets

dec <- ips_low
D <- dec[!(rownames(dec) %in% hubb.nodes),c(5, 7)]
H <- dec[rownames(dec) %in% hubb.nodes,c(5, 7)]

postscript(file = paste(output_dir,"network_analysis/plots/degree_by_cytoscape_connectivity_vitale_IPS_low.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, H$coVar, col=c(pretty.col), names=c("Disconnected\n N=971", "Hubb\n N=168"), ylab="Coefficient of variation", xlab="Classified degree of connectivity", main="iPS low (Vitale) coefficient of variation\n for each degree of connectivity")
dev.off()
DH.test <- wilcox.test(D$coVar, H$coVar)
#   DH p.val =  1.326112e-25

d <- D$coVar
h <- H$coVar
test <- ks.test(d, h)
#   p.value = 0


dec <- ips_high
D <- dec[!(rownames(dec) %in% hubb.nodes),c(6, 8)]
H <- dec[rownames(dec) %in% hubb.nodes,c(6, 8)]

postscript(file = paste(output_dir,"network_analysis/plots/degree_by_cytoscape_connectivity_vitale_IPS_high.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, H$coVar, col=c(pretty.col), names=c("Disconnected\n N=971", "Hubb\n N=168"), ylab="Coefficient of variation", xlab="Classified degree of connectivity", main="iPS high (Vitale) coefficient of variation\n for each degree of connectivity")
dev.off()
DH.test <- wilcox.test(D$coVar, H$coVar)
#   DH p.val = 5.179582e-05

d <- D$coVar
h <- H$coVar
test <- ks.test(d, h)
#   p.value = 2.126511e-05
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Cov_density_network_topology_ips_high.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(d), ylab= "Density", ylim= c(range(0, 0.2)), xlab="Coefficient of Variation", lty=1, lwd=4, col="red")
		lines(density(h), lty=1, lwd=4, col="purple")
        legend(x=20, y=0.15, c("Disconnected from core", "Network core"), lty=c(1, 1), lwd=(4), col=c("red", "purple"), text.col=c("red", "purple"))
	dev.off()


dec <- hes_data
D <- dec[!(rownames(dec) %in% hubb.nodes), c(4, 6)]
H <- dec[rownames(dec) %in% hubb.nodes, c(4, 6)]

postscript(file = paste(output_dir,"network_analysis/plots/degree_by_cytoscape_connectivity_vitale_HES.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, H$coVar, col=c(pretty.col), names=c("Disconnected\n N=971", "Hubb\n N=168"), ylab="Coefficient of variation", xlab="Classified degree of connectivity", main="hES (Vitale) coefficient of variation\n for each degree of connectivity")
dev.off()
DH.test <- wilcox.test(D$coVar, H$coVar)
#   DH p.val = 0.07331

d <- D$coVar
h <- H$coVar
test <- ks.test(d, h)
#   p.value = 0.0003758447
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Cov_density_network_topology_vitale_HES.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(d), ylab= "Density", ylim= c(range(0, 0.3)), xlab="Coefficient of Variation", lty=1, lwd=4, col="red")
		lines(density(h), lty=1, lwd=4, col="purple")
        legend(x=20, y=0.25, c("Disconnected from core", "Network core"), lty=c(1, 1), lwd=(4), col=c("red", "purple"), text.col=c("red", "purple"))
	dev.off()

##############################################################################################################################
#   ARCHIVED: look at variance profiles, and then the degree of connectivity in the co-expression network

########################################################################
#   Coexpression network CoV by quantiles
########################################################################

dec <- ips[order(ips$coVar, -(ips$Degree)),]

qCoV <- quantile(dec$coVar)

low <- dec[1:287, 19:21]
mid <- dec[288:862, 19:21]
upper <- dec[863:1150, 19:21]

bitmap(file = paste(output_dir,"network_analysis/plots/plot_archives/cov_by_quantiles.png",sep=""), type="png16m", height=1000, width = 1000, units = "px")
boxplot(low$Degree, mid$Degree, upper$Degree, col=c(pretty.col), names=c("Low Variance", "Moderate Variance", "High Variance"), ylim = range(-5,85), ylab="Degree", xlab="Coefficient of Variation Quantiles")
dev.off()

low.mid.test <- wilcox.test(low$Degree, mid$Degree)
mid.up.test <- wilcox.test(mid$Degree, upper$Degree)
low.up.test <- wilcox.test(low$Degree, upper$Degree)

low.degree <- density(low$Degree)
mid.degree <- density(mid$Degree)
upper.degree <- density(upper$Degree)

postscript(file=paste(output_dir,"network_analysis/cov_analysis/Degree_density_plot_cov_quantiles.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(low.degree, main="Degree density of CoV quantiles", ylim = range(c(0, 1.5)), ylab= "Density", xlab="Degree", lty=1, lwd=4, col="red")
		lines(upper.degree, lty=1, lwd=4, col="purple")
        legend(x=60, y=1, c("low variance genes", "high variance genes"), lty=c(1, 1), lwd=(4), col=c("red", "purple"), text.col=c("red", "purple"))
	dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of high and low variance gene sets
test <- ks.test(low$Degree, upper$Degree)
#   P.value = 0.0003195599


#   Histograms
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Degree_histogram_plot_cov_low_quantiles.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		hist(low$Degree, xlim = range(c(dec$Degree)), ylim = range(c(0, 300)), xlab="Degree", main= "Degree Frequency Histogram by CoV Quantiles")
	dev.off()
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Degree_histogram_plot_cov_high_quantiles.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		hist(upper$Degree, xlim = range(c(dec$Degree)), ylim = range(c(0, 300)), xlab="Degree", main= "Degree Frequency Histogram by CoV Quantiles")
	dev.off()


#   Use generic objects to plot and change with dataset

#   iPS isogenic
low.var <- ips.iso[1:288,]
mid.var <- ips.iso[289:861,]
high.var <- ips.iso[862:1150,]

#   Prepare the density distributions for each variance percentile
#   A Gaussian kernel density estimator (density function in R using Gaussian method to select bandwidth) was used to produce degree distribution density plots
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Density_plot_coexpression_covar_degree_ips_isogenic.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(low.var$Degree), main="Co-expression degree density plots for variance bins\n in iPS isogenic dataset", ylim=range(c(0, 0.3)), xlab="CoV", ylab="Degree", lty=1, lwd=4, col="red")
		lines(density(mid.var$Degree), lty=1, lwd=4, col="gray")
        lines(density(high.var$Degree), lty=1, lwd=4, col="deepskyblue")
        legend(x=20, y=0.27, c("Low Variance", "Mid Variance", "High Variance"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
	dev.off()
#   Use a non-parametric Kolmogorov-Smirnov test used to test for significant differences between degree distributions
low <- low.var$Degree
high <- high.var$Degree
test <- ks.test(low, high)
#   P.value = 5.29e-05

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_coexpression_covar_degree_ips_isogenic.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(low.var$Degree, mid.var$Degree, high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Co-expression degree box plots for variance bins\n in iPS dataset", xlab="Degree", ylab="Density")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
wilcox.test <- wilcox.test(low.var$Degree, high.var$Degree)
#  p.val = 0.1197

write.table(low.var, file=paste(output_dir,'ontology_analysis/low_var_iPS_isogenic.txt', sep=""),  quote = FALSE, sep="\t")
write.table(high.var, file=paste(output_dir,'ontology_analysis/high_var_iPS_isogenic.txt', sep=""),  quote = FALSE, sep="\t")


#   iPS Briggs
low.var <- ips[1:288,]
mid.var <- ips[289:861,]
high.var <- ips[862:1150,]

#   Prepare the density distributions for each variance percentile
#   A Gaussian kernel density estimator (density function in R using Gaussian method to select bandwidth) was used to produce degree distribution density plots
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Density_plot_coexpression_covar_degree_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(low.var$Degree), main="Co-expression degree density plots for variance bins\n in iPS dataset", ylim=range(c(0, 0.3)), xlab="CoV", ylab="Degree", lty=1, lwd=4, col="red")
		lines(density(mid.var$Degree), lty=1, lwd=4, col="gray")
        lines(density(high.var$Degree), lty=1, lwd=4, col="deepskyblue")
        legend(x=20, y=0.27, c("Low Variance", "Mid Variance", "High Variance"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
	dev.off()
#   Use a non-parametric Kolmogorov-Smirnov test used to test for significant differences between degree distributions
low <- low.var$Degree
high <- high.var$Degree
test <- ks.test(low, high)
#   P.value = 0.0002318

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_coexpression_covar_degree_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(low.var$Degree, mid.var$Degree, high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Co-expression degree box plots for variance bins\n in iPS dataset", xlab="Degree", ylab="Density")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
wilcox.test <- wilcox.test(low.var$Degree, high.var$Degree)
#  p.val = 0.07286

write.table(low.var, file=paste(output_dir,'ontology_analysis/low_var_iPS_briggs.txt', sep=""),  quote = FALSE, sep="\t")
write.table(high.var, file=paste(output_dir,'ontology_analysis/high_var_iPS_briggs.txt', sep=""),  quote = FALSE, sep="\t")


#   Vitale hES
low.var <- hes_data[1:285,]
mid.var <- hes_data[286:853,]
high.var <- hes_data[854:1139,]

#   Prepare the density distributions for each variance percentile
#   A Gaussian kernel density estimator (density function in R using Gaussian method to select bandwidth) was used to produce degree distribution density plots
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Density_plot_coexpression_covar_degree_hes.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(low.var$Degree), main="Co-expression degree density plots for variance bins\n in hES dataset", ylim=range(c(0, 0.3)), xlab="CoV", ylab="Degree", lty=1, lwd=4, col="red")
		lines(density(mid.var$Degree), lty=1, lwd=4, col="gray")
        lines(density(high.var$Degree), lty=1, lwd=4, col="deepskyblue")
        legend(x=20, y=0.27, c("Low Variance", "Mid Variance", "High Variance"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
	dev.off()
#   Use a non-parametric Kolmogorov-Smirnov test used to test for significant differences between degree distributions
low <- low.var$Degree
high <- high.var$Degree
test <- ks.test(low, high)
#   p-value = 0.001138

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_coexpression_covar_degree_hes.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(low.var$Degree, mid.var$Degree, high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Co-expression degree box plots for variance bins\n in Vitale hES phenotype", xlab="Degree", ylab="Density")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
wilcox.test <- wilcox.test(low.var$Degree, high.var$Degree)
#  p.val = 0.705

write.table(low.var, file=paste(output_dir,'ontology_analysis/low_var_hes_Vitale.txt', sep=""),  quote = FALSE, sep="\t")
write.table(high.var, file=paste(output_dir,'ontology_analysis/high_var_hes_Vitale.txt', sep=""),  quote = FALSE, sep="\t")


#   Vitale iPS_low

low.var <- ips_low[1:285,]
mid.var <- ips_low[286:853,]
high.var <- ips_low[854:1139,]

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_coexpression_covar_degree_ips_low.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(low.var$Degree, mid.var$Degree, high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Co-expression degree box plots for variance bins\n in Vitale iPS_high phenotype", xlab="Degree", ylab="Density")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
wilcox.test <- wilcox.test(low.var$Degree, high.var$Degree)
#   P.val = 

write.table(low.var, file=paste(output_dir,'ontology_analysis/low_var_ips_low_Vitale.txt', sep=""),  quote = FALSE, sep="\t")
write.table(high.var, file=paste(output_dir,'ontology_analysis/high_var_ips_low_Vitale.txt', sep=""),  quote = FALSE, sep="\t")

#   Vitale iPS_high

low.var <- ips_high[1:285,]
mid.var <- ips_high[286:853,]
high.var <- ips_high[854:1139,]

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_coexpression_covar_degree_ips_high.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(low.var$Degree, mid.var$Degree, high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Co-expression degree box plots for variance bins\n in Vitale iPS_high phenotype", xlab="Degree", ylab="Density")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
wilcox.test <- wilcox.test(low.var$Degree, high.var$Degree)
#   P.val = 0.06369

write.table(low.var, file=paste(output_dir,'ontology_analysis/low_var_ips_high_Vitale.txt', sep=""),  quote = FALSE, sep="\t")
write.table(high.var, file=paste(output_dir,'ontology_analysis/high_var_ips_high_Vitale.txt', sep=""),  quote = FALSE, sep="\t")

#   Hough P7
p.7.coexpression <- p7[rownames(p7) %in% network_nodes,]
p.7.coexpression$Degree <- ips[rownames(ips) %in% rownames(p.7.coexpression),"Degree"]

low.var <- p.7.coexpression[1:257,]
mid.var <- p.7.coexpression[258:768,]
high.var <- p.7.coexpression[769:1026,]

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_coexpression_covar_degree_P7.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(low.var$Degree, mid.var$Degree, high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Co-expression degree box plots for variance bins\n in Hough P7 phenotype", xlab="Degree", ylab="Density")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
wilcox.test <- wilcox.test(low.var$Degree, high.var$Degree)
#   P.val = 0.2946

write.table(low.var, file=paste(output_dir,'ontology_analysis/low_var_P7_hough.txt', sep=""),  quote = FALSE, sep="\t")
write.table(high.var, file=paste(output_dir,'ontology_analysis/high_var_P7_hough.txt', sep=""),  quote = FALSE, sep="\t")

#   Hough P4
p.4.coexpression <- p4[rownames(p4) %in% network_nodes,]
p.4.coexpression$Degree <- ips[rownames(ips) %in% rownames(p.4.coexpression),"Degree"]

low.var <- p.4.coexpression[1:257,]
mid.var <- p.4.coexpression[258:768,]
high.var <- p.4.coexpression[769:1026,]

#   Prepare box and whisker plots for each variance percentile
postscript(file=paste(output_dir,"network_analysis/cov_analysis/Boxplot_plot_coexpression_covar_degree_P4.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		boxplot(low.var$Degree, mid.var$Degree, high.var$Degree, col=c("red", "gray", "deepskyblue"), names=c("Bottom\n 25th", "Middle\n 50th", "Top\n 25th"), main="Co-expression degree box plots for variance bins\n in Hough P4 phenotype", xlab="Degree", ylab="Density")
	dev.off()
#   Man-whitney wilcoxin rank sum test used to test for significant differences between degree distributions
wilcox.test <- wilcox.test(low.var$Degree, high.var$Degree)
#   P.val = 

write.table(low.var, file=paste(output_dir,'ontology_analysis/low_var_P4_hough.txt', sep=""),  quote = FALSE, sep="\t")
write.table(high.var, file=paste(output_dir,'ontology_analysis/high_var_P4_hough.txt', sep=""),  quote = FALSE, sep="\t")

