library(lumi)
library(limma)

base_dir <- '/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/'
input_dir <- paste(base_dir,'output/mapped_expression_data/',sep="")
output_dir <- paste(base_dir,'output/',sep="")

val.dat <- paste(input_dir,'mapped_expression_validation_dataset.csv',sep="")
val.cols <- paste(input_dir,'header_validation.csv',sep="")

val_expression_data <- read.table(val.dat, header = F, sep = ",")
cnames <- read.table(val.cols, header = F, sep = ",")
cnames <- as.character(cnames$V1)
colnames(val_expression_data) <- cnames
rnames <- as.character(val_expression_data$gene)
rownames(val_expression_data) <- rnames

if ( identical(rnames, as.character(rownames(val_expression_data))) ) {
    val_expression_data <- val_expression_data[,-1]
}

pheno <- '/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/input/validation_data/phenotype_info.txt'
phenotype.info <- read.table(pheno, header=T, sep="\t")
cell.type <- factor(phenotype.info$Cell_Pheno)
design <- model.matrix(~0 + cell.type)
colnames(design) <- levels(cell.type)
#	Fit the normalised expression data with a linear model
fit <- lmFit((val_expression_data), design)
contrast.matrix <- makeContrasts(p4-p7, levels = design)
fit2 <- contrasts.fit(fit, contrast.matrix)
fit3 <- eBayes(fit2)
tt <- topTable(fit3, adjust="BH", n=nrow(val_expression_data))

write.table(tt, file=paste(output_dir,'normalisation/data/differential_expression_network_p4_p7_validation.txt', sep=""),  quote = FALSE, sep="\t")

p45 <- val_expression_data[,1:6]
p67 <- val_expression_data[,7:12]

p45.genes <- rownames(p45)
p67.genes <- rownames(p67)


plurinet <- paste(base_dir,'input/pathways/mapped_plurinet_probes.csv',sep="")
ecmr <- paste(base_dir,'input/pathways/mapped_ECM_probes.csv',sep="")

plurinet.genes <- read.table(plurinet, header = F, sep = "\t")
ecmr.genes <- read.table(ecmr, header = F, sep = ",")

plurinet <- plurinet.genes[,2]
plurinet <- unique(plurinet)
plurinet <- as.character(plurinet)

ecmr <- ecmr.genes[,2]
ecmr <- unique(ecmr)

plurinet.p45 <- intersect(plurinet,p45.genes)
