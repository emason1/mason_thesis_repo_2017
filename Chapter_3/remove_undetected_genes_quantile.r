#! /usr/bin/Rscript


#	FILES TO LOAD
qn_objects <- '/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/output/normalisation/quantile_normalisation_workspace.RData'

load(qn_objects)

#	Reset the working directories

base_dir <- '/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/'

output_dir <- paste(base_dir,'/output/detection_scores/',sep="")


#	REMOVE UNDETECTED GENES BY PHENOTYPE

#	Plot and output the detection scores
detect.scores <- detection(dat.raw)

#	Set the threshold for each phenotype
detected_genes_cfib <- function(x){ sum(x <= detect.cutoff) >= 9 }
detected_genes_ciPS <- function(x){ sum(x <= detect.cutoff) >= 12 }

#	Assign a detection cutoff
detect.cutoff <- 1/100

#	List the genes in each phenotype that are detected above the cutoff according to the threshold
list.genes.detect <- list() 
list.genes.detect[[1]] <- rownames(lumi.norm)[apply(detect.scores[,cell.type == "Cfib"], 1, detected_genes_cfib)]
list.genes.detect[[2]] <- rownames(lumi.norm)[apply(detect.scores[,cell.type == "CiPSC"], 1, detected_genes_ciPS)]

detect.genes <- unique(unlist(list.genes.detect))

exprs.dat.detect <- lumi.norm[rownames(lumi.norm) %in% detect.genes,]

exprs.dat.detect <- log2(exprs.dat.detect)  

#	OUTPUT FILES
#	Raw detection scores
write.table(detect.scores, file = paste(output_dir,"raw_detection_scores.txt",sep=""), sep="\t", row.names=TRUE, col.names=TRUE, quote=FALSE)
write.table(exprs.dat.detect, file = paste(base_dir,"output/normalisation/data/quantile_normalised_data_undetected_genes_removed.txt",sep=""), sep="\t", row.names=TRUE, col.names=TRUE, quote=FALSE)

#	PLOTS
#	Raw detection scores
bitmap(file = paste(output_dir,"raw_detection_scores.png",sep=""), type="png16m", height=1000, width = 1000, units = "px")
	plot(detect.scores[,1], lumi.norm[,1], xlab="Detection Score", ylab="Expression", main="Raw Detection Scores")
dev.off()

#	Normalised data with undetected genes removed
bitmap(file = paste(base_dir,"output/normalisation/qc_plots/PCA_detected_genes_only.png",sep=""), type="png16m", height=1000, width = 1000, units = "px")
	plotSampleRelation(exprs.dat.detect, method='mds', cv.Th=0.1, main = "PCA: Quantile normalisation and undetected genes removed", col=cell.type)
dev.off()

bitmap(file = paste(base_dir,"output/normalisation/qc_plots/Density_detected_genes_only.png",sep=""), type="png16m", height=1000, width = 1000, units = "px")
	plotDensity(exprs.dat.detect, main = "Density: Quantile normalisation and undetected genes removed", legend = FALSE)
dev.off()

bitmap(file = paste(base_dir,"output/normalisation/qc_plots/HC_detected_genes_only.png",sep=""), type="png16m", height=1000, width = 1000, units = "px")
	plotSampleRelation(exprs.dat.detect, method="cluster", main = "HC: Quantile normalisation and undetected genes removed")
dev.off()

