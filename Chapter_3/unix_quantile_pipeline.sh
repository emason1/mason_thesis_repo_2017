
#! /bin/sh

#   MAKE RELEVANT DIRECTORIES

cd /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation

#	Remove the output directory and all its contents
rm -r output/

#	Make all the new output file directories

mkdir output/attract/correlated_partners_lists -p

mkdir output/attract/pathway_ranks

mkdir output/attract/synexpression_lists

mkdir output/attract/synexpression_plots

mkdir output/attract/correlation_matrices

mkdir output/cytoscape_input_files

mkdir output/detection_scores

mkdir output/network_analysis/chi_square -p

mkdir output/network_analysis/cov_analysis

mkdir output/network_analysis/degree_analysis

mkdir output/network_analysis/plots

mkdir output/normalisation/qc_plots -p

mkdir output/normalisation/data

mkdir output/mapped_expression_data

#   NORMALISATION

cd /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation

chmod +x /home/lizzi/workspace/wells_lab/2012_library/normalisation_quantile.r

/home/lizzi/workspace/wells_lab/2012_library/normalisation_quantile.r

#   REMOVE UNDETECTED GENES

chmod +x /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/scripts/remove_undetected_genes_quantile.r

/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/scripts/remove_undetected_genes_quantile.r

#   ATTRACT

chmod +x /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/scripts/attract_quantile.r

/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/scripts/attract_quantile.r

#   MAP TO OGS LEVEL

chmod +x /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/scripts/mapping_ens63.py

/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/scripts/mapping_ens63.py

#   REMOVE DELIMITERS FROM FILE

sed -i -r -e "s/\[//g" /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/output/mapped_expression_data/mapped_expression_dataset.csv
sed -i -r -e "s/\]//g" /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/output/mapped_expression_data/mapped_expression_dataset.csv
sed -i -r -e "s/'//g" /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/output/mapped_expression_data/mapped_expression_dataset.csv


#   NORMALISE VALIDATION DATA

chmod +x /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/scripts/validation_dataset_processing.r

/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/scripts/validation_dataset_processing.r

#   MAP THE VALIDATION DATA TO OGS

chmod +x /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/scripts/mapping_ens63_validation_dataset.py

/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/scripts/mapping_ens63_validation_dataset.py

sed -i -r -e "s/\[//g" /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/output/mapped_expression_data/mapped_expression_validation_dataset.csv
sed -i -r -e "s/\]//g" /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/output/mapped_expression_data/mapped_expression_validation_dataset.csv
sed -i -r -e "s/'//g" /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/output/mapped_expression_data/mapped_expression_validation_dataset.csv


#   NETWORK CONSTRUCTION AND ANALYSIS

chmod +x /home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/scripts/network_ens63.r

/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/scripts/network_ens63.r
