#! /usr/bin/Rscript

library(stats)

base_dir <- '/home/lizzi/workspace/wells_lab/analysis/2013_7_network_paper/'
input_dir <- paste(base_dir,'output/mapped_expression_data/',sep="")
output_dir <- paste(base_dir,'output/',sep="")

dat <- paste(input_dir,'mapped_expression_dataset.csv',sep="")
cols <- paste(input_dir,'header.csv',sep="")

val.dat <- paste(input_dir,'mapped_expression_validation_dataset.csv',sep="")
val.cols <- paste(input_dir,'header_validation.csv',sep="")

ips.val.dat <- '/home/lizzi/workspace/wells_lab/analysis/2012_12_vitale_validation/output/mapped_expression_data/IPS_mapped_expression_dataset.csv'
ips.val.cols <- '/home/lizzi/workspace/wells_lab/analysis/2012_12_vitale_validation/output/mapped_expression_data/IPS_header.csv'

hes.val.dat <- '/home/lizzi/workspace/wells_lab/analysis/2012_12_vitale_validation/output/mapped_expression_data/HES_mapped_expression_dataset.csv'
hes.val.cols <- '/home/lizzi/workspace/wells_lab/analysis/2012_12_vitale_validation/output/mapped_expression_data/HES_header.csv'

#   Bind network
bind <- paste(base_dir,'input/pathways/my_bind_network.csv',sep="")
bind_clique <- paste(base_dir,'input/pathways/bind_clique.txt',sep="")

#   String network

string.clique <- read.table(paste(base_dir,"output/cytoscape_input_files/string_clique.txt",sep=""), header=F, sep="\t")
string.disjoint <- read.table(paste(base_dir,"output/cytoscape_input_files/string_disjoint.txt",sep=""), header=F, sep="\t")

#   Coexpression network

dis <- paste(base_dir,"output/cytoscape_input_files/disjoint.txt",sep="")
hub <- paste(base_dir,"output/cytoscape_input_files/clique.txt",sep="")


#   Load files

expression_data <- read.table(dat, header = F, sep = ",")
cnames <- read.table(cols, header = F, sep = ",")
cnames <- as.character(cnames$V1)
colnames(expression_data) <- cnames
rnames <- as.character(expression_data$gene)
rownames(expression_data) <- rnames

if ( identical(rnames, as.character(rownames(expression_data))) ) {
    expression_data <- expression_data[,-1]
}

val_expression_data <- read.table(val.dat, header = F, sep = ",")
cnames <- read.table(val.cols, header = F, sep = ",")
cnames <- as.character(cnames$V1)
colnames(val_expression_data) <- cnames
rnames <- as.character(val_expression_data$gene)
rownames(val_expression_data) <- rnames

if ( identical(rnames, as.character(rownames(val_expression_data))) ) {
    val_expression_data <- val_expression_data[,-1]
}


ips_data <- read.table(ips.val.dat, header = F, sep = ",")
cnames <- read.table(ips.val.cols, header = F, sep = ",")
cnames <- as.character(cnames$V1)
colnames(ips_data) <- cnames
rnames <- as.character(ips_data$gene)
rownames(ips_data) <- rnames

if ( identical(rnames, as.character(rownames(ips_data))) ) {
    ips_data <- ips_data[,-1]
}

hes_data <- read.table(hes.val.dat, header = F, sep = ",")
cnames <- read.table(hes.val.cols, header = F, sep = ",")
cnames <- as.character(cnames$V1)
colnames(hes_data) <- cnames
rnames <- as.character(hes_data$gene)
rownames(hes_data) <- rnames

if ( identical(rnames, as.character(rownames(hes_data))) ) {
    hes_data <- hes_data[,-1]
}


############################################################################
#   NETWORK CONSTRUCTION

correlation.matrix <- t(expression_data)
correlation <- cor(correlation.matrix, method = 'pearson')

matrixFromList <- function(listX) t(sapply(listX, function(x, n) c(x, rep(NA, n))[1:n], n = max(sapply(listX, length)))) 
cnames <- c("GENE_A", "GENE_B", "PEARSON", "EDGE_TYPE")

#   These cutoffs discover a network with 2600 edges
cutoff <- 0.995
neg.cutoff <- -0.995
temp <- list()
cols <- colnames(correlation)
rows <- rownames(correlation)

for ( i in 1:nrow(correlation) ) {
    for ( j in 1:ncol(correlation) ) {
        if ( i != j ) {
            if ( correlation[i, j] >= cutoff ) {
                temp[[length(temp) + 1]]= c(rows[i], cols[j], correlation[i,j], "pos")
            } else { if ( correlation[i, j] <= neg.cutoff ) {
                temp[[length(temp) + 1]]= c(rows[i], cols[j], correlation[i,j], "neg")
                }
			}
        }
	}
}

#   Format the network according to Cytoscape
network_1_with_duplicates <- matrixFromList(temp)
network_1_nodes <- t(apply(network_1_with_duplicates[,1:2], 1, sort))
network_1 <- unique(cbind(network_1_nodes, network_1_with_duplicates[,3:4]))
colnames(network_1) <- cnames

network_nodes <- sort(unique(as.character(network_1[,1:2])))

write.table(network_1, file=paste(output_dir,'cytoscape_input_files/correlation_network_ens63.txt', sep=""),  quote = FALSE, row.names=FALSE, col.names=TRUE, sep="\t")
write.table(network_nodes, file=paste(output_dir,'cytoscape_input_files/correlation_network_node_list_ens63.txt', sep=""),  quote = FALSE, row.names=FALSE, col.names=TRUE, sep="\t")



########################################################################
#   Process the datasets
########################################################################

#   Calculate mean and coefficient of variation for the Briggs iPS dataset

co.var <- function(x)(100*sd(x)/mean(x))

ips <- expression_data[,13:30]
ips.iso <- ips[,c("CiPSC1", "CiPSC2", "CiPSC3", "CiPSC10", "CiPSC11", "CiPSC12", "CiPSC13", "CiPSC14", "CiPSC15")]

ips$coVar <- apply(ips, 1, co.var)
ips.covar <- ips$coVar
ips$Mean <- apply(ips[,1:18], 1, mean)
ips.mean <- ips$Mean
ips.exprs <- ips

ips.iso$coVar <- apply(ips.iso, 1, co.var)
ips.iso.covar <- ips.iso$coVar
ips.iso$Mean <- apply(ips.iso[,1:9], 1, mean)
ips.iso.mean <- ips.iso$Mean
ips.iso.exprs <- ips.iso

reorder.by.network <- as.character(network_nodes)

ips <- ips[rownames(ips) %in% network_nodes,]
ips <- ips[reorder.by.network,]

ips.iso <- ips.iso[rownames(ips.iso) %in% network_nodes,]
ips.iso <- ips.iso[reorder.by.network,]

#   Input the BIND network, degree of connectivity, covar and mean

bind_data <- read.table(bind, header = T, sep = "\t")
rnames <- as.character(bind_data$Bind_Gene)
rownames(bind_data) <- rnames
reorder.bind <- rnames

bind_ips <- ips[rownames(ips) %in% rnames,]
bind_ips <- bind_ips[reorder.bind,]

b <- cbind(bind_data, bind_ips$coVar, bind_ips$Mean)
cnames <- c("Bind_Degree", "Bind_coVar", "Bind_Mean")
colnames(b) <- cnames

if ( identical(rownames(b), as.character(b$Bind_Gene)) ) {
    b <- b[,-1]
}
b <- b[rowSums(is.na(b))==0, ]


#   Calculate mean and coefficient of variation in the Vitale datasets

ips_low <- ips_data[,6:9]
ips_low$coVar <- apply(ips_low, 1, co.var)
ips_low.covar <- ips_low$coVar
ips_low$Mean <- apply(ips_low[,1:4], 1, mean)
ips.low.mean <- ips_low$Mean
ips.low.exprs <- ips_low
ips_low <- ips_low[rownames(ips_low) %in% network_nodes,]
ips_low <- ips_low[reorder.by.network,]
ips_low <- ips_low[rowSums(is.na(ips_low))==0, ]

ips_high <- ips_data[,1:5]
ips_high$coVar <- apply(ips_high, 1, co.var)
ips_high.covar <- ips_high$coVar
ips_high$Mean <- apply(ips_high[,1:5], 1, mean)
ips.high.mean <- ips_high$Mean
ips.high.exprs <- ips_high
ips_high <- ips_high[rownames(ips_high) %in% network_nodes,]
ips_high <- ips_high[reorder.by.network,]
ips_high <- ips_high[rowSums(is.na(ips_high))==0, ]


hes_data$coVar <- apply(hes_data, 1, co.var)
hes_covar <- hes_data$coVar
hes_data$Mean <- apply(hes_data[,1:3], 1, mean)
hes_Mean <- hes_data$Mean
hes.exprs <- hes_data
hes_data <- hes_data[rownames(hes_data) %in% network_nodes,]
hes_data <- hes_data[reorder.by.network,]
hes_data <- hes_data[rowSums(is.na(hes_data))==0, ]


#   Calculate mean and cov for P-fractions validation datasets
p67.exprs <- val_expression_data[,7:12]
p67.exprs$coVar <-apply(p67.exprs, 1, co.var)
p67.exprs.coVar <- p67.exprs$coVar
p67.exprs$Mean <- apply(p67.exprs[,1:6], 1, mean)
p67.exprs.mean <- p67.exprs$Mean

p7 <- p67.exprs[,4:6]
p7$coVar <- apply(p7, 1, co.var)
p7$Mean <- apply(p7[,1:3], 1, mean)
p7 <- p7[order(p7$coVar),]

p4 <- val_expression_data[,1:3]
p4$coVar <- apply(p4, 1, co.var)
p4$Mean <- apply(p4[,1:3], 1, mean)
p4 <- p4[order(p4$coVar),]

p5 <- val_expression_data[,4:6]
p5$coVar <- apply(p5, 1, co.var)
p5$Mean <- apply(p5[,1:3], 1, mean)
p5 <- p5[order(p5$coVar),]

p6 <- val_expression_data[,7:9]
p6$coVar <- apply(p6, 1, co.var)
p6$Mean <- apply(p6[,1:3], 1, mean)
p6 <- p6[order(p6$coVar),]

all_pfract <- val_expression_data
all_pfract$coVar <- apply(all_pfract, 1, co.var)
all_pfract$Mean <- apply(all_pfract[,1:12], 1, mean)

hes <- val_expression_data[rownames(val_expression_data) %in% network_nodes,]

hes.p45 <- hes[,1:6]
hes.p45$coVar <-apply(hes.p45, 1, co.var)
hes.p45.coVar <- hes.p45$coVar
hes.p45$Mean <- apply(hes.p45[,1:6], 1, mean)
hes.p45.mean <- hes.p45$Mean

hes.p67 <- hes[,7:12]
hes.p67$coVar <-apply(hes.p67, 1, co.var)
hes.p67.coVar <- hes.p67$coVar
hes.p67$Mean <- apply(hes.p67[,1:6], 1, mean)
hes.p67.mean <- hes.p67$Mean


p45 <- hes[,1:6]
p67 <- hes[,7:12]

p45$coVar <- apply(p45, 1, co.var)
p45 <- p45[reorder.by.network,]
p45 <- p45[rowSums(is.na(p45))==0, ]

p67$coVar <- apply(p67, 1, co.var)
p67 <- p67[reorder.by.network,]
p67 <- p67[rowSums(is.na(p67))==0, ]

########################################################################
#   Genome-wide mean and variance analysis
########################################################################

pretty.col <- rainbow(10)

#   All probes CoV
postscript(file=paste(output_dir,"network_analysis/cov_analysis/box_plot_all_datasets_all_probes_CoV.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(ips.low.exprs$coVar, ips.high.exprs$coVar, ips.exprs$coVar, ips.iso.exprs$coVar, hes.exprs$coVar, all_pfract$coVar, col=c(pretty.col), names=c("iPS low\n (Vitale)", "iPS_high\n (Vitale)", "iPS\n (Briggs)", "iPS\n (isogenic)", "hES\n (Vitale)", "hES P-fractions\n (Hough)"), ylim=c(0,85), ylab="Coefficient of variation", xlab="Dataset", main="Coefficient of variation for all probes\n in each dataset")
dev.off()


#   P fractions Mean
postscript(file=paste(output_dir,"network_analysis/mean_analysis/box_plot_p_fractions_all_probes_mean.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(all_pfract$Mean, p4$Mean, p5$Mean, p6$Mean, p7$Mean, col=c("#003380", "#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"), names=c("hES all samples\n (Hough)", "hES P4\n (Hough)", "hES P5\n (Hough)", "hES P6\n (Hough)", "hES P7\n (Hough)"), ylim=c(0,20), ylab="Coefficient of variation", xlab="Dataset", main="Mean Log(2) expression for all probes\n in P-fractions dataset")
dev.off()

#   P fractions CoV
postscript(file=paste(output_dir,"network_analysis/cov_analysis/box_plot_p_fractions_all_probes_CoV.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(all_pfract$coVar, p4$coVar, p5$coVar, p6$coVar, p7$coVar, col=c("#003380", "#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"), names=c("hES all samples\n (Hough)", "hES P4\n (Hough)", "hES P5\n (Hough)", "hES P6\n (Hough)", "hES P7\n (Hough)"), ylim=c(0,20), ylab="Coefficient of variation", xlab="Dataset", main="Coefficient of variation for all probes\n in P-fractions dataset")
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/density_plot_p_fractions_all_probes_CoV.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
plot(density(log2(all_pfract$coVar)), main="Variance reflects homogeneity of a population", ylim=range(0, 0.5), xlab="Log 2 CoV", ylab="Density", lty=1, lwd=4, col="#003380")
    lines(density(log2(p4$coVar)), lty=1, lwd=4, col='#0055D4')
    lines(density(log2(p5$coVar)), lty=1, lwd=4, col='#5599FF')
    lines(density(log2(p6$coVar)), lty=1, lwd=4, col='#AACCFF')
    lines(density(log2(p7$coVar)), lty=1, lwd=4, col='#D7E3F4')
    legend(x=3, y=0.4, c("All samples", "P4", "P5", "P6", "P7"), lty=c(1, 1, 1, 1, 1), lwd=(4), col=c("#003380", "#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"), text.col=c("#003380", "#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()


postscript(file=paste(output_dir,"network_analysis/cov_analysis/box_plot_p_fractions_network_nodes_CoV.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(p4[rownames(p4) %in% network_nodes,"coVar"], p5[rownames(p5) %in% network_nodes,"coVar"], p6[rownames(p6) %in% network_nodes,"coVar"], p7[rownames(p7) %in% network_nodes,"coVar"], all_pfract[rownames(all_pfract) %in% network_nodes,"coVar"], col=c(pretty.col), names=c("hES P4\n (Hough)", "hES P5\n (Hough)", "hES P6\n (Hough)", "hES P7\n (Hough)", "hES all samples\n (Hough)"), ylim=c(0,20), ylab="Coefficient of variation", xlab="Dataset", main="Coefficient of variation for network nodes\n in P-fractions dataset")
dev.off()

#   All probes mean
postscript(file=paste(output_dir,"network_analysis/mean_analysis/box_plot_all_datasets_all_probes_Mean.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(ips.low.exprs$Mean, ips.high.exprs$Mean, ips.exprs$Mean, ips.iso.exprs$Mean, hes.exprs$Mean, all_pfract$Mean, col=c(pretty.col), names=c("iPS low\n (Vitale)", "iPS_high\n (Vitale)", "iPS\n (Briggs)", "iPS\n (isogenic)", "hES\n (Vitale)", "hES P-fractions\n (Hough)"), ylim=c(0,25), ylab="Mean log2 expression", xlab="Dataset", main="Mean expression of all probes\n in each dataset")
dev.off()


########################################################################
#   Calculate the degree of connectivity in the co-expression network per network node in each dataset
########################################################################

all_nodes_duplicates <- sort(as.character(network_1[,1:2]))

nodes_degree <- vector()

for ( i in network_nodes ){
    degree <- 0
    for ( j in all_nodes_duplicates ){
        if ( i == j ){
            degree <- degree + 1
        }
    }
    nodes_degree <- c(nodes_degree, degree)
}

ips$Degree <- nodes_degree
ips.iso$Degree <- nodes_degree

degree_of_connectivity <- matrixFromList(nodes_degree)

hes$Degree <- ips[rownames(ips) %in% rownames(hes),"Degree"]
p45$Degree <- ips[rownames(ips) %in% rownames(p45),"Degree"]
p67$Degree <- ips[rownames(ips) %in% rownames(p67),"Degree"]
hes_data$Degree <- ips[rownames(ips) %in% rownames(hes_data),"Degree"]
ips_low$Degree <- ips[rownames(ips) %in% rownames(ips_low),"Degree"]
ips_high$Degree <- ips[rownames(ips) %in% rownames(ips_high),"Degree"]

#   Order data by the coVar column

ips <- ips[order(ips$coVar),]
ips.iso <- ips.iso[order(ips.iso$coVar),]
p45 <- p45[order(p45$coVar),]
p67 <- p67[order(p67$coVar),]
ips_low <- ips_low[order(ips_low$coVar),]
ips_high <- ips_high[order(ips_high$coVar),]
hes_data <- hes_data[order(hes_data$coVar),]

#   Break up P-fractions data for network nodes

P4 <- p4[rownames(p4) %in% network_nodes,]
P5 <- p5[rownames(p5) %in% network_nodes,]
P6 <- p6[rownames(p6) %in% network_nodes,]
P7 <- p7[rownames(p7) %in% network_nodes,]
All.P <- all_pfract[rownames(all_pfract) %in% network_nodes,]

######################################################################################################################################################################################################


##################################
#   Coexpression network variance and mean analysis
##################################

#   Boxplots of all CoV values in coexpression network
postscript(file=paste(output_dir,"network_analysis/cov_analysis/cov_boxplots_network_nodes.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    boxplot(ips_low$coVar, ips_high$coVar, ips$coVar, ips.iso$coVar, hes_data$coVar, P4$coVar, P7$coVar, col=c(pretty.col), names=c("iPS low\n (Vitale)", "iPS_high\n (Vitale)", "iPS\n (Briggs)", "iPS\n (isogenic)", "hES\n (Vitale)", "hES P4\n (Hough)", "hES P7\n (Hough)"), ylim=c(0,80), ylab="Coefficient of variation", xlab="Dataset", main="Coefficient of variation\n for all network nodes in each dataset")
dev.off()
#   Boxplots of all Mean values in coexpression network 
postscript(file=paste(output_dir,"network_analysis/mean_analysis/mean_boxplots_network_nodes.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    boxplot(ips_low$Mean, ips_high$Mean, ips$Mean, ips.iso$Mean, hes_data$Mean, P4$Mean, P7$Mean, col=c(pretty.col), names=c("iPS low\n (Vitale)", "iPS_high\n (Vitale)", "iPS\n (Briggs)", "iPS\n (isogenic)", "hES\n (Vitale)", "hES P4\n (Hough)", "hES P7\n (Hough)"), ylim=c(0,25), ylab="Mean (log2) expression", xlab="Dataset", main="Coefficient of variation\n for all network nodes in each dataset")
dev.off()
#   Density plots of all CoV values in coexpression network

postscript(file=paste(output_dir,"network_analysis/cov_analysis/cov_densityplots_network_nodes.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
plot(density(log2(ips_low$coVar)), main="CoV Network Nodes", ylim=range(0, 0.5), xlab="Log 2 CoV", ylab="Density", lty=1, lwd=4, col="#FF0000FF")
    lines(density(log2(ips_high$coVar)), lty=1, lwd=4, col='#FF9900FF')
    lines(density(log2(ips$coVar)), lty=1, lwd=4, col='#CCFF00FF')
    lines(density(log2(ips.iso$coVar)), lty=1, lwd=4, col='#33FF00FF')
    lines(density(log2(hes_data$coVar)), lty=1, lwd=4, col='#00FF66FF')
    lines(density(log2(P4$coVar)), lty=1, lwd=4, col='#00FFFFFF')
    lines(density(log2(P7$coVar)), lty=1, lwd=4, col='#0066FFFF')
    legend(x=5, y=0.5, c("iPS low", "iPS_high", "iPS", "iPS isogenic", "hES", "hES P4", "hES P7"), lty=c(1, 1, 1, 1, 1), lwd=(4), col=c("#FF0000FF", "#FF9900FF", "#CCFF00FF", "#33FF00FF", "#00FF66FF", "#00FFFFFF", "#0066FFFF"), text.col=c("#FF0000FF", "#FF9900FF", "#CCFF00FF", "#33FF00FF", "#00FF66FF", "#00FFFFFF", "#0066FFFF"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/cov_densityplots_network_nodes_pfractions.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
plot(density(log2(All.P$coVar)), main="CoV Network Nodes", xlab="Log 2 CoV", ylab="Density", lty=1, lwd=4, col="red")
    lines(density(log2(P5$coVar)), lty=1, lwd=4, col='#FF9900FF')
    lines(density(log2(P6$coVar)), lty=1, lwd=4, col= '#CCFF00FF')
    lines(density(log2(P4$coVar)), lty=1, lwd=4, col='#00FFFFFF')
    lines(density(log2(P7$coVar)), lty=1, lwd=4, col='#0066FFFF')
    legend(x=3, y=0.4, c("All Pfract", "P4", "P5", "P6", "P7"), lty=c(1, 1, 1, 1, 1), lwd=(4), col=c("red", "#FF9900FF", '#CCFF00FF', "#00FFFFFF", "#0066FFFF"), text.col=c("red", "#FF9900FF", '#CCFF00FF', "#00FFFFFF", "#0066FFFF"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/cov_densityplots_network_nodes_iPS.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    plot(density(log2(ips$coVar)), main="CoV Network Nodes iPS", xlab="Log 2 CoV", ylab="Density", lty=1, lwd=4, col="red")
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/cov_densityplots_network_nodes_iPS_iso.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    plot(density(log2(ips.iso$coVar)), main="CoV Network Nodes iPS isogenic", xlab="Log 2 CoV", ylab="Density", lty=1, lwd=4, col="red")
dev.off()


########################################################################
#   Degree by looking at Network
########################################################################
#   For the BIND network
clique <- read.table(bind_clique, header=F, sep="\t")
clique <- as.character(clique[,1])

#   For the STRING network
s.c <- as.character(string.clique[,1])
s.d <- as.character(string.disjoint[,1])
s.all <- c(s.c,s.d)

#   For the COEXPRESSION network
#   Identify the disconnected/hubb/periphery in the coexpression network
disconnected <- read.table(dis, header=T, sep="\t")
hubb <- read.table(hub, header=T, sep="\t")

dis.nodes <- as.character(disconnected[,1])
hubb.nodes <- as.character(hubb[,1])
nodes <- c(dis.nodes,hubb.nodes)

############################
#   iPS unrelated (Briggs) phenotype
#   "dec" is a generic (reusable) variable for the dataset
############################

dec <- ips

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

C <- dec[rownames(dec) %in% clique,]
Dis <- dec[!(rownames(dec) %in% clique),]

SC <- dec[rownames(dec) %in% s.c,]
SD <- dec[rownames(dec) %in% s.d,]
SL <- dec[!(rownames(dec)) %in% s.all,]


#   Write each list out to file for GO analysis
write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/iPS_Briggs/periphery_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/iPS_Briggs/disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/iPS_Briggs/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")


postscript(file = paste(output_dir,"network_analysis/degree_analysis/iPS_Briggs/CoV_Boxplot_by_network_location_iPS.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylab="Coefficient of variation", xlab="Location in network", main="iPS coefficient of variation\n for location in network")
dev.off()

postscript(file = paste(output_dir,"network_analysis/degree_analysis/iPS_Briggs/CoV_Boxplot_by_network_location_iPS_BIND_NETWORK.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(Dis$coVar, C$coVar, col=c("red", "deepskyblue"), names=c("Disjoint\n N=1095", "Clique\n N=55"), ylab="Coefficient of variation", xlab="Location in network", main="iPS coefficient of variation\n for location in BIND network")
dev.off()

postscript(file = paste(output_dir,"network_analysis/degree_analysis/iPS_Briggs/CoV_Boxplot_by_network_location_iPS_STRING_NETWORK.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(SD$coVar, SL$coVar, SC$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disjoint\n N=44", "Leaf\n N=1003", "Clique\n N=103"), ylab="Coefficient of variation", xlab="Location in network", main="iPS coefficient of variation\n for location in STRING network")
dev.off()

#   Mann-Whitney-Wilcoxin test (non-parametric: differences in distribution and sample size)

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

DC.test <- wilcox.test(Dis$coVar, C$coVar)

DL.test <- wilcox.test(SD$coVar, SL$coVar)
DC.test <- wilcox.test(SD$coVar, SC$coVar)
CL.test <- wilcox.test(SC$coVar, SL$coVar)


#   DP p.val = 0.0009642
#   PH p.val = 6.619e-13
#   DH p.val = 0.002347

#   DC p.val = 2.533e-05

#   DL p.val = 0.00323
#   DC p.val = 6.688e-08
#   CL p.val =  2.461e-07

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/iPS_Briggs/CoV_Density_by_network_location_iPS.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(log2(D$coVar), kernel = c("gaussian")), main="Density plots for CoV of all co-expression network nodes\n in iPS Briggs dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(log2(P$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="gray")
		lines(density(log2(H$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="deepskyblue")
        legend(x=25, y=0.25, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 3.849e-08

############################
#   iPS Isogenic (Briggs) phenotype
############################


dec <- ips.iso

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

#   Write each list out to file for GO analysis
write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/iPS_Briggs/periphery_iPS_iso.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/iPS_Briggs/disconnected_iPS_iso.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/iPS_Briggs/hubb_iPS_iso.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")


postscript(file = paste(output_dir,"network_analysis/degree_analysis/iPS_Briggs/CoV_Boxplot_by_network_location_iPS_iso.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylab="Coefficient of variation", xlab="Location in network", main="iPS coefficient of variation\n for location in network")
dev.off()

#   Mann-Whitney-Wilcoxin test (non-parametric: differences in distribution and sample size)

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = W = 27137.5, p-value = 0.0005741
#   PH p.val = W = 5607, p-value = 7.824e-11
#   DH p.val = W = 57138, p-value = 0.00101

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/iPS_Briggs/CoV_Density_by_network_location_iPS_iso.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(log2(D$coVar), kernel = c("gaussian")), main="Density plots for CoV of all co-expression network nodes\n in isogenic iPS Briggs dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(log2(P$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="gray")
		lines(density(log2(H$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="deepskyblue")
        legend(x=25, y=0.25, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 6.172e-07


############################
#   All P-factions  (Hough) phenotype
############################

dec <- all_pfract[rownames(all_pfract) %in% network_nodes,]

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

apf.p <- P
apf.d <- D
apf.h <- H

write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/hES_all_pfractions/all_p_fract_leaf_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/hES_all_pfractions/all_p_fract_disjoint_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/hES_all_pfractions/all_p_fract_clique_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")

postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_all_pfractions/degree_by_network_location_all_p_fract.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="hES all fractions coefficient of variation\n for location in network")
dev.off()

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = 9.255e-09
#   PH p.val = 0.1465
#   DH p.val = 1.289e-13

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/hES_all_pfractions/CoV_Density_by_network_location_all_pfract.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(D$coVar), kernel = c("gaussian"), main="Density plots for mean expression of all probes\n in all P fractions Hough dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(P$coVar), kernel = c("gaussian"), lty=1, lwd=4, col="gray")
		lines(density(H$coVar), kernel = c("gaussian"), lty=1, lwd=4, col="deepskyblue")
        legend(x=6, y=0.4, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 

############################
#   Assess P4 and P7 (Hough) phenotypes together
############################

dec <- p4[rownames(p4) %in% network_nodes,]
P.4 <- dec[!(rownames(dec) %in% nodes),]
D.4 <- dec[rownames(dec) %in% dis.nodes,]
H.4 <- dec[rownames(dec) %in% hubb.nodes,]

dec <- p7[rownames(p7) %in% network_nodes,]
P.7 <- dec[!(rownames(dec) %in% nodes),]
D.7 <- dec[rownames(dec) %in% dis.nodes,]
H.7 <- dec[rownames(dec) %in% hubb.nodes,]

postscript(file=paste(output_dir,"network_analysis/degree_analysis/hES_P4_Hough/CoV_Density_by_network_location_P4_P7.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(log2(D.4$coVar)), kernel = c("gaussian"), ylim=range(0, 0.5), xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(log2(H.4$coVar)), kernel = c("gaussian"), lty=1, lwd=4, col="deepskyblue")
		lines(density(log2(D.7$coVar)), kernel = c("gaussian"), lty=1, lwd=4, col="orange")
		lines(density(log2(H.7$coVar)), kernel = c("gaussian"), lty=1, lwd=4, col="darkblue")
        legend(x=2, y=0.5, c("Disjoint P4", "Disjoint P7", "Clique P4", "Clique P7"), lty=c(1, 1, 1), lwd=(4), col=c("red", "orange", "deepskyblue", "darkblue"), text.col=c("red", "orange", "deepskyblue", "darkblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D.4$coVar, D.7$coVar)
#   Dijoint group P4-P7 p.val 2.2e-16

test <- ks.test(H.4$coVar, H.7$coVar)
#   Clique group P4-P7 p.val 2.997e-09


############################
#   P4 phenotype (Hough)
############################

dec <- p4[rownames(p4) %in% network_nodes,]
P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

p4.p <- P
p4.d <- D
p4.h <- H

#   Write each list out to file for GO analysis
write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/hES_P4_Hough/periphery_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/hES_P4_Hough/disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/hES_P4_Hough/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")


postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_P4_Hough/degree_by_network_location_p4.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=863", "Periphery\n N=72", "Hubb\n N=91"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="hES P4 coefficient of variation\n for location in network")
dev.off()

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = 0.001026
#   PH p.val = 0.2348
#   DH p.val = 1.496e-08

postscript(file=paste(output_dir,"network_analysis/degree_analysis/hES_P4_Hough/CoV_Density_by_network_location_P4.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(D$coVar), kernel = c("gaussian"), main="Density plots for mean expression of all probes\n in P4 Hough dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(P$coVar), kernel = c("gaussian"), lty=1, lwd=4, col="gray")
		lines(density(H$coVar), kernel = c("gaussian"), lty=1, lwd=4, col="deepskyblue")
        legend(x=6, y=0.4, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 2.791e-08


############################
#   P5 (Hough) phenotype
############################

dec <- p5[rownames(p5) %in% network_nodes,]

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

p5.p <- P
p5.d <- D
p5.h <- H

postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_P5_Hough/degree_by_network_location_p5.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="hES P7 coefficient of variation\n for location in network")
dev.off()

############################
#   P6 (Hough) phenotype
############################

dec <- p6[rownames(p6) %in% network_nodes,]

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

p6.p <- P
p6.d <- D
p6.h <- H

postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_P6_Hough/degree_by_network_location_p6.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="hES P7 coefficient of variation\n for location in network")
dev.off()

############################
#   P7 (Hough) phenotype
############################

dec <- p7[rownames(p7) %in% network_nodes,]

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

p7.p <- P
p7.d <- D
p7.h <- H

write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/hES_P7_Hough/periphery_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/hES_P7_Hough/disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/hES_P7_Hough/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")

postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_P7_Hough/degree_by_network_location_p7.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="hES P7 coefficient of variation\n for location in network")
dev.off()

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = 0.0189
#   PH p.val = 0.2994
#   DH p.val = 0.2328

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/hES_P7_Hough/CoV_Density_by_network_location_P7.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(D$coVar), kernel = c("gaussian"), main="Density plots for mean expression of all probes\n in P7 Hough dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(P$coVar), kernel = c("gaussian"), lty=1, lwd=4, col="gray")
		lines(density(H$coVar), kernel = c("gaussian"), lty=1, lwd=4, col="deepskyblue")
        legend(x=6, y=0.4, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 0.2287


############################
#   Clique/Leaf/Disjoint boxplots
############################

postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_all_pfractions/clique_boxplots.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(p4.h$coVar, p5.h$coVar, p6.h$coVar, p7.h$coVar, col=c("deepskyblue", "deepskyblue", "deepskyblue", "deepskyblue"), names=c("P4", "P5", "P6", "P7"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="Clique coefficient of variation\n for each P fraction")
dev.off()

p4p5.test <- wilcox.test(p4.h$coVar, p5.h$coVar)
p5p6.test <- wilcox.test(p5.h$coVar, p6.h$coVar)
p6p7.test <- wilcox.test(p6.h$coVar, p7.h$coVar)
p4p7.test <- wilcox.test(p4.h$coVar, p7.h$coVar)


postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_all_pfractions/disjoint_boxplots.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(p4.d$coVar, p5.d$coVar, p6.d$coVar, p7.d$coVar, col=c("red", "red", "red", "red"), names=c("P4", "P5", "P6", "P7"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="Disjoint coefficient of variation\n for each P fraction")
dev.off()

p4p5.test <- wilcox.test(p4.d$coVar, p5.d$coVar)
p5p6.test <- wilcox.test(p5.d$coVar, p6.d$coVar)
p6p7.test <- wilcox.test(p6.d$coVar, p7.d$coVar)
p4p7.test <- wilcox.test(p4.d$coVar, p7.d$coVar)


postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_all_pfractions/leaf_boxplots.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(p4.p$coVar, p5.p$coVar, p6.p$coVar, p7.p$coVar, col=c("gray", "gray", "gray", "gray"), names=c("P4", "P5", "P6", "P7"), ylim=c(0,10), ylab="Coefficient of variation", xlab="Location in network", main="Leaf coefficient of variation\n for each P fraction")
dev.off()

p4p5.test <- wilcox.test(p4.p$coVar, p5.p$coVar)
p5p6.test <- wilcox.test(p5.p$coVar, p6.p$coVar)
p6p7.test <- wilcox.test(p6.p$coVar, p7.p$coVar)
p4p7.test <- wilcox.test(p4.p$coVar, p7.p$coVar)

############################
#   iPS_low phenotype (Vitale)
############################


dec <- ips_low

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/iPS_low_Vitale/periphery_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/iPS_low_Vitale/disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/iPS_low_Vitale/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")

postscript(file = paste(output_dir,"network_analysis/degree_analysis/iPS_low_Vitale/degree_by_network_location_vitale_IPS_low.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylab="Coefficient of variation", xlab="Location in network", main="iPS low (Vitale) coefficient of variation\n for location in network")
dev.off()

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = 2.636e-13
#   PH p.val = 0.7009
#   DH p.val = 3.03e-16

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/iPS_low_Vitale/CoV_Density_by_network_location_iPS_low.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(log2(D$coVar), kernel = c("gaussian")), main="Density plots for mean expression of all probes\n in iPS_low Vitale dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(log2(P$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="gray")
		lines(density(log2(H$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="deepskyblue")
        legend(x=30, y=0.1, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 1.243e-14

############################
#   iPS_high (Vitale) phenotype
############################

dec <- ips_high

P <- dec[!(rownames(dec) %in% nodes),]
D <- dec[rownames(dec) %in% dis.nodes,]
H <- dec[rownames(dec) %in% hubb.nodes,]

write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/iPS_high_Vitale/periphery_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/iPS_high_Vitale/disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/iPS_high_Vitale/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")

postscript(file = paste(output_dir,"network_analysis/degree_analysis/iPS_high_Vitale/degree_by_network_location_vitale_IPS_high.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=982", "Periphery\n N=73", "Hubb\n N=97"), ylab="Coefficient of variation", xlab="Location in network", main="iPS high (Vitale) coefficient of variation\n for location in network")
dev.off()

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = 1.834e-07
#   PH p.val = 0.0001151
#   DH p.val = 0.1941

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/iPS_high_Vitale/CoV_Density_by_network_location_iPS_high.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(log2(D$coVar), kernel = c("gaussian")), main="Density plots for mean expression of all probes\n in iPS_high Vitale dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(log2(P$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="gray")
		lines(density(log2(H$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="deepskyblue")
        legend(x=30, y=0.1, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value = 0.009821

############################
#   hES (Vitale) phenotype
############################

dec <- hes_data

P <- dec[!(rownames(dec) %in% nodes),4:5]
D <- dec[rownames(dec) %in% dis.nodes,4:5]
H <- dec[rownames(dec) %in% hubb.nodes,4:5]

write.table(P, file=paste(output_dir,'network_analysis/degree_analysis/hES_Vitale/periphery_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(D, file=paste(output_dir,'network_analysis/degree_analysis/hES_Vitale/disconnected_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
write.table(H, file=paste(output_dir,'network_analysis/degree_analysis/hES_Vitale/hubb_iPS.txt', sep=""),  quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")

postscript(file = paste(output_dir,"network_analysis/degree_analysis/hES_Vitale/degree_by_network_location_vitale_HES.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
boxplot(D$coVar, P$coVar, H$coVar, col=c("red", "gray", "deepskyblue"), names=c("Disconnected\n N=963", "Periphery\n N=73", "Hubb\n N=97"), ylab="Coefficient of variation", xlab="Location in network", main="hES (Vitale) coefficient of variation\n for location in network")
dev.off()

DP.test <- wilcox.test(D$coVar, P$coVar)
PH.test <- wilcox.test(P$coVar, H$coVar)
DH.test <- wilcox.test(D$coVar, H$coVar)

#   DP p.val = 0.0001619
#   PH p.val = 0.0001623
#   DH p.val =  0.5609

#   Density plots
postscript(file=paste(output_dir,"network_analysis/degree_analysis/hES_Vitale/CoV_Density_by_network_location_hES_Vitale.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density(log2(D$coVar), kernel = c("gaussian")), main="Density plots for mean expression of all probes\n in hES Vitale dataset", xlab="Log2 coefficient of variation", lty=1, lwd=4, col="red")
		lines(density(log2(P$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="gray")
		lines(density(log2(H$coVar), kernel = c("gaussian")), lty=1, lwd=4, col="deepskyblue")
        legend(x=30, y=0.2, c("Disconnected", "Periphery", "Hub"), lty=c(1, 1, 1), lwd=(4), col=c("red", "gray", "deepskyblue"), text.col=c("red", "gray", "deepskyblue"))
dev.off()
#   Kolmogorov-Smirnov test used to test degree distributions of disconnected and hubb variance gene sets
test <- ks.test(D$coVar, H$coVar)
#   P.value =  0.1735


########################################################################
#
#   Genes that change across different datasets
#
########################################################################

p6 <- p6[rownames(p6) %in% network_nodes,]
p5 <- p5[rownames(p5) %in% network_nodes,]


#   Low variance/clique
#   OCT4
oct4.average <- c(p4["POU5F1","Mean"], p5["POU5F1","Mean"], p6["POU5F1","Mean"], p7["POU5F1","Mean"])
oct4.cov <- c(p4["POU5F1","coVar"], p5["POU5F1","coVar"], p6["POU5F1","coVar"], p7["POU5F1","coVar"])
oct4.average.ips <- c(ips_low["POU5F1","Mean"], ips_high["POU5F1","Mean"], ips["POU5F1","Mean"], hes_data["POU5F1","Mean"])
oct4.cov.ips <- c(ips_low["POU5F1","coVar"], ips_high["POU5F1","coVar"], ips["POU5F1","coVar"], hes_data["POU5F1","coVar"])


postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_pou5f1.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(oct4.average, main="Average expression of POU5F1", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_pou5f1.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(oct4.cov, main="Variance in expression of POU5F1", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_pou5f1_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(oct4.average.ips, main="Average expression of POU5F1", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_pou5f1_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(oct4.cov.ips, main="Variance in expression of POU5F1", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

#   CLDN6
cldn6.average <- c(p4["CLDN6","Mean"], p5["CLDN6","Mean"], p6["CLDN6","Mean"], p7["CLDN6","Mean"])
cldn6.cov <- c(p4["CLDN6","coVar"], p5["CLDN6","coVar"], p6["CLDN6","coVar"], p7["CLDN6","coVar"])
cldn6.average.ips <- c(ips_low["CLDN6","Mean"], ips_high["CLDN6","Mean"], ips["CLDN6","Mean"], hes_data["CLDN6","Mean"])
cldn6.cov.ips <- c(ips_low["CLDN6","coVar"], ips_high["CLDN6","coVar"], ips["CLDN6","coVar"], hes_data["CLDN6","coVar"])

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_cldn6.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(cldn6.average, main="Average expression of CLDN6", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_cldn6.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(cldn6.cov, main="Variance in expression of CLDN6", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_cldn6_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(cldn6.average.ips, main="Average expression of CLDN6", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_cldn6_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(cldn6.cov.ips, main="Variance in expression of CLDN6", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()


#   SOX2
sox2.average <- c(p4["SOX2","Mean"], p5["SOX2","Mean"], p6["SOX2","Mean"], p7["SOX2","Mean"])
sox2.cov <- c(p4["SOX2","coVar"], p5["SOX2","coVar"], p6["SOX2","coVar"], p7["SOX2","coVar"])
sox2.average.ips <- c(ips_low["SOX2","Mean"], ips_high["SOX2","Mean"], ips["SOX2","Mean"], hes_data["SOX2","Mean"])
sox2.cov.ips <- c(ips_low["SOX2","coVar"], ips_high["SOX2","coVar"], ips["SOX2","coVar"], hes_data["SOX2","coVar"])

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_sox2.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(sox2.average, main="Average expression of SOX2", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_sox2.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(sox2.cov, main="Variance in expression of SOX2", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_sox2_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(sox2.average.ips, main="Average expression of SOX2", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_sox2_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(sox2.cov.ips, main="Variance in expression of SOX2", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

#   DNMT3B
dnmt3b.average <- c(p4["DNMT3B","Mean"], p5["DNMT3B","Mean"], p6["DNMT3B","Mean"], p7["DNMT3B","Mean"])
dnmt3b.cov <- c(p4["DNMT3B","coVar"], p5["DNMT3B","coVar"], p6["DNMT3B","coVar"], p7["DNMT3B","coVar"])
dnmt3b.average.ips <- c(ips_low["DNMT3B","Mean"], ips_high["DNMT3B","Mean"], ips["DNMT3B","Mean"], hes_data["DNMT3B","Mean"])
dnmt3b.cov.ips <- c(ips_low["DNMT3B","coVar"], ips_high["DNMT3B","coVar"], ips["DNMT3B","coVar"], hes_data["DNMT3B","coVar"])


postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_dnmt3b.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(dnmt3b.average, main="Average expression of DNMT3B", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_dnmt3b.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(dnmt3b.cov, main="Variance in expression of DNMT3B", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_dnmt3b_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(dnmt3b.average.ips, main="Average expression of DNMT3B", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_dnmt3b_ips.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(dnmt3b.cov.ips, main="Variance in expression of DNMT3B", names.arg=c("iPS_low", "iPS_high", "iPS", "hES"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

#   High variance/disjoint
MST1.average <- c(p4["MST1","Mean"], p5["MST1","Mean"], p6["MST1","Mean"], p7["MST1","Mean"])
MST1.cov <- c(p4["MST1","coVar"], p5["MST1","coVar"], p6["MST1","coVar"], p7["MST1","coVar"])

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_MST1.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(MST1.average, main="Average expression of MST1", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_MST1.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(MST1.cov, main="Variance in expression of MST1", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

CDK11A.average <- c(p4["CDK11A","Mean"], p5["CDK11A","Mean"], p6["CDK11A","Mean"], p7["CDK11A","Mean"])
CDK11A.cov <- c(p4["CDK11A","coVar"], p5["CDK11A","coVar"], p6["CDK11A","coVar"], p7["CDK11A","coVar"])

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_CDK11A.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(CDK11A.average, main="Average expression of CDK11A", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_CDK11A.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(CDK11A.cov, main="Variance in expression of MST1", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()


GPR64.average <- c(p4["GPR64","Mean"], p5["GPR64","Mean"], p6["GPR64","Mean"], p7["GPR64","Mean"])
GPR64.cov <- c(p4["GPR64","coVar"], p5["GPR64","coVar"], p6["GPR64","coVar"], p7["GPR64","coVar"])

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_GPR64.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(GPR64.average, main="Average expression of GPR64", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_GPR64.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(GPR64.cov, main="Variance in expression of GPR64", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()


KCNG3.average <- c(p4["KCNG3","Mean"], p5["KCNG3","Mean"], p6["KCNG3","Mean"], p7["KCNG3","Mean"])
KCNG3.cov <- c(p4["KCNG3","coVar"], p5["KCNG3","coVar"], p6["KCNG3","coVar"], p7["KCNG3","coVar"])

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_average_KCNG3.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(KCNG3.average, main="Average expression of KCNG3", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()

postscript(file=paste(output_dir,"network_analysis/cov_analysis/barplot_cov_KCNG3.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(KCNG3.cov, main="Variance in expression of KCNG3", names.arg=c("P4", "P5", "P6", "P7"), col=c("#0055D4", "#5599FF", "#AACCFF", "#D7E3F4"))
dev.off()


