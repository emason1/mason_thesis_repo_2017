
base_dir = '/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation'
lib_dir = '/home/lizzi/workspace/wells_lab/2012_library'

# Read in files

mapping_filename = open (lib_dir+'/illumina_probes_all_platforms_annotation_for_lizzi.tsv', 'r')
network_filename = open (base_dir+'/output/ontology_analysis/background_set_all_nodes.csv', 'r')


# Open files

mapping_file = mapping_filename.readlines()
network_file = network_filename.readlines()

#	Close files

mapping_file.close()
network_file.close()

#	Split each line by delimiter and iterate over the return

mapping = []
for line in mapping_file:
	line = line.strip('\n')
	line = line.split('\t')
	mapping.append(line)

network = []
for line in network_file:
	line = line.strip()
	line = line.split()
	network.extend(line)

#	Map the ProbeID to the OGS

mapped = []

for i in network:
	for j in mapping:
		if i == j[1] and j[0] not in mapped:
			mapped.append([j[0], j[1]])


#	Output file
f = open(base_dir+"/output/ontology_analysis/all_network_probes.csv", "w")
for i in mapped:
    f.write("%s,%s\n" % (i[0],i[1]))


