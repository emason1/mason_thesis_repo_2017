#! /usr/bin/Rscript

#	To change the data input to data which has supervised normalisation, change the "norm" file ONLY

#	Load the libraries
library(attract)
library(lumi)
library(limma)
library(illuminaHumanv4.db)

#	Assign the directories
base_dir <- '/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/'
input_dir <- paste(base_dir,'input/',sep="")
output_dir <- paste(base_dir,'output/attract/',sep="")

#	Load files
pheno <- paste(input_dir,'phenotype_info.txt',sep="")
norm <- paste(base_dir,'output/normalisation/data/quantile_normalised_data_undetected_genes_removed.txt',sep="")
plurinet <- paste(input_dir,'pathways/mapped_plurinet_probes.csv',sep="")
ECM <- paste(input_dir,'pathways/mapped_ECM_probes.csv',sep="")

exprs.dat.detect <- read.table(norm, header=T, sep=" ")
all.sample.info <- read.table(pheno, header=T, sep="\t")

cell.type <- factor(all.sample.info$Cell_Pheno)

#	DATA SLICING

CiPS.data <- exprs.dat.detect[,cell.type == "CiPSC"]
Cfib.data <- exprs.dat.detect[,cell.type == "Cfib"]
attract.data <- cbind(Cfib.data, CiPS.data)

attract.data.pheno <- vector()

for ( i in all.sample.info$Cell_Pheno ){
	if ( i == "Cfib" ){
		attract.data.pheno <- c(attract.data.pheno, i)
		}else{
			if ( i == "CiPSC" ){
			attract.data.pheno <- c(attract.data.pheno, i)
			}
		}
}

attract.cell.type <- as.factor(attract.data.pheno)

#	RUN ATTRACT
attract.eset <- new("ExpressionSet")
attract.eset@assayData <- new.env() 
assign("exprs", attract.data, attract.eset@assayData)
pheno.dat <- data.frame(colnames(attract.data), attract.cell.type)
colnames(pheno.dat) <- c("Sample_ID", "Cell_Pheno")
p.eset <- new("AnnotatedDataFrame", data=pheno.dat)
attract.eset@phenoData <- p.eset 

#	The cell type tag is a column from your eset
cellTypeTag <- colnames(pData(attract.eset))[2]	

#	Calculate the ranked pathways
#	Ensure your illumina version is correct
attractor.states <- findAttractors(attract.eset, cellTypeTag, min.pwaysize = 5, annotation = "illuminaHumanv4.db")
pathways <- attractor.states@rankedPathways
write.table(pathways, file=paste(output_dir,'pathway_ranks/all_ranked_pathways.txt',sep=""), row.names=TRUE, col.names=TRUE, sep="\t", quote=FALSE)
#	Create a table containing the significant ranked pathways and output the file (you can assign any threshold)
threshold <- 0.05
significant.pathways <- pathways[pathways$AdjustedPvalues <= threshold,]
write.table(significant.pathways, file=paste(output_dir,'pathway_ranks/significant_ranked_pathways.txt',sep=""), row.names=TRUE, col.names=TRUE, sep="\t", quote=FALSE)

#	Isolate the genes that have similar profiles of expression between phenotypes (ie- the flat genes)
remove.these.genes <- removeFlatGenes(attract.eset, cellTypeTag, contrasts = NULL, limma.cutoff = 0.05)

#	CALCULATE THE SYNEXPRESSION GROUPS FOR EACH SIGNIFICANT KEGG PATHWAY
#	For finding the synexpression groups for each pathway
#	Trim is a function that removes text delimiters "" and whitespace
#	KEGG.ids is the KEGG id for all the significant pathways of interest, in character format
trim <- function (x)  sub("\\s+", "", x)
KEGG.ids <- as.character(significant.pathways$KEGGID)
all.syn <- list()
gene.list <- list()


#	For reordering the samples in each synexpression group, and calculating the correlated partners
#	If you wish to reorder your profiles so samples are easily identified in the synexpression plots
reorder.profiles <- as.character(colnames(attract.data))
cutoff <- 0.9
counter <- 1

#	Assign the objects required for plotting each synexpression group
#	Tickmarks and phenodividers will need to be manually assigned depending on the number of samples in each condition
pheno.labels <- c(levels(attract.cell.type))
tick.marks <- c(5, 20)
pretty.col <- rainbow(10)
pheno.divider <- (12.5)
KEGG.names <- significant.pathways$KEGGNAME

#	Calculate the synexpression groups for each pathway that is significant
#	Output the gene lists to file
#	Calculate correlated partners of expression and output the gene lists to file
#	Plot all synexpression groups for significant pathways

plotmysynexprs <- function (mySynExpressionSet, tickMarks, tickLabels, vertLines, index = 1, ...) {
    if (is.numeric(index)) {
        plot(mySynExpressionSet@profiles[index, ], ylab = "Log2(Expression)", 
            axes = FALSE, xlab = "Groups", type = "p", pch = 19, col=cell.type,
            ...)
        axis(1, at = tickMarks, lab = tickLabels)
        axis(2)
        box()
        abline(v = vertLines, lwd = 3, col = "gray")
    }
}

for ( i in KEGG.ids ) {
	
	temp <- findSynexprs(trim(i), attractor.states, remove.these.genes)
		if ( (length(attributes(temp))) == 0 ){
			temp <- findSynexprs(trim(i), attractor.states)
		}	else {
			temp <- temp
		}

	synexpression.groups <- temp@groups
    gene.list <- c(gene.list, synexpression.groups)
	this.syn <- unique(unlist(synexpression.groups))
    all.syn[[length(all.syn)+1]] <- c(this.syn)
    
	xx <- file(paste(output_dir,"synexpression_lists/",trim(KEGG.names[counter]),"_synexpression_group_genes.txt","w",sep=""))
	sink(xx)
	print (synexpression.groups)
	sink()
	close(xx)

	
	correlated.partners.synexpression <- findCorrPartners(temp, attract.eset, remove.these.genes, cor.cutoff = cutoff)

	correlated.partners <- correlated.partners.synexpression@groups
	zz <- file(paste(output_dir,"correlated_partners_lists/",trim(KEGG.names[counter]),"_correlated_partners_genes.txt","w",sep=""))
	sink(zz)
	print (correlated.partners)
	sink()
	close(zz)
	
    postscript(file = paste(output_dir,"synexpression_plots/",trim(KEGG.names[counter]),".EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
	par(mfrow = c(3, 3))

		for ( j in 1:length(temp@groups)){
			
			if ( length(temp@groups) > 1 ) {
					reordered.syn <- temp@profiles[,reorder.profiles]
					temp@profiles <- reordered.syn
				} else {
					syn.profiles <- temp@profiles
					syn.reordered <- syn.profiles[,reorder.profiles]
					reordered.syn <- as.matrix(syn.reordered)
					profiles <- t(reordered.syn)
					temp@profiles <- profiles
				}
			
			plotmysynexprs(temp, frame.plot = TRUE, tickMarks=tick.marks, tickLabels=pheno.labels, index = j, ylim =range(c(temp@profiles)), vertLines = pheno.divider, main = paste((KEGG.names[counter]),j,sep= " "))

		}
		
		dev.off()
		counter <- counter + 1
}

#   Build a membership matrix  (mm)
mm <- matrix(numeric(0), nrow = length(all.syn), ncol = length(all.syn))
colnames(mm) = c(KEGG.ids[1:length(all.syn)])
rownames(mm) = c(KEGG.ids[1:length(all.syn)])

#   Fill the matrix from the information in the gene lists
for ( i in 1:nrow(mm) ) {
    for ( j in 1:ncol(mm) ) {
        mm[i,j] = as.numeric(length(intersect(all.syn[i][[1]], all.syn[j][[1]])))
    }
}

write.table(mm, file=paste(output_dir,'pathway_ranks/membership_matrix.txt',sep=""), row.names=TRUE, col.names=TRUE, quote = FALSE, sep="\t")

#	FIND SYNEXPRESSION GROUPS FOR PLURINET
#	Assign the vec.gene.id for Plurinet
all.plurinet <- read.csv(plurinet, header=FALSE, sep='\t')
vec.gene.id <- as.character(all.plurinet[,1])

source(file=paste(input_dir,"attract_functions/update_attract_findSynexprs_user_defined_pathway.r",sep=""))

pluri.temp <- findSynexprs.OnePway(attract.eset, vec.gene.id, cellTypeTag, removeGenes=NULL, min.clustersize=5)

plurinet.synexpression.groups <- pluri.temp@groups

aa <- file(paste(output_dir,"synexpression_lists/plurinet_synexpression_group_genes.txt","w",sep=""))
sink(aa)
print (plurinet.synexpression.groups)
sink()
close(aa)

plurinet.correlated.partners.synexpression <- findCorrPartners(pluri.temp, attract.eset, remove.these.genes, cor.cutoff = cutoff)

plurinet.correlated.partners <- plurinet.correlated.partners.synexpression@groups
	bb <- file(paste(output_dir,"correlated_partners_lists/plurinet_correlated_partners_genes.txt","w",sep=""))
	sink(bb)
	print (plurinet.correlated.partners)
	sink()
	close(bb)

bitmap(file = paste(output_dir,"synexpression_plots/Plurinet.png",sep=""), type="png16m", height=1000, width = 1000, units = "px")
par(mfrow = c(3, 3))

for ( j in 1:length(pluri.temp@groups)){
	plotmysynexprs(pluri.temp, frame.plot = TRUE, tickMarks=tick.marks, tickLabels=pheno.labels, index = j, ylim =range(c(pluri.temp@profiles)), vertLines = pheno.divider, main = paste("Plurinet",trim(j),sep= " "))
		}
dev.off()

#	Calculate the synexpression groups and correlated partners for ECMR
ecm <- findSynexprs("04512", attractor.states, remove.these.genes)
ecm.corr <- findCorrPartners(ecm, attract.eset, remove.these.genes, cor.cutoff = cutoff)

#	Read in the ECMR probe list
ecm_probe_ids <- read.csv(ECM, header=FALSE)

#	Isolate the gene lists of interest all must be in character string format
pluri.syn.genes <- unlist(pluri.temp@groups)
pluri.corr.genes <- unlist(plurinet.correlated.partners)
ecmr.syn.genes <- unlist(ecm@groups)
ecm.corr.genes <- unlist(ecm.corr@groups)
all.genes <- list(pluri.syn.genes, pluri.corr.genes, ecmr.syn.genes, ecm.corr.genes)
all.genes.of.interest <- unique(unlist(all.genes))

#	Perform a probe-wise Pearson R correlation
corr.data <- attract.data[rownames(attract.data) %in% all.genes.of.interest,]
correlation.matrix <- t(corr.data)
correlation <- cor(correlation.matrix, method = 'pearson')

all.pluri.ecm.genes <- list(pluri.syn.genes, ecmr.syn.genes)
pluri.ecm.genes <- unique(unlist(all.pluri.ecm.genes))

pluri.ecm.data <- attract.data[rownames(attract.data) %in% pluri.ecm.genes,]
pluri.ecm.correlation.matrix <- t(pluri.ecm.data)
pluri.ecm.correlation <- cor(pluri.ecm.correlation.matrix, method = 'pearson')


write.table(pluri.ecm.correlation, file=paste(output_dir,'correlation_matrices/plurinet_ecm_syn_probe_level_pearson_correlation_control_pluripotent.txt',sep=""), row.names=TRUE, col.names=TRUE, sep="\t", quote=FALSE)
write.table(pluri.ecm.data, file=paste(base_dir,'output/normalisation/data/pluri_ecm_syn_expression_data.txt',sep=""), row.names=TRUE, col.names=TRUE, sep="\t", quote=FALSE)

write.table(correlation, file=paste(output_dir,'correlation_matrices/probe_level_pearson_correlation_control_pluripotent.txt',sep=""), row.names=TRUE, col.names=TRUE, sep="\t", quote=FALSE)
write.table(CiPS.data, file=paste(base_dir,'output/normalisation/data/control_iPS_expression_data.txt',sep=""), row.names=TRUE, col.names=TRUE, sep="\t", quote=FALSE)
write.table(Cfib.data, file=paste(base_dir,'output/normalisation/data/control_fib_expression_data.txt',sep=""), row.names=TRUE, col.names=TRUE, sep="\t", quote=FALSE)
write.table(corr.data, file=paste(base_dir,'output/normalisation/data/iPS_fib_expression_data.txt',sep=""), row.names=TRUE, col.names=TRUE, sep="\t", quote=FALSE)

cc <- file(paste(output_dir,"all_genes_of_interest.txt","w",sep=""))
	sink(cc)
	print (all.genes.of.interest)
	sink()
	close(cc)

#   Address the ascertainment bias by testing variances of the synexpression groups vs. all probes vs. correlated partners of expression

synexpression.groups <- list(pluri.syn.genes, ecmr.syn.genes)
synexpression.genes <- unique(unlist(synexpression.groups))
synexpression <- attract.data[rownames(attract.data) %in% synexpression.genes,]

corr.partners.groups <- list(pluri.corr.genes, ecm.corr.genes)
corr.genes <- unique(unlist(corr.partners.groups))
corr.partners <- attract.data[rownames(attract.data) %in% corr.genes,]

all.probes <- attract.data

co.var <- function(x)(100*sd(x)/mean(x))

synexpression$coVar <- apply(synexpression, 1, co.var)
corr.partners$coVar <- apply(corr.partners, 1, co.var)
all.probes$coVar <- apply(all.probes, 1, co.var)

density.synexpression <- density(synexpression$coVar)
density.corr <- density(corr.partners$coVar)
density.all <- density(all.probes$coVar)

postscript(file=paste(output_dir,"cov_density_plots.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
		plot(density.synexpression, main="Coefficient of variation density plots", ylim=c(range(0, 0.09)), xlab="CoV", lty=1, lwd=4, col="red")
		lines(density.corr, lty=1, lwd=4, col="deepskyblue")
		lines(density.all, lty=1, lwd=4, col="purple")
		legend(x=40, y=0.08, c("Synexpression groups", "Correlated partners", "All probes"), lty=c(1, 1, 1), lwd=(4), col=c("red", "deepskyblue", "purple"), text.col=c("red", "deepskyblue", "purple"))
	dev.off()


#   Levene test
library(Rcmdr)
y <- c(synexpression$coVar, corr.partners$coVar)
group <- as.factor(c(rep(1, length(synexpression$coVar)), rep(2, length(corr.partners$coVar))))
levene.test(y, group)


