
#! /usr/bin/Rscript

library(stats)

base_dir <- '/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/'
input_dir <- paste(base_dir,'output/mapped_expression_data/',sep="")
output_dir <- paste(base_dir,'output/',sep="")

dat <- paste(input_dir,'pluri_ecm_syn_mapped_expression_dataset.csv',sep="")
cols <- paste(input_dir,'pluri_ecm_syn_header.csv',sep="")


#   Load files

expression_data <- read.table(dat, header = F, sep = ",")
cnames <- read.table(cols, header = F, sep = ",")
cnames <- as.character(cnames$V1)
colnames(expression_data) <- cnames
rnames <- as.character(expression_data$gene)
rownames(expression_data) <- rnames

if ( identical(rnames, as.character(rownames(expression_data))) ) {
    expression_data <- expression_data[,-1]
}


############################################################################
#   NETWORK CONSTRUCTION

correlation.matrix <- t(expression_data)
correlation <- cor(correlation.matrix, method = 'pearson')

matrixFromList <- function(listX) t(sapply(listX, function(x, n) c(x, rep(NA, n))[1:n], n = max(sapply(listX, length)))) 
cnames <- c("GENE_A", "GENE_B", "PEARSON", "EDGE_TYPE")

#   These cutoffs discover a network with 2600 edges
cutoff <- 0.9
temp <- list()
cols <- colnames(correlation)
rows <- rownames(correlation)

for ( i in 1:nrow(correlation) ) {
    for ( j in 1:ncol(correlation) ) {
        if ( i != j ) {
            if ( correlation[i, j] >= cutoff ) {
                temp[[length(temp) + 1]]= c(rows[i], cols[j], correlation[i,j], "pos")
            }
        }
	}
}

#   Format the network according to Cytoscape
network_1_with_duplicates <- matrixFromList(temp)
network_1_nodes <- t(apply(network_1_with_duplicates[,1:2], 1, sort))
network_1 <- unique(cbind(network_1_nodes, network_1_with_duplicates[,3:4]))
colnames(network_1) <- cnames

network_nodes <- sort(unique(as.character(network_1[,1:2])))

write.table(network_1, file=paste(output_dir,'cytoscape_input_files/pluri_ecm_syn_correlation_network_ens63.txt', sep=""),  quote = FALSE, row.names=FALSE, col.names=TRUE, sep="\t")
write.table(network_nodes, file=paste(output_dir,'cytoscape_input_files/pluri_ecm_syn_correlation_network_node_list_ens63.txt', sep=""),  quote = FALSE, row.names=FALSE, col.names=TRUE, sep="\t")

