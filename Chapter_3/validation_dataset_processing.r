#! /usr/bin/Rscript


library(lumi)
library(limma)
library(illuminaHumanv2.db)

base_dir <- '/home/lizzi/workspace/wells_lab/analysis/2012_11_network_finalisation/'


raw.data <- paste(base_dir,'input/validation_data/bead_studio_file.txt',sep="")
pheno <- paste(base_dir,'input/validation_data/phenotype_info.txt',sep="")
genes <- paste(base_dir,'input/validation_data/all_genes_of_interest.txt',sep="")
jess <- paste(base_dir,'input/validation_data/Robjects_lizzi.RData',sep="")

load(jess)

#gene.list <- read.table(genes, header=F, sep=",")
phenotype.info <- read.table(pheno, header=T, sep="\t")

#genes <- as.character(gene.list$V1)
exprs.dat.detect <- hes2.eset@assayData$exprs
#exprs.dat.detect <- exprs.dat.detect[rownames(exprs.dat.detect) %in% genes,]

#	Reorder the data, relabel with sample names
cell.type <- factor(phenotype.info$Cell_Pheno)
reorder.by.phenotype <- as.character(phenotype.info$Sample_ID)
exprs.dat.detect <- exprs.dat.detect[,reorder.by.phenotype]
sample.names <- as.character(phenotype.info$Sample_Name)	
colnames(exprs.dat.detect) <- sample.names

write.table(exprs.dat.detect, file=paste(base_dir,'output/normalisation/data/validation_data.txt', sep=""),  quote = FALSE, sep="\t")

#   Differential expression for mapping
#	DIFFERENTIAL GENE EXPRESSION ANALYSIS
#  Get R to read the annotation file and call it 'targets'
design <- model.matrix(~0 + cell.type)
colnames(design) <- levels(cell.type)
#	Fit the normalised expression data with a linear model
fit <- lmFit((exprs.dat.detect), design)
contrast.matrix <- makeContrasts(p4-p7, levels = design)
fit2 <- contrasts.fit(fit, contrast.matrix)
fit3 <- eBayes(fit2)
tt <- topTable(fit3, adjust="BH", n=nrow(exprs.dat.detect))

write.table(tt, file=paste(base_dir,'output/normalisation/data/differential_expression_mapping_validation.txt', sep=""),  quote = FALSE, sep="\t")
