# SCRIPTING TO ACCOMPANY #
# PHD THESIS OF ELIZABETH ANNE MASON #
# UNIVERSITY OF QUEENSLAND #


* All relevant scripting included for 3 data chapters

### Contribution guidelines ###

* Refer to thesis

### Contact Details ###

* Repo owner Elizabeth Mason
* elizabeth.mason@unimelb.edu.au