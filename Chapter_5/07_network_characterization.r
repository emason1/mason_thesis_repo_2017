#! /usr/bin/Rscript

#   First author     : Lizzi Mason
#   Acknowledgements : Othmar Korn
#   Depends on R-3.3.2

##---------------------------------------------------------------------------
#   This script is for the characterization of correlation networks constructed
#   from single cell RNA-seq data
##---------------------------------------------------------------------------

#   Last edited Wednesday 26 April 2017

#   Script 7 of 7

## ===========================================================================
##                  LOAD VARIABLES AND WORKSPACE
## ===========================================================================
#   Variables
source(file="/home/lizzi/scratch/Single_cell_analysis/LANNER/scripts/scripts/07_network_characterization_instantiate_variables.r")

#   Workspace
load(file=paste("/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/exports/Correlation_network_analysis_output",sep=""))


## ===========================================================================
##                  VARIABILITY ANALYSIS
##                   MU-LAMBDA NETWORKS
## ===========================================================================
##  QUARTILE ANALYSIS

##  STANDARD DEVIATION
#   Subset the standard deviation data and apply the quartile assignment in new column
#   Increasing genes
SD_increasing_matrix <- as.data.frame(sd_matrix[rownames(sd_matrix) %in% mu_lambda_increasing, c("SD_C2_E3", "SD_C2_E4-5", "SD_C2_E6", "SD_C2_E7")])
colnames(SD_increasing_matrix) <- c("SD_C2_E3", "SD_C2_E45", "SD_C2_E6", "SD_C2_E7")
SD_increasing_matrix <- within(SD_increasing_matrix, E3_quartile <- cut(SD_C2_E3, quantile(SD_C2_E3, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
SD_increasing_matrix <- within(SD_increasing_matrix, E45_quartile <- cut(SD_C2_E45, quantile(SD_C2_E45, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
SD_increasing_matrix <- within(SD_increasing_matrix, E6_quartile <- cut(SD_C2_E6, quantile(SD_C2_E6, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
SD_increasing_matrix <- within(SD_increasing_matrix, E7_quartile <- cut(SD_C2_E7, quantile(SD_C2_E7, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
write.table(SD_increasing_matrix, file=paste(output_dir,"SD_Increasing_Network_SD_Quartiles.txt",sep=""), quote=FALSE, sep=import_export_delimiter)

#   Which genes change and stay the same
SD_quartiles <- SD_increasing_matrix[,c("E3_quartile", "E45_quartile", "E6_quartile", "E7_quartile")]
SD_increasing_classifications <- classify_genes(SD_quartiles)
write.table(SD_increasing_classifications, file=paste(output_dir,"SD_increasing_quartiles_classifications.txt",sep=""), quote=FALSE, sep=import_export_delimiter)

#   Decreasing genes
SD_decreasing_matrix <- as.data.frame(sd_matrix[rownames(sd_matrix) %in% mu_lambda_decreasing, c("SD_C2_E3", "SD_C2_E4-5", "SD_C2_E6", "SD_C2_E7")])
colnames(SD_decreasing_matrix) <- c("SD_C2_E3", "SD_C2_E45", "SD_C2_E6", "SD_C2_E7")
SD_decreasing_matrix <- within(SD_decreasing_matrix, E3_quartile <- cut(SD_C2_E3, quantile(SD_C2_E3, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
SD_decreasing_matrix <- within(SD_decreasing_matrix, E45_quartile <- cut(SD_C2_E45, quantile(SD_C2_E45, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
SD_decreasing_matrix <- within(SD_decreasing_matrix, E6_quartile <- cut(SD_C2_E6, quantile(SD_C2_E6, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
SD_decreasing_matrix <- within(SD_decreasing_matrix, E7_quartile <- cut(SD_C2_E7, quantile(SD_C2_E7, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
write.table(SD_decreasing_matrix, file=paste(output_dir,"SD_Decreasing_Network_SD_Quartiles.txt",sep=""), quote=FALSE, sep=import_export_delimiter)

#   Which genes change and stay the same
SD_quartiles <- SD_decreasing_matrix[,c("E3_quartile", "E45_quartile", "E6_quartile", "E7_quartile")]
SD_decreasing_classifications <- classify_genes(SD_quartiles)
write.table(SD_decreasing_classifications, file=paste(output_dir,"SD_decreasing_quartiles_classifications.txt",sep=""), quote=FALSE, sep=import_export_delimiter)

##  MEAN
#   Subset the mean gene expression data and apply the quartile assignment in new column
#   Increasing genes
MU_increasing_matrix <- as.data.frame(mu_matrix[rownames(mu_matrix) %in% mu_lambda_increasing, c("Mean_C2_E3", "Mean_C2_E4-5", "Mean_C2_E6", "Mean_C2_E7")])
colnames(MU_increasing_matrix) <- c("Mean_C2_E3", "Mean_C2_E45", "Mean_C2_E6", "Mean_C2_E7")
MU_increasing_matrix <- within(MU_increasing_matrix, E3_quartile <- cut(Mean_C2_E3, quantile(Mean_C2_E3, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
MU_increasing_matrix <- within(MU_increasing_matrix, E45_quartile <- cut(Mean_C2_E45, quantile(Mean_C2_E45, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
MU_increasing_matrix <- within(MU_increasing_matrix, E6_quartile <- cut(Mean_C2_E6, quantile(Mean_C2_E6, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
MU_increasing_matrix <- within(MU_increasing_matrix, E7_quartile <- cut(Mean_C2_E7, quantile(Mean_C2_E7, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
write.table(MU_increasing_matrix, file=paste(output_dir,"MU_Increasing_Network_MU_Quartiles.txt",sep=""), quote=FALSE, sep=import_export_delimiter)

#   Which genes change and stay the same
MU_quartiles <- MU_increasing_matrix[,c("E3_quartile", "E45_quartile", "E6_quartile", "E7_quartile")]
MU_increasing_classifications <- classify_genes(MU_quartiles)
write.table(MU_increasing_classifications, file=paste(output_dir,"MU_increasing_quartiles_classifications.txt",sep=""), quote=FALSE, sep=import_export_delimiter)

#   Decreasing genes
MU_decreasing_matrix <- as.data.frame(mu_matrix[rownames(mu_matrix) %in% mu_lambda_decreasing, c("Mean_C2_E3", "Mean_C2_E4-5", "Mean_C2_E6", "Mean_C2_E7")])
colnames(MU_decreasing_matrix) <- c("Mean_C2_E3", "Mean_C2_E45", "Mean_C2_E6", "Mean_C2_E7")
MU_decreasing_matrix <- within(MU_decreasing_matrix, E3_quartile <- cut(Mean_C2_E3, quantile(Mean_C2_E3, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
MU_decreasing_matrix <- within(MU_decreasing_matrix, E45_quartile <- cut(Mean_C2_E45, quantile(Mean_C2_E45, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
MU_decreasing_matrix <- within(MU_decreasing_matrix, E6_quartile <- cut(Mean_C2_E6, quantile(Mean_C2_E6, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
MU_decreasing_matrix <- within(MU_decreasing_matrix, E7_quartile <- cut(Mean_C2_E7, quantile(Mean_C2_E7, probs=0:4/4), include.lowest=TRUE, labels=FALSE))
write.table(MU_decreasing_matrix, file=paste(output_dir,"MU_Decreasing_Network_MU_Quartiles.txt",sep=""), quote=FALSE, sep=import_export_delimiter)

#   Which genes change and stay the same
MU_quartiles <- MU_decreasing_matrix[,c("E3_quartile", "E45_quartile", "E6_quartile", "E7_quartile")]
MU_decreasing_classifications <- classify_genes(MU_quartiles)
write.table(MU_decreasing_classifications, file=paste(output_dir,"MU_decreasing_quartiles_classifications.txt",sep=""), quote=FALSE, sep=import_export_delimiter)

## ===========================================================================
##                  DEGREE ANALYSIS
## ===========================================================================
##  MEAN
#   Increasing gene set
mu_increasing_all_nodes <- c(MU_increasing_correlation_network$GENE_A, MU_increasing_correlation_network$GENE_B)
mu_increasing_node_degree <- table(mu_increasing_all_nodes)

#   Decreasing gene set
mu_decreasing_all_nodes <- c(MU_decreasing_correlation_network$GENE_A, MU_decreasing_correlation_network$GENE_B)
mu_decreasing_node_degree <- table(mu_decreasing_all_nodes)

##  STANDARD DEVIATION
#   Increasing gene set
sd_increasing_all_nodes <- c(SD_increasing_correlation_network$GENE_A, SD_increasing_correlation_network$GENE_B)
sd_increasing_node_degree <- table(sd_increasing_all_nodes)

#   Decreasing gene set
sd_decreasing_all_nodes <- c(SD_decreasing_correlation_network$GENE_A, SD_decreasing_correlation_network$GENE_B)
sd_decreasing_node_degree <- table(sd_decreasing_all_nodes)

#   Plot the degree distributions as histograms
cairo_ps(file=paste(output_dir,"Degree_histograms_SD_Mean_mu_lambda_increasing_decreasing_gene_sets.EPS",sep=""), width=10, height=8)
par(mfrow=c(2,2), mar=c(5,5,5,5), oma=c(3,3,3,3))
    hist(sd_decreasing_node_degree, breaks=10, freq=FALSE, col="#f2f0f7", main="SD Network (Decreasing)", xlab="Network Degree",  ylab="Density", cex.main=1.5, cex.lab=1.5, cex.axis=1.4)
        lines(density(sd_decreasing_node_degree), col="red")
    hist(mu_decreasing_node_degree, breaks=10, freq=FALSE, col="#f2f0f7", main="MU Network (Decreasing)", xlab="Network Degree",  ylab="Density", cex.main=1.5, cex.lab=1.5, cex.axis=1.4)
        lines(density(mu_decreasing_node_degree), col="red")
    hist(sd_increasing_node_degree, breaks=10, freq=FALSE, col="#f2f0f7", main="SD Network (Increasing)", xlab="Network Degree",  ylab="Density", cex.main=1.5, cex.lab=1.5, cex.axis=1.4)
        lines(density(sd_increasing_node_degree), col="red")
    hist(mu_increasing_node_degree, breaks=10, freq=FALSE, col="#f2f0f7", main="MU Network (Increasing)", xlab="Network Degree",  ylab="Density", cex.main=1.5, cex.lab=1.5, cex.axis=1.4)
        lines(density(mu_increasing_node_degree), col="red")
dev.off()

