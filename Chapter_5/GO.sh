
#   Author:  Lizzi Mason

#   Acknowledgements:  Othmar Korn

#   Last edited:  April 2017

#   Using the task spooler for larger jobs   e.g.    ts -L <jobname> <command>

#-------------------------------------------------------------------------------------------------------------------------

##   Login to server
ssh lizzi@pipe.stemformatics.org

cd /home/lizzi/scratch/Single_cell_analysis/LANNER/scripts/scripts/

##   Run setup script
chmod +x 01_setup.sh

01_setup.sh

##   Run thresholding and differential expression (run and finalised by LM on 07-04-2017)
chmod +x task_spooler_02_threshold_inspection_and_differential_expression.sh

ts -L LANNER_02_threshold_inspection_DE ./task_spooler_02_threshold_inspection_and_differential_expression.sh

##   Run cluster analysis of CPM data (run and finalised by LM on 07-04-2017)
chmod +x task_spooler_03_cluster_stability_CPM.sh

ts -L LANNER_03_cluster_stability_CPM ./task_spooler_03_cluster_stability_CPM.sh

##  ECDF and correlation analysis
chmod +x task_spooler_04_ECDF.sh

ts -L LANNER_04_ECDF ./task_spooler_04_ECDF.sh

##   Implement the mixture model

chmod +x task_spooler_05_mixture_model_implementation.sh

ts -L LANNER_05_mixture_model ./task_spooler_05_mixture_model_implementation.sh

##   Run correlation network analysis

chmod +x task_spooler_06_correlation_network_analysis.sh

ts -L LANNER_06_correlation_network_analysis ./task_spooler_06_correlation_network_analysis.sh

##   Run network characterization

chmod +x task_spooler_07_network_characterization.sh

ts -L LANNER_07_network_characterization_analysis ./task_spooler_07_network_characterization.sh


##   Run setup script
chmod +x 09_HOMER_II.sh

ts -L MOTIF_Analysis ./09_HOMER_II.sh





