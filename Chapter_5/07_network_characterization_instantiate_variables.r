#! /usr/bin/Rscript

#   First author     : Lizzi Mason
#   This script is for the discovery of correlation based networks in single cell RNA-seq data
#   Depends on R-3.3.2 and mix-tools 1.0.4

#   Last edited Wednesday 26 April 2017

#   Script 7 of 7


## ===========================================================================
##                             DEFINE FILEPATHS
## ===========================================================================
input_dir <- '/home/lizzi/scratch/Data/Single_cell/LANNER/input/'
output_dir <- '/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/correlation_networks/'
import_export_delimiter <- "\t"


## ===========================================================================
##                             DEFINE FUNCTIONS
## ===========================================================================
#   This function is to classify genes according to whether they change quantiles or not
#   Expects a data frame with 4 columns named "E3_quartile", "E45_quartile", "E6_quartile", "E7_quartile"

classify_genes <- function(Quartile_matrix){


unchanged_genes <- rownames(Quartile_matrix)[apply(Quartile_matrix, 1, function(x) length(unique(x))==1)]
unchanged_dat <- rep("unchanged", times=length(unchanged_genes))
unchanged <- cbind(unchanged_genes, unchanged_dat)

high_low_genes <- rownames(Quartile_matrix)[which(Quartile_matrix$E3_quartile > Quartile_matrix$E7_quartile)]
high_low_dat <- rep("high_low", times=length(high_low_genes))
high_low <- cbind(high_low_genes, high_low_dat)

low_high_genes <- rownames(Quartile_matrix)[which(Quartile_matrix$E3_quartile < Quartile_matrix$E7_quartile)]
low_high_dat <- rep("low_high", times=length(low_high_genes))
low_high <- cbind(low_high_genes, low_high_dat)

mid_genes <- rownames(Quartile_matrix)[!(rownames(Quartile_matrix) %in% c(unchanged_genes, high_low_genes, low_high_genes))]
mid_genes_dat <- rep("mid", times=length(mid_genes))
mid <- cbind(mid_genes, mid_genes_dat)

transition_matrix <- rbind(unchanged, high_low, low_high, mid)
colnames(transition_matrix) <- c("Gene", "Classification")

return(transition_matrix)
}
