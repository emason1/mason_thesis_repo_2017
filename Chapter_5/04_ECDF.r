#! /usr/bin/Rscript

#   First author     : Lizzi Mason
#   Acknowledgements : Shila Ghazanfar
#   This script is for the implementation of mixture models to single cell RNA-seq data
#   Depends on R-3.3.2

##---------------------------------------------------------------------------
##  This script is for fitting ECDFs to each phenotype to detect shifts in distribution
##  Expects single cell RNA-seq data that has been filtered and normalised
##  Currently testing in a list of 1000 genes
##---------------------------------------------------------------------------

#   Last edited March 2017

#   Script 4 of 6

## ===========================================================================
##                  LOAD VARIABLES AND WORKSPACE
## ===========================================================================
#   Variables
source(file="/home/lizzi/scratch/Single_cell_analysis/LANNER/scripts/scripts/04_ECDF_instantiate_variables.r")

#   Functions
source(file="/home/lizzi/scratch/Single_cell_analysis/LANNER/scripts/scripts/gammaNormalFunctions.R")

#   Workspace
load(file=paste("/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/exports/Input_for_mixture_model_implementation",sep=""))

## ===========================================================================
##                             PREPARE UPDATED PHENOTYPE INFORMATION
##                                  E4-5 includes E4, E4-Late, E5-Early, E5
## ===========================================================================
print("Preparing phenotype information..")
#   Import phenotype information
pheno_info <- read.csv(pheno_filename, header=TRUE, sep=import_export_delimiter)
pheno_info <- pheno_info[order(pheno_info$SampleID),]                               #   Ensure it is in the same order as the data frame
identical(colnames(log_expression_data), as.character(pheno_info$SampleID))         #   Ensure outcome is TRUE - make this a regression test
cell.type <- factor(pheno_info$Cell_pheno)                                          #   Phenotype information as a factor

#   "assert that"  https://cran.r-project.org/web/packages/assertthat/assertthat.pdf

## ===========================================================================
##                  EMPIRICAL CDF CLASSIFICATION OF GENES
## ===========================================================================
print("Running empirical CDF classification of genes ...")

#   Until script is finalised work with cut down version of 999 randomly selected genes + OCT4
set.seed(100)
cdf_gene_ids <- rownames(log_expression_data)

#-----SPECIFY THE ALPHA THRESHOLD FOR D STATISTIC
#   Specify the number of samples in each phenotype
E3_N <- ncol(log_expression_data[,cell.type=="E3"])
E45_N <- ncol(log_expression_data[,cell.type=="E4-5"])
E6_N <- ncol(log_expression_data[,cell.type=="E6"])
E7_N <- ncol(log_expression_data[,cell.type=="E7"])

#   Table of threshold cutoffs
alpha <- 0.05 / (3*length(cdf_gene_ids))
critical_val_alpha <- sqrt( -(1/2)*log(alpha/2) )

#   Calculate the thresholds for each phase
phase_1_alpha <- critical_val_alpha * sqrt( (E3_N + E45_N) / (E3_N * E45_N) )
phase_2_alpha <- critical_val_alpha * sqrt( (E45_N + E6_N) / (E45_N * E6_N) )
phase_3_alpha <- critical_val_alpha * sqrt( (E6_N + E7_N) / (E6_N * E7_N) )
alpha_thresholds <- c(phase_1_alpha, phase_2_alpha, phase_3_alpha)

for ( gene in cdf_gene_ids ){

    cairo_ps(file=paste(output_dir,"Empirical_CDFs_",gene,".EPS",sep=""), width=15, height=10)
    par(mfrow=c(2,2), mar=c(5,5,5,5), oma=c(3,3,3,3))
    
    #-----HISTOGRAM OF ALL CELLS
    #   All cells expression
    all_cells <- log_expression_data[gene,]+1
    hist(all_cells, breaks=20, freq=FALSE, col="gray60", main="Histogram all cells", xlab="Shifted Log2(CPM)",  ylab="Density", cex.main=2, cex.lab=1.5, cex.axis=1.5)
    legend("topleft", legend=paste("N=", length(log_expression_data[gene,]), sep=""))

    #-----EMPIRICAL CDF BY PHENOTYPE    
    #   All cells CDF
    ecdf_data <- ecdf(all_cells)                                                               #   Calculate the CDF for all cells
    plot.stepfun(ecdf_data, vertical=TRUE, do.points=FALSE, col="black", main="Empirical CDF", xlab="Shifted Log2(CPM)", cex.main=2, cex.lab=1.5, cex.axis=1.5)
    legend_text <- c("All cells N=1529")                                                       #   Instantiate plot and legend text

    #   CDF of each phenotype
    for ( pheno in levels(cell.type) ){
    
        this_phenotype <- log_expression_data[gene,cell.type==pheno]+1
        pheno_ecdf_data <- ecdf(this_phenotype)                                                #   Calculate CDF for given phenotype
        plot.stepfun(pheno_ecdf_data, vertical=TRUE, do.points=FALSE, col=ED_colours_CDF_list[[pheno]], add=TRUE)
        legend_text <- c(legend_text, paste(pheno, " N=", length(this_phenotype), sep=""))
        }
        legend("topleft", legend=legend_text, text.col=ED_colours_CDF)

    #-----KS TEST
    #   Segregate by phases to compare stepwise changes
    phase_1 <- ks.test(log_expression_data[gene,cell.type=="E3"]+1, log_expression_data[gene,cell.type=="E4-5"]+1)$statistic
    phase_2 <- ks.test(log_expression_data[gene,cell.type=="E4-5"]+1, log_expression_data[gene,cell.type=="E6"]+1)$statistic
    phase_3 <- ks.test(log_expression_data[gene,cell.type=="E6"]+1, log_expression_data[gene,cell.type=="E7"]+1)$statistic
    
    all_stats <- c(phase_1, phase_2, phase_3)
        barplot(rev(all_stats), yaxt="n", col=phases_barplot_cols, main="KS Test", xlab="D Statistic", names=rev(phases), cex.main=2, cex.lab=1.5, cex.axis=1.5, horiz=TRUE, border=NA)
        axis(2, at=c(0.7, 1.9, 3), labels=rev(phases), las=2, cex.axis=1.5, cex.lab=1.5)
        points(x=rev(alpha_thresholds), y=c(0.7,1.9,3), col="red", pch=19, cex=4)
    dev.off()
    
    #-----SELECT DIVERGENT GENES 
    for ( i in 1:length(all_stats) ){
        if ( all_stats[i] > alpha_thresholds[i] ){
            divergent_genes <- c(divergent_genes, gene)
            }
        }
    
    #-----VECTORISE THE KS-STATS
    ks_matrix <- rbind(ks_matrix, all_stats)

}

#-----IDENTIFY DIVERGENT KS-STATS
ks_matrix <- ks_matrix[-1,]
colnames(ks_matrix) <- phases
rownames(ks_matrix) <- cdf_gene_ids

#   For the 999 randomly selected genes + OCT4 there are 847 divergent genes
divergent_genes_ks_matrix <- ks_matrix[rownames(ks_matrix) %in% divergent_genes,]

## ===========================================================================

############## EXPORT KEY VARIABLES ##################################
#   For the "Mixture Modeling" analysis
output_dir <- '/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/exports/'
file_path_to_save <- paste(output_dir,"ECDF_output",sep="")
objects_to_save <- c("divergent_genes", "log_expression_data", "cell.type")
save(list=objects_to_save, file=file_path_to_save)



