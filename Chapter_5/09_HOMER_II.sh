
#   First author:  Elizabeth Mason

#   Last edited:  Wednesday 10-May-2017

#-----------------------------------

#   HOMER analysis for genes with different SD and MEAN classifications in the MU and SIGMA networks

#   Gene sets are unique ENSEMBL gene IDS

#   Usage:  findMotifs.pl <inputfile.txt> <promoter set> <output directory> [options]

#   For all [options] specific to this analysis: http://homer.salk.edu/homer/microarray/index.html

#scp -r /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/HOMER/INPUT/ lizzi@pipe.stemformatics.org:~/scratch/Single_cell_analysis/LANNER/output/final/motif_analysis/input/

#scp -r /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/gene_sets/Mean_Increasing_Network_full_gene_set.csv lizzi@pipe.stemformatics.org:~/scratch/Single_cell_analysis/LANNER/output/final/motif_analysis/input/

#scp -r /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/gene_sets/SD_Increasing_Network_full_gene_set.csv lizzi@pipe.stemformatics.org:~/scratch/Single_cell_analysis/LANNER/output/final/motif_analysis/input/


#-----------------------------------

#ssh lizzi@pipe.stemformatics.org

cd /home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/motif_analysis/


#   Proximal Promoter Regions

#   Default settings -300 to +50

#   Mu increasing

patterns="
high_low
low_high
mid
unchanged
"

cmd1="findMotifs.pl input/MU_increasing_"
cmd2=".txt human output/BG_network_Promoter_MU_increasing_"
cmd3=" -bg input/Mean_Increasing_Network_full_gene_set.csv"

for pattern in $patterns
    do $cmd1$pattern$cmd2$pattern"/"$cmd3
done

#   Sd increasing

cmd1="findMotifs.pl input/SD_increasing_"
cmd2=".txt human output/BG_network_Promoter_SD_increasing_"
cmd3=" -bg input/SD_Increasing_Network_full_gene_set.csv"

for pattern in $patterns
    do $cmd1$pattern$cmd2$pattern"/"$cmd3
done

#   Find targets for given known motifs

#http://homer.ucsd.edu/homer/microarray/index.html

#Scroll down to Find Instances of Specific Motifs

cd /home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/motif_analysis/

findMotifs.pl input/MU_increasing_high_low.txt human output -find output/BG_network_Promoter_MU_increasing_high_low/homerMotifs.all.motifs > Targets_BG_network_Promoter_MU_increasing_high_low.txt

findMotifs.pl input/MU_increasing_low_high.txt human output -find output/BG_network_Promoter_MU_increasing_low_high/homerMotifs.all.motifs > Targets_BG_network_Promoter_MU_increasing_low_high.txt

findMotifs.pl input/MU_increasing_mid.txt human output -find output/BG_network_Promoter_MU_increasing_mid/homerMotifs.all.motifs > BG_network_Targets_Promoter_MU_increasing_mid.txt

findMotifs.pl input/MU_increasing_unchanged.txt human output -find output/BG_network_Promoter_MU_increasing_unchanged/homerMotifs.all.motifs > BG_network_Targets_Promoter_MU_increasing_unchanged.txt

findMotifs.pl input/SD_increasing_high_low.txt human output -find output/BG_network_Promoter_SD_increasing_high_low/homerMotifs.all.motifs > BG_network_Targets_Promoter_SD_increasing_high_low.txt

findMotifs.pl input/SD_increasing_low_high.txt human output -find output/BG_network_Promoter_SD_increasing_low_high/homerMotifs.all.motifs > BG_network_Targets_Promoter_SD_increasing_low_high.txt

findMotifs.pl input/SD_increasing_mid.txt human output -find output/BG_network_Promoter_SD_increasing_mid/homerMotifs.all.motifs > BG_network_Targets_Promoter_SD_increasing_mid.txt

findMotifs.pl input/SD_increasing_unchanged.txt human output -find output/BG_network_Promoter_SD_increasing_unchanged/homerMotifs.all.motifs > BG_network_Targets_Promoter_SD_increasing_unchanged.txt













