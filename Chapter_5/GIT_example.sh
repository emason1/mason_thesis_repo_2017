#! /bin/sh

########## Author:  Lizzi Mason  

########## Last edited:  November 2016

Author:  Lizzi Mason
Email:  emason.uq@gmail.com

#########################################################################

#   Example of how to add a new analysis to the GIT repository

#   Change directories into the new analysis folder

cd /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/scripts/

#   Create a git repository

git init

#   Check that it added the correct file

ls -a

#   Add the files you want to commit

git add CAGE_analysis.r
git add draft_pipeline.sh

#   Commit the changes and comment them

git commit -m "Add new files to repository"

#   Push changes to bitbucket

git push


#########################################################################

#   Example of how to commit new changes to an existing GIT repository

#   Change directories into the analysis folder

cd /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/scripts/

#   Stage all changes in <directory> for the next commit

git add -A

#   Commit the changes and comment them

git commit -m "Update Waterfall analysis"

#   Push changes to bitbucket

git push

