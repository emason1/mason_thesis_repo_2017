#! /usr/bin/Rscript

#   First author : Lizzi Mason
#   This script is for the K-means cluster analysis of single cell RNA-seq data
#   Depends on R-3.3.2

#   Last edited January 2017


## ===========================================================================
##                             LOAD LIBRARIES
## ===========================================================================
require(fpc)
##  fpc documentaion https://www.rdocumentation.org/packages/fpc/versions/2.1-10/topics/clusterboot and https://cran.r-project.org/web/packages/fpc/fpc.pdf
#   
## ===========================================================================
##                             DEFINE FILEPATHS
## ===========================================================================
input_dir <- '/home/lizzi/scratch/Data/Single_cell/LANNER/input/'
output_dir <- '/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/clustering/'
import_export_delimiter <- "\t"


## ===========================================================================
##                             DEFINE FUNCTIONS
## ===========================================================================



## ===========================================================================
##                             DEFINE VARIABLES
## ===========================================================================

ED_axis_label <- c("E3", "E4", "E4_Late", "E5_Early", "E5", "E6", "E7")

pca_plot_filename <- paste(output_dir,"PCA_with_K_means_overlaid.EPS",sep="")

k_range_to_test <- 1:20

optimal_k_image_filename <- paste(output_dir,"Optimal_clusters_test_1_to_20.EPS",sep="")

cnames_clustering_bootstrap_output <- c("Cluster_Size", "Jaccard_Mean", "Recovered", "Dissolved", "Recovered_Percentage", "Dissolved_Percentage")

cluster_stability_filename <- paste(output_dir,'clustering_stability_k_16.txt',sep="")

