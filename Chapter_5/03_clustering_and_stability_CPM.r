#! /usr/bin/Rscript

#   First author : Lizzi Mason
#   This script is for the K-means cluster analysis of single cell RNA-seq data
#   Depends on R-3.3.2

#   Last edited January 2017

#   Script 3 of 6

## ===========================================================================
##                  LOAD VARIABLES AND WORKSPACE
## ===========================================================================
#   Variables
source(file="/home/lizzi/scratch/Single_cell_analysis/LANNER/scripts/scripts/03_clustering_and_stability_CPM_instantiate_variables.r")

#   Workspace
load(file=paste("/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/exports/Input_for_clustering_and_stability",sep=""))

## ===========================================================================
##                  BASIC PCA
## ===========================================================================
print("Computing principal components of thresholded data..")

##  Begin with a K-means cluster analysis of gene expression data

#   Standard principal component analysis for all embryonic days combined
pc_ED_data <- prcomp(t(log_expression_data))

#   Select PC1 and PC2 for k-means cluster analysis
pc_ED_for_k_means <- pc_ED_data$x[,1:2]

## ===========================================================================
##                  K MEANS CLUSTER ANALYSIS OF PC1 AND PC2
## ===========================================================================
print("Computing optimal number of clusters for K-means cluster analysis..")

#   Use the kmeansruns function to find the optimal number of clusters between N=7 and N=20 clusters
#   Default settings are 100 random starts and 100 max iterations per run
#   Not sure why the range tested looks like it's 1-20 not 7-20

set.seed(100)
find_optimal_k_asw <- kmeansruns(pc_ED_for_k_means, krange=k_range_to_test, criterion="asw", iter.max=100, runs=100)
find_optimal_k_ch <- kmeansruns(pc_ED_for_k_means, krange=k_range_to_test, criterion="ch", iter.max=100, runs=100)

print(paste("Optimal K asw",find_optimal_k_asw$bestk,sep=" "))
print(paste("Optimal K ch",find_optimal_k_ch$bestk,sep=" "))

#   One line for each analysis (so 2 sets of x and y coordinates)
y_asw <- as.numeric(scale(find_optimal_k_asw$crit))
x_asw <- 1:length(y_asw)
y_ch <-  as.numeric(scale(find_optimal_k_ch$crit))
x_ch <- 1:length(y_ch)

postscript(file=optimal_k_image_filename, onefile=FALSE, horizontal=FALSE, width=10, height=10, paper="special", family="Times")
    par(mar=c(5,5,5,5), oma=c(3,3,3,3))
    plot(x_asw, y_asw, type="o", pch=16, lty=1, col="orange", xlab="K clusters", ylab="Scaled score", cex.lab=1.7)
    axis(1, at=k_range_to_test, lab=k_range_to_test)
    lines(x_ch, y_ch, type="o", pch=17, lty=1, col="blue")
    abline(v=find_optimal_k_asw$bestk, col="orange", lty=2)
    abline(v=find_optimal_k_ch$bestk, col="blue", lty=2)
    legend("topright", legend=c("asw", "ch"), text.col=c("orange", "blue"), cex=1.5)
dev.off()

## ===========================================================================
##                  BOOTSTRAP THE CLUSTERS TO ASSESS STABILITY
## ===========================================================================
print("Performing bootstrap of K-means cluster analysis..")

#   Run the clusterboot with "removing one sample", dissolving clusters at 0.5 and recovering clusters at 0.75
cboot <- clusterboot(pc_ED_for_k_means, B=nrow(pc_ED_for_k_means), bootmethod="subset", subtuning=nrow(pc_ED_for_k_means)-1, clustermethod=kmeansCBI, dissolution=0.5, k=find_optimal_k_ch$bestk, seed=100)
cluster_assignment <-cboot$partition
cluster_sizes <- table(cluster_assignment)

#   Put the information into a table and export
clustering_bootstrap_output <- cbind(cluster_sizes, cboot$subsetmean, cboot$subsetrecover, cboot$subsetbrd, (cboot$subsetrecover/1529)*100, (cboot$subsetbrd/1529)*100)
colnames(clustering_bootstrap_output) <- cnames_clustering_bootstrap_output
rownames(clustering_bootstrap_output) <- 1:16

write.table(clustering_bootstrap_output, file=cluster_stability_filename, row.names=TRUE, col.names=TRUE, sep=import_export_delimiter, quote=FALSE)

## ===========================================================================
##                  DENSITY PLOTS FOR EACH CLUSTER
## ===========================================================================
print("Exporting density plots..")

#   Use a cut down version of the dataset until scripting and analysis is finalised
temp <- log_expression_data[1:50,]

#   3 nice examples are ENSG00000120948, 233461 and 244531

for (i in rownames(temp)){
    
    postscript(file=paste(output_dir,i,"_density_plot.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height=18, paper="special", family="Times")
    par(mfrow=c(6,3), mar=c(5,5,5,5), oma=c(3,3,3,3))
    xrange <- c(-2, max(log_expression_data))
    
    plot(density(log_expression_data[i,]), main=c("All Cells"), xlab=c("log(2) CPM"), ylab=paste("Density",i,sep=" "), xlim=xrange)
    abline(v=1, col="orange", lty=2)
        
    for ( cluster in 1:find_optimal_k_ch$bestk ){
        plot(density(log_expression_data[i,which(cluster_assignment==cluster)]), main=paste("Cluster",cluster,sep=" "), xlab=c("log(2) CPM"), ylab=paste("Density",i,sep=" "), xlim=xrange)
        abline(v=1, col="orange", lty=2)
    }
    dev.off()
}


############## EXPORT KEY VARIABLES ##################################
#   For the "mixture_model_implementation" analysis
output_dir <- '/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/exports/'

file_path_to_save_mixture_modeling <- paste(output_dir,"Input_for_mixture_model_implementation",sep="")
objects_to_save <- c("log_expression_data", "cell.type", "ED_colours_all_samples", "ED_axis_label", "ED_colours", "pheno_info")

save(list=objects_to_save, file=file_path_to_save_mixture_modeling)




