#! /usr/bin/Rscript

#   First author     : Lizzi Mason
#   This script is for the discovery of correlation based networks in single cell RNA-seq data
#   Depends on R-3.3.2 and mix-tools 1.0.4

#   Last edited Wednesday 26 April 2017

#   Script 6 of 6


## ===========================================================================
##                             DEFINE FILEPATHS
## ===========================================================================
input_dir <- '/home/lizzi/scratch/Data/Single_cell/LANNER/input/'
output_dir <- '/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/correlation_networks/'
import_export_delimiter <- "\t"


## ===========================================================================
##                             DEFINE FUNCTIONS
## ===========================================================================

#   Build a matrix from a list
matrixFromList <- function(listX) t(sapply(listX, function(x, n) c(x, rep(NA, n))[1:n], n = max(sapply(listX, length)))) 

#   Build a correlation network with positive edges only
build_correlation_network <- function(correlation, correlation_cutoff, output_file_path, cell_phenotype){

temp <- matrix(ncol=3, nrow=1)
nc <- ncol(correlation)

## Checks correlation for half the matrix (along the diagonal - ignoring taking correlation of same gene)
for ( i in 1:nrow(correlation) ) {
  x <- i+1
  if (x >= nc) {
    ## row finished, move to next
    next
  }
  for ( j in x:nc ) {
    if ( as.numeric(correlation[i,j]) >= correlation_cutoff & row.names(correlation)[i] != row.names(correlation)[j] ) {
      temp <- rbind(temp, c(row.names(correlation)[i], row.names(correlation)[j], correlation[i,j]))
    }
  }
}

temp <- temp[-1,]                                                       #   Remove the NA row from instantiating the vector
temp <- as.data.frame(temp, stringsAsFactors=FALSE)                     #   Coerce to data frame
colnames(temp) <- c("GENE_A", "GENE_B", "PEARSON_R")                    #   Label columns

temp <- within(temp, RNAMES <- paste(GENE_A, GENE_B, sep='_'))
temp <- temp[!duplicated(temp), ]
rownames(temp) <- temp$RNAMES           #   Make this the rownames of the data frame
temp <- temp[,-4]               

network_nodes <- sort(unique(as.character(temp[,1:2])))                 #   Generate unique list of nodes

write.table(network_nodes, file=paste(output_file_path,cell_phenotype,"_network_nodes.txt",sep=""), quote=FALSE, sep=import_export_delimiter)
write.table(temp, file=paste(output_file_path,cell_phenotype,"_coexpression_network.txt",sep=""), quote=FALSE, row.names=FALSE, col.names=TRUE, sep=import_export_delimiter)

return(temp)

}




## ===========================================================================
##                             DEFINE VARIABLES
## ===========================================================================

##  THRESHOLD BASED ON MU/LAMBDA IN E3 AND E7
phenos_to_threshold <- c("E3", "E7")
E3_E7_mean_threshold <- list()
mean_threshold <- 3
sd_threshold <- 1

##  PLOT COLOURS
ED_colours <- c("#d73027","#abd9e9", "#4575b4", "gray30")
ED_colours_list <- list()
ED_colours_list[["E3"]] <- "#d73027"
ED_colours_list[["E4-5"]] <- "#abd9e9"
ED_colours_list[["E6"]] <- "#4575b4"
ED_colours_list[["E7"]] <- "gray30"

ED_columns <- c("E3", "E4-5", "E6", "E7")
