#! /usr/bin/Rscript

#   First author : Lizzi Mason
#   This script is for the analysis of pooled single cell RNA-seq data
#   For visualising

#   Last edited March 2017

#   Script 2 of 6

## ===========================================================================
##                             DEFINE FILEPATHS
## ===========================================================================

workspace_filename <- "/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/exports/unpooled_data_differential_expression_input"

input_dir <- '/home/lizzi/scratch/Data/Single_cell/LANNER/input/'
output_dir <- '/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/differential_expression/'
import_export_delimiter <- "\t"

#1.     Import and process data
cpm_filename <- "/home/lizzi/scratch/Data/Single_cell/LANNER/Stemformatics/gene_count_frags_CPM.txt"
pheno_filename <- "/home/lizzi/scratch/Data/Single_cell/LANNER/Stemformatics/phenotype_information.csv"


ED_axis_label <- c("E3", "E4", "E4_Late", "E5_Early", "E5", "E6", "E7")

#   7 colours segregate E3, E4, E4_Late from E5_Early, E5, E6, E7
ED_colours <- c("#d73027","#fc8d59","#fee090","#e0f3f8","#91bfdb","#4575b4", "gray30")

log_2_CPM_label <- c("Log(2) CPM")

Equivalence_dentisy_plots <- paste(output_dir,"Density_plots.PNG",sep="")

PCA_before_DE_filename <- paste(output_dir,"PCA_screeplot_components_1_to_3.EPS",sep="")

PCA_before_DE_filename_SD <- paste(output_dir,"FGF_SD_SEED_PCA_screeplot_components_1_to_3.EPS",sep="")
PCA_before_DE_filename_MU <- paste(output_dir,"FGF_MU_SEED_PCA_screeplot_components_1_to_3.EPS",sep="")


list_of_contrasts <- c("E3 - E4", "E4 - E4_Late", "E4_Late - E5_Early", "E5_Early - E6", "E6 - E7")


venn_diagram_limma_filename <- paste(output_dir,'differential_expression_all_contrasts.PNG', sep="")

hierarchcal_cluster_all_samples_filename <- paste(output_dir,'Hierarcical_cluster_all_samples.EPS', sep="")


##  Set detection threshold CPM
#   Detection cutoff = 10 (log is on plotting only), percent cutoff = 10
phenos_for_thresholding <- c("E3", "E6", "E7")
detect_cutoff <- 10
