#! /usr/bin/Rscript

#   First author     : Lizzi Mason
#   Acknowledgements : Shila Ghazanfar

#   Last edited March 2017

#   Script 5 of 6

## ===========================================================================
##                             LOAD LIBRARIES
## ===========================================================================

require(stats)

## ===========================================================================
##                             DEFINE FILEPATHS
## ===========================================================================
input_dir <- '/home/lizzi/scratch/Data/Single_cell/LANNER/input/'
output_dir <- '/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/ECDFS/'
import_export_delimiter <- "\t"

## ===========================================================================
##                             DEFINE FUNCTIONS
## ===========================================================================
#   CDF of mixture of two normal distributions
#   Pinched from here: http://stats.stackexchange.com/questions/28873/goodness-of-fit-test-for-a-mixture-in-r
pmixnorm <- function(x, mu, sigma, pmix) {
  pmix[1]*pnorm(x,mu[1],sigma[1]) + (1-pmix[1])*pnorm(x,mu[2],sigma[2])
}



## ===========================================================================
##                             DEFINE VARIABLES
## ===========================================================================

pheno_filename <- "/home/lizzi/scratch/Data/Single_cell/LANNER/Stemformatics/phenotype_information_E45_combined.csv"

##  THRESHOLD BASED ON MU/LAMBDA IN E3 AND E7
phenos_to_threshold <- c("E3", "E7")
E3_E7_mean_threshold <- list()
mean_threshold <- 3
sd_threshold <- 1

##  ECDF
divergent_genes <- vector()
phases <- c("Phase_1", "Phase_2", "Phase_3")
phases_barplot_cols <- c("#efedf5", "#bcbddc", "#756bb1")
ks_matrix <- matrix(ncol=length(phases), nrow=1)

##  PLOT COLOURS
ED_colours_CDF <- c("#d73027","#abd9e9", "#4575b4", "gray30")
ED_colours_CDF_list <- list()
ED_colours_CDF_list[["E3"]] <- "#d73027"
ED_colours_CDF_list[["E4-5"]] <- "#abd9e9"
ED_colours_CDF_list[["E6"]] <- "#4575b4"
ED_colours_CDF_list[["E7"]] <- "gray30"

ED_columns <- c("E3", "E4-5", "E6", "E7")

##  CORRELATION NETWORK
density_plots_filename <- "/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/correlation_network/Density_plots_cutoff_09.EPS"
correlation_network_filepath <- "/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/correlation_network"

