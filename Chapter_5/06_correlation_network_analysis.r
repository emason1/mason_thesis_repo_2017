#! /usr/bin/Rscript

#   First author     : Lizzi Mason
#   Acknowledgements : Othmar Korn
#   Depends on R-3.3.2

##---------------------------------------------------------------------------
#   This script is for the construction of mu and sd networks using single cell
#   RNA seq data
##---------------------------------------------------------------------------

#   Last edited Wednesday 26 April 2017

#   Script 6 of 7

## ===========================================================================
##                  LOAD VARIABLES AND WORKSPACE
## ===========================================================================
#   Variables
source(file="/home/lizzi/scratch/Single_cell_analysis/LANNER/scripts/scripts/06_correlation_network_analysis_instantiate_variables.r")

#   Workspace
load(file=paste("/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/exports/Mixture_modeling_output",sep=""))


## ===========================================================================
##           TRANSFORM VARIANCE TO STANDARD DEVIATION
## ===========================================================================
#   Currently it's variance.  Need to take the square root to make it standard deviation.
sd_matrix <- sqrt(sigma_matrix)     #   8 columns 8153 rows

##  Ranges of mu matrix and sigma matrix are a bit weird
#   mu matrix 0.99 - 48.57
#   sigma matrix  3.669923e-06 to 1.027712e+04
#   sd matrix 1.915704e-03 to 1.013761e+02


##  There are 2 outliers
#   ENSG00000132854 with Mean of 48 in E3
#   ENSG00000087842 with Mean of 18 in E3
#   Remove from analysis

outliers <- c("ENSG00000087842", "ENSG00000132854")

mu_matrix <- mu_matrix[!rownames(mu_matrix) %in% outliers, ]
sd_matrix <- sd_matrix[!rownames(sd_matrix) %in% outliers, ]
lambda_matrix <- lambda_matrix[!rownames(lambda_matrix) %in% outliers, ]


## ===========================================================================
##           CLASSIFY GENES BASED ON THE MIXTURE COMPONENTS
##       FOLLOWED BY MIXTURE COMPONENTS + MEAN EXPRESSION OF C2
## ===========================================================================
print("Thresholding genes based on changes to the gamma gamma mixing components...")

##   Component 1 = low expression |  Component 2 = mid-high expression
#       Decreasing expression = transitioning from ON/HIGH in E3 to LOW/OFF in E7
#       Lambda C1 < C2 E3 & Lambda C1 > C2 E7
#
#       Increasing expression = transitioning from LOW/OFF in E3 to ON/HIGH in E7
#       Lambda C1 > C2 E3 & Lambda C1 < C2 E7

lambda_matrix <- as.data.frame(lambda_matrix)

decreasing_genes <- rownames(subset(lambda_matrix, E3_C1 < E3_C2 & E7_C1 > E7_C2))      #   1155 genes
increasing_genes <- rownames(subset(lambda_matrix, E3_C1 > E3_C2 & E7_C1 < E7_C2))      #   1760 genes

lambda_decreasing <- decreasing_genes      #    1155 genes
lambda_increasing <- increasing_genes      #    1760 genes

##  Display C1/C2 in each phenotype as boxplot
cairo_ps(file=paste(output_dir,"Boxplots_lambda_increasing_decreasing_gene_sets.EPS",sep=""), width=12, height=10)
par(mfrow=c(2,2), mar=c(5,5,5,5))
    boxplot(lambda_matrix[decreasing_genes,"E3_C1"], lambda_matrix[decreasing_genes,"E4-5_C1"], lambda_matrix[decreasing_genes,"E6_C1"], lambda_matrix[decreasing_genes,"E7_C1"], col=ED_colours, ylab=c("Proportion Component 1"), names=c("E3_C1", "E4-5_C1", "E6_C1", "E7_C1"), main=c("Decreasing C1"), ylim=c(0,1))
    boxplot(lambda_matrix[decreasing_genes,"E3_C2"], lambda_matrix[decreasing_genes,"E4-5_C2"], lambda_matrix[decreasing_genes,"E6_C2"], lambda_matrix[decreasing_genes,"E7_C2"], col=ED_colours, ylab=c("Proportion Component 2"), names=c("E3_C2", "E4-5_C2", "E6_C2", "E7_C2"), main=c("Decreasing C2"), ylim=c(0,1))
    boxplot(lambda_matrix[increasing_genes,"E3_C1"], lambda_matrix[increasing_genes,"E4-5_C1"], lambda_matrix[increasing_genes,"E6_C1"], lambda_matrix[increasing_genes,"E7_C1"], col=ED_colours, ylab=c("Proportion Component 1"), names=c("E3_C1", "E4-5_C1", "E6_C1", "E7_C1"), main=c("Increasing C1"), ylim=c(0,1))
    boxplot(lambda_matrix[increasing_genes,"E3_C2"], lambda_matrix[increasing_genes,"E4-5_C2"], lambda_matrix[increasing_genes,"E6_C2"], lambda_matrix[increasing_genes,"E7_C2"], col=ED_colours, ylab=c("Proportion Component 2"), names=c("E3_C2", "E4-5_C2", "E6_C2", "E7_C2"), main=c("Increasing C2"), ylim=c(0,1))
dev.off()

write.table(lambda_decreasing, file=paste(output_dir,"Lambda_decreasing_gene_set.txt",sep=""), quote=FALSE, sep=import_export_delimiter)
write.table(lambda_increasing, file=paste(output_dir,"Lambda_increasing_gene_set.txt",sep=""), quote=FALSE, sep=import_export_delimiter)

cairo_ps(file=paste(output_dir,"Boxplots_SD_Mean_lambda_increasing_decreasing_gene_sets.EPS",sep=""), width=10, height=8)
par(mfrow=c(2,2), mar=c(5,5,5,5), oma=c(3,3,3,3))
    boxplot(mu_matrix[rownames(mu_matrix) %in% lambda_decreasing, c("Mean_C2_E3", "Mean_C2_E4-5", "Mean_C2_E6", "Mean_C2_E7")], main=c("Decreasing"), col=ED_colours, ylab=c("Mean Log2(CPM)"), xlab=c("Embryonic Day"), names=ED_columns)
    boxplot(sd_matrix[rownames(sd_matrix) %in% lambda_decreasing, c("SD_C2_E3", "SD_C2_E4-5", "SD_C2_E6", "SD_C2_E7")], main=c("Decreasing"), col=ED_colours, ylab=c("SD Log2(CPM)"), xlab=c("Embryonic Day"), names=ED_columns)
    boxplot(mu_matrix[rownames(mu_matrix) %in% lambda_increasing, c("Mean_C2_E3", "Mean_C2_E4-5", "Mean_C2_E6", "Mean_C2_E7")], main=c("Increasing"), col=ED_colours, ylab=c("Mean Log2(CPM)"), xlab=c("Embryonic Day"), names=ED_columns)
    boxplot(sd_matrix[rownames(sd_matrix) %in% lambda_increasing, c("SD_C2_E3", "SD_C2_E4-5", "SD_C2_E6", "SD_C2_E7")], main=c("Increasing"), col=ED_colours, ylab=c("SD Log2(CPM)"), xlab=c("Embryonic Day"), names=ED_columns)
dev.off()

##  Further thresholding using lambda and mean expression

mu_lambda_decreasing <- rownames(subset(as.data.frame(mu_matrix[rownames(mu_matrix) %in% decreasing_genes,]), Mean_C2_E3 > Mean_C2_E7))    # 237
mu_lambda_increasing <- rownames(subset(as.data.frame(mu_matrix[rownames(mu_matrix) %in% increasing_genes,]), Mean_C2_E3 < Mean_C2_E7))    # 1078

write.table(mu_lambda_decreasing, file=paste(output_dir,"Decreasing_gene_set.txt",sep=""), quote=FALSE, sep=import_export_delimiter)
write.table(mu_lambda_increasing, file=paste(output_dir,"Increasing_gene_set.txt",sep=""), quote=FALSE, sep=import_export_delimiter)

cairo_ps(file=paste(output_dir,"Boxplots_SD_Mean_mu_lambda_increasing_decreasing_gene_sets.EPS",sep=""), width=10, height=8)
par(mfrow=c(2,2), mar=c(5,5,5,5), oma=c(3,3,3,3))
    boxplot(mu_matrix[rownames(mu_matrix) %in% mu_lambda_decreasing, c("Mean_C2_E3", "Mean_C2_E4-5", "Mean_C2_E6", "Mean_C2_E7")], main=c("Decreasing"), col=ED_colours, ylab=c("Mean Log2(CPM)"), xlab=c("Embryonic Day"), names=ED_columns)
    boxplot(sd_matrix[rownames(sd_matrix) %in% mu_lambda_decreasing, c("SD_C2_E3", "SD_C2_E4-5", "SD_C2_E6", "SD_C2_E7")], main=c("Decreasing"), col=ED_colours, ylab=c("SD Log2(CPM)"), xlab=c("Embryonic Day"), names=ED_columns)
    boxplot(mu_matrix[rownames(mu_matrix) %in% mu_lambda_increasing, c("Mean_C2_E3", "Mean_C2_E4-5", "Mean_C2_E6", "Mean_C2_E7")], main=c("Increasing"), col=ED_colours, ylab=c("Mean Log2(CPM)"), xlab=c("Embryonic Day"), names=ED_columns)
    boxplot(sd_matrix[rownames(sd_matrix) %in% mu_lambda_increasing, c("SD_C2_E3", "SD_C2_E4-5", "SD_C2_E6", "SD_C2_E7")], main=c("Increasing"), col=ED_colours, ylab=c("SD Log2(CPM)"), xlab=c("Embryonic Day"), names=ED_columns)
dev.off()

## ===========================================================================
##                  LAMBDA
##              CORRELATION ANALYSIS
## ===========================================================================
print("Performing correlation network analysis for genes selected based on Lambda only...")
#   Build 2 x correlation networks using C2 only in the increasing and decreasing categories

##  STANDARD DEVIATION
#   Subset the standard deviation data
SD_increasing_matrix <- sd_matrix[rownames(sd_matrix) %in% lambda_increasing, c("SD_C2_E3", "SD_C2_E4-5", "SD_C2_E6", "SD_C2_E7")]
SD_decreasing_matrix <- sd_matrix[rownames(sd_matrix) %in% lambda_decreasing, c("SD_C2_E3", "SD_C2_E4-5", "SD_C2_E6", "SD_C2_E7")]

#   Build correlation matrices
SD_increasing_correlation_matrix <- cor(t(SD_increasing_matrix), method="pearson")    #   range -0.9 - 1
SD_decreasing_correlation_matrix <- cor(t(SD_decreasing_matrix), method="pearson")    #   range -0.9 - 1

SD_increasing_correlation_network <- build_correlation_network(SD_increasing_correlation_matrix, 0.99, output_dir, "SD_Human_Embryogenesis_lambda_increasing_expression_099")
SD_decreasing_correlation_network <- build_correlation_network(SD_decreasing_correlation_matrix, 0.99, output_dir, "SD_Human_Embryogenesis_lambda_decreasing_expression_099")

##########
##  MEAN
#   Subset the mean gene expression data
MU_increasing_matrix <- mu_matrix[rownames(mu_matrix) %in% lambda_increasing, c("Mean_C2_E3", "Mean_C2_E4-5", "Mean_C2_E6", "Mean_C2_E7")]
MU_decreasing_matrix <- mu_matrix[rownames(mu_matrix) %in% lambda_decreasing, c("Mean_C2_E3", "Mean_C2_E4-5", "Mean_C2_E6", "Mean_C2_E7")]
#   Build correlation matrices
MU_increasing_correlation_matrix <- cor(t(MU_increasing_matrix), method="pearson")    #   range -1 - 1
MU_decreasing_correlation_matrix <- cor(t(MU_decreasing_matrix), method="pearson")    #   range -.99 - 1

MU_increasing_correlation_network <- build_correlation_network(MU_increasing_correlation_matrix, 0.99, output_dir, "MU_Human_Embryogenesis_lambda_increasing_expression_099")
MU_decreasing_correlation_network <- build_correlation_network(MU_decreasing_correlation_matrix, 0.99, output_dir, "MU_Human_Embryogenesis_lambda_decreasing_expression_099")


## ===========================================================================
##                  MU and LAMBDA
##              CORRELATION ANALYSIS
## ===========================================================================
print("Performing correlation network analysis for genes selected based on Lambda and mean expression...")
#   Build 2 x correlation networks using C2 only in the increasing and decreasing categories

##  STANDARD DEVIATION
#   Subset the standard deviation data
SD_increasing_matrix <- sd_matrix[rownames(sd_matrix) %in% mu_lambda_increasing, c("SD_C2_E3", "SD_C2_E4-5", "SD_C2_E6", "SD_C2_E7")]
SD_decreasing_matrix <- sd_matrix[rownames(sd_matrix) %in% mu_lambda_decreasing, c("SD_C2_E3", "SD_C2_E4-5", "SD_C2_E6", "SD_C2_E7")]

#   Build correlation matrices
SD_increasing_correlation_matrix <- cor(t(SD_increasing_matrix), method="pearson")    #   range -0.9 - 1
SD_decreasing_correlation_matrix <- cor(t(SD_decreasing_matrix), method="pearson")    #   range -0.9 - 1

SD_increasing_correlation_network <- build_correlation_network(SD_increasing_correlation_matrix, 0.99, output_dir, "SD_Human_Embryogenesis_mu_lambda_increasing_expression_099")
SD_decreasing_correlation_network <- build_correlation_network(SD_decreasing_correlation_matrix, 0.99, output_dir, "SD_Human_Embryogenesis_mu_lambda_decreasing_expression_099")

##########
##  MEAN
#   Subset the mean gene expression data
MU_increasing_matrix <- mu_matrix[rownames(mu_matrix) %in% mu_lambda_increasing, c("Mean_C2_E3", "Mean_C2_E4-5", "Mean_C2_E6", "Mean_C2_E7")]
MU_decreasing_matrix <- mu_matrix[rownames(mu_matrix) %in% mu_lambda_decreasing, c("Mean_C2_E3", "Mean_C2_E4-5", "Mean_C2_E6", "Mean_C2_E7")]
#   Build correlation matrices
MU_increasing_correlation_matrix <- cor(t(MU_increasing_matrix), method="pearson")    #   range -1 - 1
MU_decreasing_correlation_matrix <- cor(t(MU_decreasing_matrix), method="pearson")    #   range -.99 - 1

MU_increasing_correlation_network <- build_correlation_network(MU_increasing_correlation_matrix, 0.99, output_dir, "MU_Human_Embryogenesis_mu_lambda_increasing_expression_099")
MU_decreasing_correlation_network <- build_correlation_network(MU_decreasing_correlation_matrix, 0.99, output_dir, "MU_Human_Embryogenesis_mu_lambda_decreasing_expression_099")


############## EXPORT KEY VARIABLES ##################################
#   For the "Inspection_and_plotting" analysis
output_dir <- '/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/exports/'
file_path_to_save <- paste(output_dir,"Correlation_network_analysis_output",sep="")
objects_to_save <- c("mu_matrix","sd_matrix","SD_increasing_correlation_network","SD_decreasing_correlation_network","MU_increasing_correlation_network","MU_decreasing_correlation_network", "mu_lambda_increasing", "mu_lambda_decreasing")
save(list=objects_to_save, file=file_path_to_save)

## ===========================================================================
##              NETWORK MEAN AND STANDARD DEVIATION ANALYSIS
## ===========================================================================

#   load(file=paste("/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/exports/Correlation_network_analysis_output",sep=""))



