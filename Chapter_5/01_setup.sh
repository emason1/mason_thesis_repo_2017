
#   Author:  Lizzi Mason

#   Acknowledgements:  Othmar Korn, Isaac Virshup

#   Last edited:  March 2017

#-------------------------------------------------------------------------------------------------------------------------

#   This script is for installing R, installing CRAN/Bioconductor packages, and uploading data and scripting for single cell RNA-seq data

#   Script 1 of N

#   Further documentation (Readme): 

#-------------------------------------------------------------------------------------------------------------------------

#   Download all the relevant files, and store them in a temporary scratch file on the server

#    To login to the server
ssh lizzi@pipe.stemformatics.org

#   Create a directory /scripts where all the scripting will be saved
mkdir /home/lizzi/scratch/Single_cell_analysis/LANNER/scripts/ -p

#   Upload scripts from local machine to pipe server

#scp -r /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/scripts lizzi@pipe.stemformatics.org:~/scratch/Single_cell_analysis/LANNER/scripts/

#scp -r /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/gene_sets/FGF_MU_seed_network.csv lizzi@pipe.stemformatics.org:~/scratch/Single_cell_analysis/LANNER/input/

#scp -r /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/gene_sets/FGF_SD_seed_network.csv lizzi@pipe.stemformatics.org:~/scratch/Single_cell_analysis/LANNER/input/


cd /home/lizzi/scratch/Single_cell_analysis/LANNER/

#    Make the necessary files in self-contained script
mkdir ../input/
mkdir /home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/

cd /home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/
mkdir differential_expression/
mkdir clustering/
mkdir thresholding/
mkdir ECDFS/
mkdir correlation_networks/
mkdir mixture_modeling/
mkdir variability/
mkdir exports/
mkdir motif_analysis/

mkdir attract/
cd attract/
mkdir pathway_ranks/
mkdir synexpression_lists/
mkdir synexpression_plots/

#-------------------------------------------------------------------------------------------------------------------------

#   Install version 3.3.2 of R and all dependencies

#-------------------------------------------------------------------------------------------------------------------------

cd scratch/applications/

#   Scripting provided by Isaac Virshup

#   Detailed blog post about installing R-devel here http://pj.freefaculty.org/blog/?p=315

mkdir src
mkdir packages
mkdir tmp
INSTALL_DIR=`pwd`
TMPDIR=/home/lizzi/scratch/applications/tmp

export TMPDIR
export PATH=$INSTALL_DIR/packages/bin:$PATH
export LD_LIBRARY_PATH=$INSTALL_DIR/packages/lib:$LD_LIBRARY_PATH
export CFLAGS="-I$INSTALL_DIR/packages/include"
export LDFLAGS="-L$INSTALL_DIR/packages/lib"


## Get R3
cd $INSTALL_DIR/src
wget https://cran.r-project.org/src/base/R-3/R-3.3.2.tar.gz
tar -xzf R-3.3.2.tar.gz
# To check if requirements for installation have been met yet:
cd $INSTALL_DIR/src/R-3.3.2
./configure --enable-R-shlib --prefix=/home/lizzi/scratch/applications/R-3.3.2

## Make zlib
cd $INSTALL_DIR/src
wget http://zlib.net/zlib-1.2.8.tar.gz
tar -xzf zlib-1.2.8.tar.gz
cd zlib-1.2.8
./configure --prefix=$INSTALL_DIR/packages
make
make install

## Make bzip2
cd $INSTALL_DIR/src
wget http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz
tar xzvf bzip2-1.0.6.tar.gz
cd bzip2-1.0.6
make -f Makefile-libbz2_so
make clean
# Adding '-fPIC' CFLAG
make CFLAGS="-fPIC -Wall -Winline -O2 -g $(BIGFILES)"
make -n install PREFIX=$INSTALL_DIR/packages
make install PREFIX=$INSTALL_DIR/packages

## Make liblzma
cd $INSTALL_DIR/src
wget http://tukaani.org/xz/xz-5.2.2.tar.gz
tar xzvf xz-5.2.2.tar.gz
cd xz-5.2.2
./configure --prefix=$INSTALL_DIR/packages
make -j3
make install

## Make pcre
cd $INSTALL_DIR/src
wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.38.tar.gz
tar xzvf pcre-8.38.tar.gz
cd pcre-8.38
./configure --enable-utf8 --prefix=$INSTALL_DIR/packages
make -j3
make install

## Make libcurl
cd $INSTALL_DIR/src
wget --no-check-certificate https://curl.haxx.se/download/curl-7.47.1.tar.gz
tar xzvf curl-7.47.1.tar.gz
cd curl-7.47.1
./configure --prefix=$INSTALL_DIR/packages
make -j3
make install

## Install R
cd $INSTALL_DIR/src/R-3.3.2
./configure --enable-R-shlib --prefix=/home/lizzi/scratch/applications/src/R-3.3.2
make -j4

#-------------------------------------------------------------------------------------------------------------------------

#   Make aliases in ~/.bashrc file:
vim ~/.bashrc
G
i
alias R-3.3.2="$HOME/scratch/applications/src/R-3.3.2/bin/R --no-save"
alias R-3.1.1="$HOME/scratch/applications/R-3.1.1/bin/R --no-save"
#   Hit "ESC"

#   To quit vim and discard all changes, press the escape key and then type :q!
:wq
source ~/.bashrc

#-------------------------------------------------------------------------------------------------------------------------

#   Bioconductor package installation:  Install bioconductor using

#   source("https://bioconductor.org/biocLite.R") followed by biocLite() in the R shell

#-------------------------------------------------------------------------------------------------------------------------

#   CRAN package installation

#   Install all packages using R CMD INSTALL <package-name>.tar.gz in /applications





