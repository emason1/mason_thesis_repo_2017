Hello,

We provide five analyses using Waterfall. These codes can completely regenerate all the analyses we presented in the main text and the Supplementary Methods. Furthermore, they contain additional supporting analyses that we had to omit for the sake of brevity.

Waterfall accepts a table with TPM table (floating numbers) with cell id on the column names and gene id on the row names. We recommend to use RSEM (Dewey lab) to calculate TPM. In this tutorial, we provide TPM tables which were calculated using RSEM.

Each folder contains:
1. vignette for each analysis (file.R)
2. table for single cell id after excluding outliers (file.csv)
3. calculated expression table (file.txt)

For Bendall et al. analysis, we do not have 2. cell id file. For the two synthetic dataset, we generate the expression table at the very beginning of the vignette file, so we only have 1. vignette file.

The vignette for each analysis (file.R) can be a good start point. Especially, since the preprocessing step of Waterfall requires writing of your own codes (eg. removing outliers and selecting route of interest based on marker gene expression) to use Waterfall functions (Waterfall.R), we strongly recommend the users to go over the examples. The preprocessing step consists of 1. Determining the number of groups and grouping the cells, 2. Removing outliers, and 3. Orienting and defining trajectory using marker gene expression. Please refer to the Supplementary methods for more detailed descriptions. Defining linear trajectory require iterative steps. For example, if there are six groups within the PCA plot, one should inspect the PCA plot and check the marker expression to exclude minor branches and define one linear trajectory. We showed examples in the Supplementary methods section and attached R codes (file.R).

The Waterfall required packages include matrixStats, rgl, pheatmap, limma, MASS, ape, RColorBrewer, RHmm. Among them, support for RHmm has been discontinues, thus should be installed manually from (http://cran.r-project.org/src/contrib/Archive/RHmm/). Please see R Installation and Administration (http://cran.r-project.org/doc/manuals/r-release/R-admin.html) for more information. Package prada is required to read single cell masscytometry datasets. Optionally, for calculate kernel density of total distance, foreach and doMC are required for parallelization.

After installing all the packages, first run the Waterfall.R to load the Waterfall functions. And then set the directory for each analysis folder using setwd function. 

Here is the session information of my R:

########### R Session Information ##############
R version 3.1.1 (2014-07-10)
Platform: x86_64-apple-darwin13.1.0 (64-bit)

locale:
[1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
[1] RHmm_2.0.3         nlme_3.1-120       RColorBrewer_1.1-2 ape_3.2            MASS_7.3-39        limma_3.22.5       pheatmap_0.7.7     rgl_0.93.1098      matrixStats_0.14.0

loaded via a namespace (and not attached):
[1] grid_3.1.1      lattice_0.20-30
#################################################

The latest version of the code can be provided upon request.
If you have any question, please send an email to me (shin@jhmi.edu) or Dr. Hongjun Song (shongju1@jhmi.edu).

Jaehoon Shin, MD
Graduate student in Dr. Hongjun Song lab
Institute of Cell Engineering
Johns Hopkins Medical Institutions