#   First author:  Elizabeth Mason

#   Last edited:  Friday 05-May-2017

#-----------------------------------

#   HOMER analysis for genes with different SD and MEAN classifications in the MU and SIGMA networks

#   Gene sets are unique ENSEMBL gene IDS

#   Usage:  findMotifs.pl <inputfile.txt> <promoter set> <output directory> [options]

#   For all [options] specific to this analysis: http://homer.salk.edu/homer/microarray/index.html

cd /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/HOMER/

#   Make HOMER executable if not already (problems with this)

#-----------------------------------

#   Proximal Promoter Regions

#   Default settings -300 to +50

PATH=$PATH:/home/lizzi/homer/bin/

#   Mu increasing

patterns="
high_low
low_high
mid
unchanged
"

cmd1="findMotifs.pl INPUT/MU_increasing_"
cmd2=".txt human OUTPUT/Promoter_MU_increasing_"

for pattern in $patterns
    do $cmd1$pattern$cmd2$pattern"/"
done

#   Sd increasing

cmd1="findMotifs.pl INPUT/SD_increasing_"
cmd2=".txt human OUTPUT/Promoter_SD_increasing_"

for pattern in $patterns
    do $cmd1$pattern$cmd2$pattern"/"
done


#http://homer.ucsd.edu/homer/microarray/index.html

#Scroll down to Find Instances of Specific Motifs


cd /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/HOMER/OUTPUT/Promoter_MU_increasinghigh_low/knownResults/
cat *.motif > ./combined_motifs_known.motif

cd /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/HOMER/OUTPUT/Promoter_MU_increasinglow_high/knownResults/
cat *.motif > ./combined_motifs_known.motif

cd /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/HOMER/OUTPUT/Promoter_MU_increasingmid/knownResults/
cat *.motif > ./combined_motifs_known.motif

cd /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/HOMER/OUTPUT/Promoter_MU_increasingunchanged/knownResults/
cat *.motif > ./combined_motifs_known.motif

cd /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/HOMER/OUTPUT/Promoter_SD_increasing_high_low/knownResults/
cat *.motif > ./combined_motifs_known.motif

cd /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/HOMER/OUTPUT/Promoter_SD_increasing_low_high/knownResults/
cat *.motif > ./combined_motifs_known.motif

cd /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/HOMER/OUTPUT/Promoter_SD_increasing_mid/knownResults/
cat *.motif > ./combined_motifs_known.motif

cd /home/lizzi/workspace/wells_lab/analysis/2016_09_Waterfall/output/HOMER/OUTPUT/Promoter_SD_increasing_unchanged/knownResults/
cat *.motif > ./combined_motifs_known.motif




