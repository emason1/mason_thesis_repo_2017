#! /usr/bin/Rscript

#   First author     : Lizzi Mason
#   Acknowledgements : Shila Ghazanfar
#   This script is for the implementation of mixture models to single cell RNA-seq data
#   Depends on R-3.3.2 and mix-tools 1.0.4

#   Last edited March 2017

#   Script 6 of 6

## ===========================================================================
##                             LOAD LIBRARIES
## ===========================================================================
#   Depends on mix-tools
#   Written in mix-tools 1.0.4

require(mixtools)
require(stats)

## ===========================================================================
##                             DEFINE FILEPATHS
## ===========================================================================
input_dir <- '/home/lizzi/scratch/Data/Single_cell/LANNER/input/'
output_dir <- '/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/mixture_modeling/'
import_export_delimiter <- "\t"

## ===========================================================================
##                             DEFINE FUNCTIONS
## ===========================================================================
#   CDF of mixture of two normal distributions
#   Pinched from here: http://stats.stackexchange.com/questions/28873/goodness-of-fit-test-for-a-mixture-in-r
pmixnorm <- function(x, mu, sigma, pmix) {
  pmix[1]*pnorm(x,mu[1],sigma[1]) + (1-pmix[1])*pnorm(x,mu[2],sigma[2])
}

#   Mode of a vector
calculate_mode <- function(v) {
   uniqv <- unique(v)
   uniqv[which.max(tabulate(match(v, uniqv)))]
}


## ===========================================================================
##                             DEFINE VARIABLES
## ===========================================================================

##  THRESHOLD BASED ON MU/LAMBDA IN E3 AND E7
phenos_to_threshold <- c("E3", "E7")
E3_E7_mean_threshold <- list()
mean_threshold <- 3
sd_threshold <- 1

##  PLOT COLOURS
ED_colours <- c("#d73027","#abd9e9", "#4575b4", "gray30")
ED_colours_list <- list()
ED_colours_list[["E3"]] <- "#d73027"
ED_colours_list[["E4-5"]] <- "#abd9e9"
ED_colours_list[["E6"]] <- "#4575b4"
ED_colours_list[["E7"]] <- "gray30"

ED_columns <- c("E3", "E4-5", "E6", "E7")

##  DISTRIBUTION FITTING PER GENE USING MIXTOOLS
model_components_fitted_only <- vector()
phenos_to_fit_model <- c("E7")
#   At this stage cut points 2 and 6 are arbitary
cut_points <- c(2, 6)
goodness_of_fit <- list()
model_selection_output <- list()
mixture_model_output <- list()
all_phenotypes_mixture_model_output <- list()
no_model_fit <- vector()
no_data <- vector()
gamma_mixture_model_output <- list()
    
##  PROCESS THE SIGMA AND MU MATRICES
mu_matrix <- matrix(, nrow=1, ncol=8)
sigma_matrix <- matrix(, nrow=1, ncol=8)
lambda_matrix <- matrix(, nrow=1, ncol=8)
parameter_matrix_rownames <- vector()
mu_matrix_col_names <- c("Mean_C1_E3", "Mean_C2_E3", "Mean_C1_E4-5", "Mean_C2_E4-5", "Mean_C1_E6", "Mean_C2_E6", "Mean_C1_E7", "Mean_C2_E7")
sigma_matrix_col_names <- c("SD_C1_E3", "SD_C2_E3", "SD_C1_E4-5", "SD_C2_E4-5", "SD_C1_E6", "SD_C2_E6", "SD_C1_E7", "SD_C2_E7")
lambda_matrix_col_names <- c("E3_C1", "E3_C2", "E4-5_C1", "E4-5_C2", "E6_C1", "E6_C2", "E7_C1", "E7_C2")
