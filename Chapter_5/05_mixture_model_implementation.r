#! /usr/bin/Rscript

#   First author     : Lizzi Mason
#   Acknowledgements : Shila Ghazanfar
#   This script is for the implementation of mixture models to single cell RNA-seq data
#   Depends on R-3.3.2 and mix-tools 1.0.4

##---------------------------------------------------------------------------
##  This script is for implementation of gaussain mixture models using mix-tools
##  Expects single cell RNA-seq data that has been filtered and normalised
##  Currently testing in a list of 10 genes specified in the variable script
##---------------------------------------------------------------------------

#   Last edited Wednesday 19 April 2017

#   Script 5 of 6

## ===========================================================================
##                  LOAD VARIABLES AND WORKSPACE
## ===========================================================================
#   Variables
source(file="/home/lizzi/scratch/Single_cell_analysis/LANNER/scripts/scripts/05_mixture_model_instantiate_variables.r")

#   Functions
source(file="/home/lizzi/scratch/Single_cell_analysis/LANNER/scripts/scripts/gammaNormalFunctions.R")

#   Workspace
load(file=paste("/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/exports/ECDF_output",sep=""))

## ===========================================================================
##                      GAMMA GAMMA MIXTURE MODEL
## ===========================================================================
print("Fitting 2 component gamma-gamma mixture model..")

set.seed(100)

gamma_gene_ids <- unique(divergent_genes)

#   Fit 2 component mix
number_of_components <- 2

#   Gamma normal model is computational bottleneck but 20 genes takes about 15min to run
for ( gene in gamma_gene_ids ){
    
    #-------CHECK ZERO INFLATION
    #   Ensure the frequency of zeroes doesn't exceed 40% across all the samples
    #   It shouldn't exceed 40% given that mean thresholding is performed
    #   This is to ensure that we don't fit a mixture model to mainly zeroes
    
    this_gene <- table(round(log_expression_data[gene,], digits=0))
    if ( this_gene[1] > 600 ){
            #   If the frequency of zeroes does exceed 40% fit the mixture model on expressed values only
            #   This should be an infrequent occurrance (if at all)
            print(paste,"ERROR",gene,"more than 40% zero inflation",sep=" ")
            next
            }
            
    #   Gamma distribution requires non-zero
    data_subset <- log_expression_data[gene,]+1
    #   Recapitulating number of iterations as Shila and seems to have improved iterations without errors/warnings
    mixture_model_all_pheno <- 0
    mixture_model_all_pheno <- try( gammamixEM(data_subset, lambda = c(1, 1)/2, verb = FALSE) )
    if (class(mixture_model_all_pheno) != "mixEM"){
            next
            }
    
    #   Plot the output of the mixture model against generic QQplot and histogram
    cairo_ps(file=paste(output_dir,"Enforced_gamma_gamma_2_component_model_",gene,".EPS",sep=""), width=10, height=8)
    par(mfrow=c(2,2), mar=c(5,5,5,5), oma=c(3,3,3,3))
        #   Generic histogram
        hist(log_expression_data[gene,]+1, breaks=50, freq=FALSE, col="#f2f0f7", main=paste(gene,"\nexpression all cells",sep=" "), xlab="Shifted Log2(CPM)",  ylab="Density", cex.main=1.5, cex.lab=1.5, cex.axis=1.4)
        #   Generic density
        plot(density(log_expression_data[gene,]+1), lty=2, lwd=2, col="darkgrey", main=paste(gene,"\nexpression all cells",sep=" "), xlab="Shifted Log2(CPM)", ylab="Density", cex.main=1.5, cex.lab=1.5, cex.axis=1.4)
        #   QQ plot
        qqnorm(log_expression_data[gene,], cex.main=1.5, cex.lab=1.5, cex.axis=1.4)
        #   Mixture model
        hist(data_subset, breaks=15, freq=FALSE, col="#f2f0f7", main=paste(number_of_components," component\ngamma mixture model"), xlab="Shifted Log2(CPM)",  ylab="Density", cex.main=1.5, cex.lab=1.5, cex.axis=1.4)
        curve(mixture_model_all_pheno$lambda[1]*dgamma(x,shape=mixture_model_all_pheno$gamma.pars["alpha","comp.1"],rate=1/mixture_model_all_pheno$gamma.pars["beta","comp.1"]), add=TRUE, lwd=2, col="black")     #   gamma
        curve(mixture_model_all_pheno$lambda[2]*dgamma(x,shape=mixture_model_all_pheno$gamma.pars["alpha","comp.2"],rate=1/mixture_model_all_pheno$gamma.pars["beta","comp.2"]), add=TRUE, lwd=2, col="black")     #   gamma
        
        curve(mixture_model_all_pheno$lambda[1]*dgamma(x,shape=mixture_model_all_pheno$gamma.pars["alpha","comp.1"],rate=1/mixture_model_all_pheno$gamma.pars["beta","comp.1"])+
              mixture_model_all_pheno$lambda[2]*dgamma(x,shape=mixture_model_all_pheno$gamma.pars["alpha","comp.2"],rate=1/mixture_model_all_pheno$gamma.pars["beta","comp.2"]), add=TRUE, lwd=2, col="red")        #   mixture
    dev.off()

    file_name <- paste("Enforced_gamma_gamma_2_component_model_all_phenotypes",gene,sep="_")
    cairo_ps(file=paste(output_dir,file_name,".EPS",sep=""), width=25, height=40)
    par(mfrow=c(4,2), mar=c(5, 5, 5, 5), oma=c(5,5,5,5))
        for (pheno in levels(cell.type) ){
            #   Fit the mixture model for each gene in each phenotype
            data_subset <- log_expression_data[gene,cell.type==pheno]+1
            mixture_model_one_pheno <- 0
            mixture_model_one_pheno <- try( gammamixEM(data_subset, lambda = c(1, 1)/2, verb = FALSE) )
            if (class(mixture_model_one_pheno) != "mixEM"){
                next
                }
            
            #   Capture the output    
            gamma_mixture_model_output[[paste(gene,pheno,sep="_")]] <- mixture_model_one_pheno   
            
             #   Plot the underlying histogram
            hist(x=data_subset, breaks=15, freq=FALSE, col=ED_colours_list[[pheno]], main=paste(pheno,number_of_components,"component \ngamma gamma mixture model",sep=" "), xlab="Shifted Log2(CPM)",  ylab="Density", cex.main=3, cex.lab=2, cex.axis=2)
            #   Then add a line for the combined model
            #   Fit each component separately
            curve(mixture_model_one_pheno$lambda[1]*dgamma(x, shape=mixture_model_one_pheno$gamma.pars[1,1],rate=1/mixture_model_one_pheno$gamma.pars[2,1]), add=TRUE, lwd=2, col="black")      #   gamma
            curve(mixture_model_one_pheno$lambda[2]*dgamma(x, shape=mixture_model_one_pheno$gamma.pars[1,2],rate=1/mixture_model_one_pheno$gamma.pars[2,2]), add=TRUE, lwd=2, col="black")      #   normal
            #   Horizontal lines indicate the cutpoints used in the EM algorithm for the mixture model
            abline(v=2, col="orange", lty=2)
            abline(v=6, col="orange", lty=2)

            #   Log likelihood plot using inbuilt function
            plot(x=mixture_model_one_pheno, whichplots=1, loglik=TRUE, cex.axis=2, cex.lab=2, cex.main=3, main1=c("Log likelihood"), xlab1=c("Iterations"))
        }
    dev.off()

}

## ===========================================================================
##                  DISTRIBUTION FITTING PER GENE in E7 USING MIXTOOLS
## ===========================================================================
print("Fitting the most appropriate normal mixture model in E7 using model selection...")

set.seed(100)
norm_gene_ids <- c("ENSG00000204531", sample(gamma_gene_ids, size=19, replace=FALSE))

for ( gene in norm_gene_ids ){

#-------CHECK ZERO INFLATION
    #   Ensure the frequency of zeroes doesn't exceed 40% across all the samples
    #   It shouldn't exceed 40% given that mean thresholding is performed
    #   This is to ensure that we don't fit a mixture model to mainly zeroes
    #   Expect gene to change expression between phenotypes so zero in one phenotype will need to be handled
    
    this_gene <- table(round(log_expression_data[gene,], digits=0))
    if ( this_gene[1] > 600 ){
            #   If the frequency of zeroes does exceed 40% fit the mixture model on expressed values only
            #   This should be an infrequent occurrance (if at all)
            print(paste,"ERROR",gene,"more than 40% zero inflation",sep=" ")
            next
            }

    #   Select phenotype(s) of interest
    for (pheno in phenos_to_fit_model){
        data_subset <- log_expression_data[gene,cell.type==pheno]
    
    #-------MODEL SELECTION
    #   Model selection
        modal_data <- makemultdata(data_subset, cuts=cut_points)
        set.seed(100)    #   Ensure same starting point for every gene to ensure reproducible results
        model_selection <- multmixmodel.sel(modal_data, comps=1:4, epsilon=0.001)       #   ERROR "Restarting due to numerical problem"; number of iterations most often = 2
        model_selection_output[[paste(gene,pheno,sep="_")]] <- model_selection
        #   Use the most frequently selected number of modes
        number_of_components <- as.numeric(names(which.max(table(model_selection[,"Winner"]))))
        print(paste(gene,number_of_components,"model selected",sep=" "))
        #   Vectorise lambda for use in the expectation maximisation algorithm (mixing proportions, must sum to 1)
        lambda_vector <- c(rep(1, number_of_components))/number_of_components           #   The winner is always 2 in our example genes - can 1 be selected?  optimise model selection?

    #-------EM ALGORITHM TO FIT MIXTURE MODEL
    #   Fit a normalmix expectation maximisation
        set.seed(100)
        #   Ensure you can evaluate it (but right now if it cannot compute the whole loop breaks)
        mixture_model <- 0
        #   The suggested mean (mu) can be passed as a vector but not sure how to automate this
        mixture_model <- try( normalmixEM(data_subset, lambda=lambda_vector, verb=FALSE, maxit=1529) )
        if (class(mixture_model) != "mixEM"){
            next
            }
        mixture_model_output[[paste(gene,pheno,sep="_")]] <- mixture_model
        #   Good help doc:
        #   http://lib.dr.iastate.edu/cgi/viewcontent.cgi?article=1069&context=stat_las_pubs
        #   https://cran.r-project.org/web/views/Cluster.html

    #-------PLOTTING
    #   Plot the outputs of the mixture model, log likelihood, histogram and qq plot (because we are testing normalmixEM here)
        file_name <- paste("Model_selection_normal_mix_",gene,pheno,sep="_")
        cairo_ps(file=paste(output_dir,file_name,".EPS",sep=""), width=10, height=8)
        par(mfrow=c(2,2), mar=c(5,5,5,5), oma=c(3,3,3,3))
        
        #   Basic histogram
        hist(log_expression_data[gene, cell.type == pheno], breaks=50, freq=FALSE,col="lightgrey", main=paste(gene,"\nexpression",pheno,sep=" "), xlab="Log2(CPM)",  ylab="Density", cex.main=1.5, cex.lab=1.5, cex.axis=1.4)
                
        #   QQ plot
        qqnorm(log_expression_data[gene, cell.type == pheno], cex.main=1.5, cex.lab=1.5, cex.axis=1.4)
                
        #   Log likelihood
        plot(x=mixture_model, whichplots=1, loglik=TRUE, cex.axis=1.4, cex.lab=1.4, cex.main=1.8, main1=c("Log likelihood"), xlab1=c("Iterations"))

        #   Mixture model
        plot(x=mixture_model, whichplots=2, density=TRUE, cex.axis=1.4, cex.lab=1.4, cex.main=1.5, main2=paste(number_of_components," component\nnormal mixture model",sep=""), xlab2="Log2(CPM)")
        abline(v=2, col="orange", lty=2)
        abline(v=6, col="orange", lty=2)

        dev.off()
        
        }
}


## ===========================================================================
##                  FITTING MIXTURE MODEL PER GENE IN EACH PHENOTYPE
## ===========================================================================
print("Fitting normal mixture model selected in E7 per gene to all phenotypes...")

#   New errors:
#   NA/NaN/Inf in foreign function call (arg 6)

for ( gene in norm_gene_ids ){

    #-------EXTRACT NUMBER OF COMPONENTS FROM E7 OUTPUT
    number_of_components <- length(mixture_model_output[[paste(gene,"E7",sep="_")]][["lambda"]])
    lambda_vector <- c(rep(1, number_of_components))/number_of_components

    #   Instantiate the plot (one plot per gene)
    file_name <- paste("Normal_mixture_model_all_phenotypes",gene,sep="_")
    cairo_ps(file=paste(output_dir,file_name,".EPS",sep=""), width=25, height=40)
    par(mfrow=c(4,2), mar=c(5, 5, 5, 5), oma=c(5,5,5,5))
        
    #   Fit the model to each phenotype
    for (pheno in levels(cell.type) ){
        
        #-------EM ALGORITHM TO FIT MIXTURE MODEL
        #   Fit a normalmix expectation maximisation
        set.seed(100)
        #   Ensure you can evaluate it (but right now if it cannot compute the whole loop breaks)
        mixture_model <- 0
        mixture_model <- try( normalmixEM(log_expression_data[gene,cell.type==pheno], lambda=lambda_vector, verb=FALSE, maxit=1529) )
        if (class(mixture_model) != "mixEM"){
            no_model_fit <- c(no_model_fit, paste(gene,pheno,sep="_"))
            next
            }
        all_phenotypes_mixture_model_output[[paste(gene,pheno,sep="_")]] <- mixture_model
        
        #-------KS TEST FOR GOODNESS OF FIT
        #   For K=2 normal distributions only (at this stage)
        if ( number_of_components == 2 ){
            kstest <- ks.test(mixture_model$x, pmixnorm, mu=mixture_model$mu, sigma=mixture_model$sigma, pmix=mixture_model$lambda)
            goodness_of_fit[[paste(gene,pheno,sep="_")]] <- as.character(kstest)
            }
        
        #-------PLOTTING

        #   The mixture model
        hist(x=log_expression_data[gene,cell.type==pheno], breaks=15, freq=FALSE, col=ED_colours_list[[pheno]], main=paste(pheno,number_of_components,"component\nnormal normal mixture model",sep=" "), xlab="Log2(CPM)",  ylab="Density", cex.main=3, cex.lab=2, cex.axis=2)
        #   Then add a line for the combined model
        #   Fit each component separately
        for ( i in 1:number_of_components ){
        curve(mixture_model$lambda[i] * dnorm(x, mixture_model$posterior[,i], mean=mixture_model$mu[i], sd=mixture_model$sigma[i]), add=TRUE, lwd=2, col=c("black"))
        }
        #   Horizontal lines indicate the cutpoints used in the EM algorithm for the mixture model
        abline(v=2, col="orange", lty=2)
        abline(v=6, col="orange", lty=2)
        
        #   Ensure the KS test p-value is displayed
        legend("topleft", legend=paste("N=", length(log_expression_data[gene,cell.type==pheno]), "\n", "K.S. p-value=", round(kstest$p.value, digits=2), sep=""), cex=2)
        
        #   Log likelihood plot using inbuilt function
        plot(x=mixture_model, whichplots=1, loglik=TRUE, cex.axis=2, cex.lab=2, cex.main=3, main1=c("Log likelihood"), xlab1=c("Iterations"))

    }

    dev.off()

}


## ===========================================================================
##                  ASSESS CHANGES TO MIXTURE MODEL PARAMETERS 
##                          PER GENE IN EACH PHENOTYPE
## ===========================================================================
print("Assessing changes to gamma gamma mixture model parameters during phenotype transition...")


for ( gene in gamma_gene_ids ){

    #   Select all phenotypes and ensure loop can still run if no data present
    all_phenos <- 0
    all_phenos <- try( as.character(names(gamma_mixture_model_output))[grep(gene, names(gamma_mixture_model_output))] )         #   Order of phenotypes is out here.
    if (length(all_phenos) < 4){
            no_data <- c(no_data, gene)
            next
            }
    all_phenos <- sort(all_phenos)                                                                                              #   This is where phenotype needs to be reordered
    
    #   Save the gene name for the rownames of the parameter matrix
    parameter_matrix_rownames <- c(parameter_matrix_rownames, gene)

    #   Segregate the ED (these will be different depending on whether mixture model could be fitted)
    ED <- unlist(strsplit(all_phenos, split="_"))
    
    #   Use these to create tick labels
    x_tick_labels <- ED[c(FALSE, TRUE)]
    
    #   Use the labels to prepare the colour scheme
    plot_colours <- vector()
    for ( i in x_tick_labels ){
        plot_colours <- c(plot_colours, rep(ED_colours_list[[i]], times=1, each=2))
        }


    #   Vectorise the lambda mixing components
    lambda_mixing <- vector()

    #   Select the mu and sigma for y-coordinates and bars
    mu_points <- vector()
    sigma_points <- vector()
        
    for ( pheno in all_phenos ){
        
        #   Mean of component 1 and 2
        pheno_mu <- c(gamma_mixture_model_output[[pheno]]$gamma.pars["alpha","comp.1"] * gamma_mixture_model_output[[pheno]]$gamma.pars["beta","comp.1"], gamma_mixture_model_output[[pheno]]$gamma.pars["alpha","comp.2"] * gamma_mixture_model_output[[pheno]]$gamma.pars["beta","comp.2"])
        mu_points <- c(mu_points, pheno_mu)
        
        #   Sigma of component 1 and 2
        pheno_sigma <- c(gamma_mixture_model_output[[pheno]]$gamma.pars["alpha","comp.1"] * gamma_mixture_model_output[[pheno]]$gamma.pars["beta","comp.1"]^2, gamma_mixture_model_output[[pheno]]$gamma.pars["alpha","comp.2"] * gamma_mixture_model_output[[pheno]]$gamma.pars["beta","comp.2"]^2)
        sigma_points <- c(sigma_points, pheno_sigma)

        #   Mixing proportions
        lambda_mixing_pheno <- c(gamma_mixture_model_output[[pheno]]$lambda[1], gamma_mixture_model_output[[pheno]]$lambda[2])
        lambda_mixing <- c(lambda_mixing, lambda_mixing_pheno)
        }

    x_points <- 1:length(mu_points)
    
    #   Put mu sigma and lambda into matrices for use in clustering
    mu_matrix <- rbind(mu_matrix, mu_points)
    sigma_matrix <- rbind(sigma_matrix, sigma_points)
    lambda_matrix <- rbind(lambda_matrix, lambda_mixing)
    
    #   Plot the mean and standard deviation for each mixture-model component per embryonic day
    file_name <- paste("Gamma_gamma_model_mu_sigma_plot_",gene,sep="")
    cairo_ps(file=paste(output_dir,file_name,".EPS",sep=""), width=16, height=8)
    par(mar=c(5,5,5,5), oma=c(5,5,5,5))
        plot(x_points, mu_points, col=plot_colours, pch=rep(c(19,17), times=length(all_phenos)), axes=FALSE, ann=FALSE, ylim=c(0,max(mu_points)+3), xlim=c(0,max(x_points)+1), cex=3)
        axis(2, las=0)
        axis(1, at=c(x_points[c(FALSE, TRUE)]-0.5), labels=x_tick_labels, las=0)
        lines(x=rbind(x_points, x_points, NA), y=rbind(mu_points+sigma_points, mu_points-sigma_points, NA), col="darkgray", lwd=2)
        abline(h=2, col="orange", lty=2)
        abline(h=6, col="orange", lty=2)
        title(main=paste(gene," parameters",sep=""), ylab=c("Mean Log2(CPM)"), cex.main=3, cex.axis=2, cex.lab=2)
    dev.off()
    
    
    

}

## ===========================================================================
##                      PROCESS THE SIGMA MU AND LAMBDA MATRICES
## ===========================================================================

#   Remove the first row which is NA value from instantiating empty matrix
mu_matrix <- mu_matrix[-1,]
sigma_matrix <- sigma_matrix[-1,]
lambda_matrix <- lambda_matrix[-1,]

#   Apply rownames
rownames(mu_matrix) <- parameter_matrix_rownames
rownames(sigma_matrix) <- parameter_matrix_rownames
rownames(lambda_matrix) <- parameter_matrix_rownames

#   Apply column names
colnames(mu_matrix) <- mu_matrix_col_names
colnames(sigma_matrix) <- sigma_matrix_col_names
colnames(lambda_matrix) <- lambda_matrix_col_names


############## EXPORT KEY VARIABLES ##################################
#   For the "Inspection_and_plotting" analysis
output_dir <- '/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/exports/'
file_path_to_save <- paste(output_dir,"Mixture_modeling_output",sep="")
objects_to_save <- c("mu_matrix", "sigma_matrix", "gamma_mixture_model_output", "lambda_matrix")
save(list=objects_to_save, file=file_path_to_save)

