#! /usr/bin/Rscript

#   First author : Lizzi Mason
#   This script is for the analysis of pre-processed single cell RNA-seq data
#   Depends on R-3.3.2 and Bioconductor version 3.4

#   Last edited November 2016

#   Script 2 of 6

## ===========================================================================
##                             LOAD LIBRARIES
## ===========================================================================

require(limma)
require(sparcl)

## ===========================================================================
##                             LOAD VARIABLES
## ===========================================================================

#   Variables
source(file="/home/lizzi/scratch/Single_cell_analysis/LANNER/scripts/scripts/02_threshold_inspection_and_differential_expression_instantiate_variables.r")

## ===========================================================================
##                             READ IN DATA
## ===========================================================================

#-----DATA
#   fread function from data.table package in R speeds up reading in big files if read.table is slow
#   read.table is fine
cpm_data <- read.table(cpm_filename, header=TRUE, sep=import_export_delimiter)
#   ensure columns in order
cpm_data <- cpm_data[,order(colnames(cpm_data)),]
cpm_data_matrix <- as.matrix(cpm_data)
all_na_values <- apply(cpm_data_matrix, 1, function(x) all(is.na(x)))
cpm_data_matrix <- cpm_data_matrix[!all_na_values,]                     #   55841 rows x 1529 cols
cpm_data_matrix[is.na(cpm_data_matrix)] <- 0                            #   Change NA values to zeroes

## ===========================================================================
##                             PREPARE PHENOTYPE INFORMATION
## ===========================================================================
print("Preparing phenotype information..")
#   Import phenotype information
pheno_info <- read.csv(pheno_filename, header=TRUE, sep=import_export_delimiter)
pheno_info <- pheno_info[order(pheno_info$SampleID),]                   #   Ensure it is in the same order as the data frame
identical(colnames(cpm_data), as.character(pheno_info$SampleID))        #   Ensure outcome is TRUE - make this a regression test
donor_info <- factor(pheno_info$Donor)                                  #   Assign the factors for embryonic day and donor information
#   Phenotype information as a factor
cell.type <- factor(pheno_info$Cell_pheno)
design <- model.matrix(~0 + cell.type)
colnames(design) <- ED_axis_label

## ===========================================================================
##                             THRESHOLD DATA
## ===========================================================================
print("Threshold the CPM gene expression data..")

##  Threshold based on 10CPM per gene in 20% of samples in a given phenotype
genes_to_keep <- vector()

for ( day in phenos_for_thresholding ){
    ED <- rownames(cpm_data_matrix)[apply(cpm_data_matrix[,cell.type == day], 1, function(x){ sum(x >= detect_cutoff)  >= .20*length(x) })]
    genes_to_keep <- c(genes_to_keep, ED)
}

#   Threshold the data
genes_to_keep <- unique(genes_to_keep)                                  #   10993 genes
thresholded_unshifted <- cpm_data_matrix[genes_to_keep,]
#   Shift the data by 1 to ensure log2 transformation doesn't produce -Inf values
thresholded_shifted <- thresholded_unshifted +1                         #   Range is 1 to 94865.8
#   Log transform the data
log_expression_data <- log2(thresholded_shifted)                        #   Range is 0 to 19
print("Range of log2 unpooled expression data ")                        #   Dimensions are 10993 rows x 1529 columns
print(range(log_expression_data))


## ===========================================================================
##                             TEST FOR EQUIVALENCY
## ===========================================================================
print("Testing for equivalency between phenotypes..")

#   Illustrate the equivalency of genes that are represented between pools using
#   density plots for each ED and all pools

bitmap(file=Equivalence_dentisy_plots, type="png16m", height=1500, width=800, units="px")
    #   Set 4 rows x 2 columns of images
    par(mfrow=c(4,2), mar=c(5,5,5,5), oma=c(3,3,3,3))
    #   First plot is the distribution of all pools combined
    plot(density(log_expression_data), xlab=log_2_CPM_label, main="", col=c("red"), cex.lab=1.7, cex.axis=1.3)
    legend("topright", legend="All cells", cex=2)
    #   Plot each embryonic stage
    for ( stage in levels(cell.type) ){
        temp <- log_expression_data[,cell.type==stage]
        #   Plot the total density of all pools per stage
        plot(density(temp), xlab=log_2_CPM_label, col=c("red"), main="", cex.lab=1.7, cex.axis=1.3)
        #   Plot density of each pool individually
        for ( cell in 1:ncol(temp) ) {
            lines(density(temp[,cell]), col=c("grey50"), lty=3)
        }
    legend("topright", legend=stage, cex=2)
    
    }
    
dev.off()

## ===========================================================================
##                             CLUSTER THE FILTERED DATA
## ===========================================================================
print("Clustering filtered data using PCA..")
##  Consider using CIDR  http://biorxiv.org/content/biorxiv/early/2016/08/31/068775.full.pdf

#   Standard principal component analysis for all embryonic days combined
#   See if individual cells cluster by ED
pc_ED_data <- prcomp(t(log_expression_data))

sd_seed <- read.table(file="/home/lizzi/scratch/Single_cell_analysis/LANNER/input/FGF_SD_seed_network.csv",sep=import_export_delimiter)
mu_seed <- read.table(file="/home/lizzi/scratch/Single_cell_analysis/LANNER/input/FGF_MU_seed_network.csv",sep=import_export_delimiter)

sd_seed <- as.character(sd_seed[,1])
mu_seed <- as.character(mu_seed[,1])

sd_data <- log_expression_data[sd_seed,]

mu_data <- log_expression_data[rownames(log_expression_data) %in% mu_seed,]

pc_ED_data <- prcomp(t(sd_data))

#   Specify colours per embryonic day
ED_colours_all_samples <- vector()
for ( cell in pheno_info[,"Cell_pheno"] ){

    if ( cell == "E3" ){
        ED_colours_all_samples <- c(ED_colours_all_samples, ED_colours[1])
    }
    if ( cell == "E4" ){
        ED_colours_all_samples <- c(ED_colours_all_samples, ED_colours[2])
    }
    if ( cell == "E4-Late" ){
        ED_colours_all_samples <- c(ED_colours_all_samples, ED_colours[3])
    }
    if ( cell == "E5-Early" ){
        ED_colours_all_samples <- c(ED_colours_all_samples, ED_colours[4])
    }
    if ( cell == "E5" ){
        ED_colours_all_samples <- c(ED_colours_all_samples, ED_colours[5])
    }
    if ( cell == "E6" ){
        ED_colours_all_samples <- c(ED_colours_all_samples, ED_colours[6])
    }
    if ( cell == "E7" ){
        ED_colours_all_samples <- c(ED_colours_all_samples, ED_colours[7])
    }
}

postscript(file=PCA_before_DE_filename_SD, onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
par(mfrow=c(2,2))
    plot(pc_ED_data, type = "l")
    plot(pc_ED_data$x, col=ED_colours_all_samples, main=c("PC1 v PC2"))
    plot(pc_ED_data$x[,2:3], col=ED_colours_all_samples, main=c("PC2 v PC3"))
    plot(pc_ED_data$x[,c(1,3)], col=ED_colours_all_samples, main=c("PC1 v PC3"))
    legend("topleft", legend=ED_axis_label, col=ED_colours, text.col=ED_colours, cex=0.7)
dev.off()

#   Standard principal component analysis for each embryonic day separately
#   Overlay the donor information as colour
for ( day in levels(cell.type) ){
    #   Compute the PCA for that particular ED
    pc_ED <- prcomp(t(log_expression_data[,cell.type == day]))
    cells_per_ED <- colnames(log_expression_data[,cell.type == day])
    #   Specify donor information for colour
    donor_info <- subset(pheno_info, SampleID %in% cells_per_ED, select=Donor)
    donor_info <- factor(donor_info[,1])
    #   Plot a screeplot and PC1 and PC2
    postscript(file=paste(output_dir,"PCA_day_",day,".EPS",sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")    
        par(mfrow=c(2,2))
            plot(pc_ED, type = "l")
            plot(pc_ED$x, main=c("PC1 v PC2"), col=donor_info)
            plot(pc_ED$x[,2:3], main=c("PC2 v PC3"), col=donor_info)
            plot(pc_ED$x[,c(1,3)], main=c("PC1 v PC3"), col=donor_info)
    dev.off()
}

print("Clustering filtered data using Hierarchical cluster..")
#   Hierarchical cluster
dd <- dist(t(log_expression_data), method='euclidian')              #   Processing bottleneck
hc <- hclust(dd, method="complete")
postscript(file=hierarchcal_cluster_all_samples_filename, onefile=FALSE, horizontal=FALSE, width=16, height=8, paper="special", family="Times")
    ColorDendrogram(hc, y=ED_colours_all_samples, branchlength=500)
    legend("topright", legend=ED_axis_label, text.col=ED_colours, cex=0.7)
dev.off()


## ===========================================================================
##                             DIFFERENTIAL EXPRESSION ANALYSIS
## ===========================================================================
print("Fitting linear model according to contrast matrix..")

## Fit to linear model
fit <- lmFit(log_expression_data, design)

## Specify all contrasts
contrast.matrix <- makeContrasts(E3-E4, E4-E4_Late, E4_Late-E5_Early, E5_Early-E6, E6-E7, levels = design)
fit <- contrasts.fit(fit, contrast.matrix)
fit2 <- eBayes(fit)
results <- decideTests(fit2)

## Illustrate the number of significantly different genes in each comparison with Venn diagram
bitmap(file=venn_diagram_limma_filename, type="png16m", height=1500, width=800, units="px")
    vennDiagram(results)
dev.off()

## Calculate and export differential expression statistics
differentially_expressed_genes <- vector()

for ( i in list_of_contrasts ){

    #   Generate the differential expression statistics and export them
    tt <- topTable(fit2, coef=i, adjust="BH", n=nrow(log_expression_data))
    write.table(tt, file=paste(output_dir,'differential_expression_',i,'.txt', sep=""),  quote = FALSE, sep=import_export_delimiter)

    #   List the differentially expressed genes (adj.P.Val <= 0.05) for thresholding
    differentially_expressed_genes <- c(differentially_expressed_genes, rownames(subset(tt, adj.P.Val <= 0.05)))
    
    print(paste("Exporting differential expression statistics for",i,sep=" "))
}

## Threshold the expression data based on all genes which are differentially expressed adj.P.Val <= 0.05
## Select a different cutoff?
differentially_expressed_genes <- unique(differentially_expressed_genes)
thresholded_diff_expression <- log_expression_data[rownames(log_expression_data) %in% differentially_expressed_genes,]



############## EXPORT KEY VARIABLES ##################################
#   For the "clustering_and_stability_1" analysis
output_dir <- '/home/lizzi/scratch/Single_cell_analysis/LANNER/output/final/exports/'
objects_to_save <- c("log_expression_data", "cell.type", "ED_colours_all_samples", "ED_axis_label", "ED_colours", "pheno_info")

file_path_to_save_attract <- paste(output_dir,"Input_for_attract",sep="")
save(list=objects_to_save, file=file_path_to_save_attract)

file_path_to_save_clustering <- paste(output_dir,"Input_for_clustering_and_stability",sep="")
save(list=objects_to_save, file=file_path_to_save_clustering)

