# README #

##  For GoogleDoc explaining the analysis go here 

https://docs.google.com/document/d/1ZP-Q4WpLbwfLQh0ULAnC-ZAwle9AwYRxasUo-vtbD8c/edit?usp=sharing


# SINGLE CELL RNA-SEQ DATA ANALYSIS #

*  Version 1: March 2017
*  First Author: Lizzi Mason
*  Second Authors: Shila Ghazanfar, Othmar Korn and Jarny Choi
*  For use in the server pipe.stemformatics (GoldenOrb)

### 1.  Setup.sh ###
    Workspace set up in the Setup.sh script that is self-contained to establish 2 versions of R and all required libraries (with relevant versions).

### 2.  Threshold_inspection_and_differential_expression.r ###
    Performs thresholding and inspection of the unpooled data, followed by differential expression expression analysis using *limma*.

### 3.  Clustering_and_stability_CPM.r ###
    Performs k-means+medeoids clustering of PC1 and PC2.

### 4.  ECDF.r ###
    Clusters based on ECDF, detects ECDF patterns using correlation.

### 5.  Mixture_model_implementation.r ###
    Implements 2-component gamma-gamma mixture model and tests gaussian mixture model, assesses changes to mu and sigma parameters.

### 6.  Correlation_network_analysis.r ###
    Constructs networks based on Pearson correlation of mixture model parameters.