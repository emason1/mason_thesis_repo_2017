#! /usr/bin/Rscript

#   First author : Lizzi Mason
#   Last edited 31-MARCH-2016

#   Edits to be made
#   Density of all genes + all bivalent genes
#   Mean vs cov of 10 bivalent genes
#   Mean vs SD of 10 bivalent genes
#   COV clusters need to be condensed


##############   LOAD LIBRARIES  ##############
require(stats)
require(limma)

############## SPECIFY VARIABLES ##################################
#   1.  Data import
files <- c("hESC_mapped_microarray_data_OGS", "phenotype_info", "hESC_all_genes_normalised_data")
microarray_data_location <- c("data/2015/Microarray/")
#   2.  Kmeans analysis of CoV
columns_of_interest <- c("P7_coVar", "P6_coVar", "P5_coVar", "P4_coVar")
kmeans_variable_3 <- c('Epigenetics/k_means_5_clustering_CoVar.txt')
kmeans_variable_4 <- c('Epigenetics/LINE_graph_k_means_5_clusters_CoVar.EPS')
#   3.  ChIP-seq
#   Gene sets
TFS <- c("H3K27ac", "H3K4me3", "H3K4me1", "H3K27me3", "H3K9me3", "DNase_hypersensitivity")
#   Plot parameters
plot_cols <- c('salmon3', 'steelblue3', 'grey47')
x_axis_label <- c("Distance from TSS in kbp")
#   In the paper, Meissner identified the region of interest as falling 2000bp downstream and 500bp upstream of TSS
#   TSS defined by MACS
upper_region_of_interest <- 5000
lower_region_of_interest <- 5000
#   4.  CoV cluster analysis
epigenetic_folder <- c("Epigenetics/")
chip_data_column_names <- c("MACS_peak", "Chr", "Start", "End", "Strand", "Peak.Score", "Focus.Ratio.Size", "Annotation", "Detailed.Annotation", "Distance.to.TSS", "Nearest.PromoterID", "Entrez.ID", "Nearest.Unigene", "Nearest.Refseq", "Nearest.Ensembl", "Gene.Name", "Gene.Alias", "Gene.Description", "Gene.Type")
chip_data_columns_of_interest <- c("Chr", "Start", "End", "Strand", "Distance.to.TSS", "Nearest.Ensembl", "Gene.Name")
network_file_path <- c("_sox2_oct4_minimal_network_binding_sites.txt")
network_file_path_for_peaks <- c("_sox2_oct4_minimal_network_peaks_in_network_")
base_pairs_from_TSS_extension <- c("_bp_from_TSS.txt")
marks <- c("H3K27ac", "H3K4me3", "H3K4me1", "DNase_hypersensitivity", "H3K27me3", "H3K9me3")
distance_column <- c("Distance.to.TSS")
plot_colours <- c('salmon3', 'steelblue3', 'gray47')
legend_placement <- c("topright")
xlab_distance_from_TSS <- c("Distance from TSS (bp)")
base_pairs_from_TSS_plot_extension <- c("_bp_distance_from_TSS.EPS")
cov_cluster_legend_text <- c("CoVar Clusters 1 and 4", "CoVar Cluster 2 and 3", "CoVar Cluster 5", "All genes")
#   5.  Bivalent analysis
bivalent_genes <- c("OTX2", "GATA4", "GATA6", "PDZD4", "TMEM125", "KIF1A", "ZIC2", "DNMT3B", "FGFR4", "SBK1")
fraction_cols <- rev(c("#D7E3F4","#AACCFF","#5599FF","#0055D4"))
solid_points <- c(16, 16, 16, 16)
mean_density_axis_label <- c("Density log2(Mean) gene expression")
all_expressed_title <- c("All expressed genes")
all_expressed_legend <- c("All Expressed P7", "All Expressed P6", "All Expressed P5", "All Expressed P4")
all_bivalent_title <- c("All bivalent genes")
all_bivalent_legend <- c("All Bivalent P7", "All Bivalent P6", "All Bivalent P5", "All Bivalent P4")
all_network_title <- c("All network genes")
all_network_legend <- c("Network P7", "Network P6", "Network P5", "Network P4")
network_bivalent_title <- c("Network bivalent genes")
network_bivalent_legend <- c("Network Bivalent P7", "Network Bivalent P6", "Network Bivalent P5", "Network Bivalent P4")
mean_axis_label <- c("log2(Mean) gene expression")
cov_axis_label <- c("Coefficient of variation")
sd_axis_label <- c("Standard deviation")
bivalent_pfract_legend <- c("Bivalent P7", "Bivalent P6", "Bivalent P5", "Bivalent P4")
nonbivalent_pfract_legend <- c("Not bivalent P7", "Not bivalent P6", "Not bivalent P5", "Not bivalent P4")
pfract_points <- c(16, 16, 16, 16)
bivalent_plot_title <- c("Bivalent genes in network")
nonbivalent_plot_title <- c("Non-bivalent genes in network")

##############   SOURCE REQUIRED FUNCTIONS  ##############
source(file="/home/lizzi/workspace/wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/scripts/Functions_for_regulatory_network_analysis.r")

##############   SPECIFY FILEPATHS  ##############
base_dir <- '/home/lizzi/workspace/'
input_dir <- paste(base_dir,'data/2015/Epigenome_roadmap_data/',sep="")
output_dir <- paste(base_dir,'wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/output/',sep="")

############## IMPORT NETWORKS AND PRE-PROCESSED ARRAY DATA ##################################
network_genes <- read.csv(paste(base_dir,'data/2015/Networks/sox2_oct4_minimal_network.txt',sep=""), header=FALSE, sep='\t')
network_genes <- as.character(network_genes[,1])
gene_set <- network_genes
#   Assign microarray data in files to variabiles of same name
for ( i in files ){
    assign(i, read.table(file=paste(base_dir,microarray_data_location,i,".txt",sep=""), header=TRUE, sep='\t'))
}
#   Rename the microarray gene expression data
mapped_expression_data <- hESC_mapped_microarray_data_OGS[!rownames(hESC_mapped_microarray_data_OGS) %in% "CKMT1B",]

############## K MEANS CLUSTER ANALYSES OF P-FRACTIONS MICROARRAY DATA ##################################
#   Import array data and phenotype data
background_expression_data <- mapped_expression_data
background_expression_data <- background_expression_data[,rev(colnames(background_expression_data))]
expression_data <- mapped_expression_data[rownames(mapped_expression_data) %in% network_genes,]
expression_data <- expression_data[,rev(colnames(expression_data))]
cell.type <- factor(rev(phenotype_info$Cell_Pheno))

#   Calculate the average and CoV for each phenotype and transform to matrix for operations
for ( i in rev(levels(cell.type)) ){
    expression_data[paste(i,"_coVar",sep="")] <- apply(expression_data[,paste(i, LETTERS[1:3], sep="_")], 1, co.var)
    expression_data[paste(i,"_SD",sep="")] <- apply(expression_data[,paste(i, LETTERS[1:3], sep="_")], 1, sd)
    expression_data[paste(i,"_Mean",sep="")] <- apply(expression_data[,paste(i, LETTERS[1:3], sep="_")], 1, mean)    
}
expression_data <- as.matrix(expression_data)

#   Calculate the average and CoV for each phenotype and transform to matrix for operations
for ( i in rev(levels(cell.type)) ){
    background_expression_data[paste(i,"_coVar",sep="")] <- apply(background_expression_data[,paste(i, LETTERS[1:3], sep="_")], 1, co.var)
    background_expression_data[paste(i,"_SD",sep="")] <- apply(background_expression_data[,paste(i, LETTERS[1:3], sep="_")], 1, sd)
    background_expression_data[paste(i,"_Mean",sep="")] <- apply(background_expression_data[,paste(i, LETTERS[1:3], sep="_")], 1, mean)
}
background_expression_data <- as.matrix(background_expression_data)

#   Kmeans analysis for CoV
covar_data <- expression_data[,columns_of_interest]
clusters_and_expression <- K_means_analysis(5, covar_data, kmeans_variable_3, kmeans_variable_4)
#   For the downstream analysis we have decided to combine clusters 2 and 3
clusters_and_expression$fit.cluster[clusters_and_expression$fit.cluster == 3] <- 2
#   For the downstream analysis we have decided to combine clusters 1 and 4
clusters_and_expression$fit.cluster[clusters_and_expression$fit.cluster == 4] <- 1
#   There should be 3 cluster assignments: 1, 2 and 5
cluster_assignment <- factor(clusters_and_expression$fit.cluster)
rows_reorder_by_cluster_matrix <- rownames(clusters_and_expression)

############## EXPORT MERGED CHIP-SEQ BED FILES FOR A GIVEN GENE SET ############################################
for ( gene in TFS ){
    #   Instantiate variables
    chomosome_location_cluster_assignment <- vector()
    #   List all files containing the TF and cell type of interest
    file_list <- list.files(input_dir, pattern=gene, full.names=T)
    #   Ensuring there is at least 1 file matching the query
    if ( (length(file_list) >= 1) == TRUE ){
        #-----Combine all files into a single data frame, add heading, and output as a single merged file
        temp_data <- do.call("rbind", lapply(file_list, read.csv, header = FALSE, sep='\t'))
        colnames(temp_data) <- chip_data_column_names
        temp_data <- temp_data[,chip_data_columns_of_interest]
        temp_data <- data.frame(lapply(temp_data, as.character), stringsAsFactors=FALSE)
        temp_data_subset <- temp_data[as.numeric(temp_data$Distance.to.TSS) <= upper_region_of_interest & as.numeric(temp_data$Distance.to.TSS) >= -lower_region_of_interest,]
        assign(paste("total_",gene,sep=""), temp_data_subset)
        output_data <- temp_data[temp_data$Gene.Name %in% gene_set,]
        write.table(output_data, file=paste(output_dir,"Merged_bed_files/",gene,network_file_path,sep=""), row.names=FALSE, col.names=TRUE, quote = FALSE, sep='\t')
        
        #-----Plot the peak density per cluster as a function of distance from TSS
        #   Instantiate variables
        peaks <- output_data[as.numeric(output_data$Distance.to.TSS) <= upper_region_of_interest & as.numeric(output_data$Distance.to.TSS) >= -lower_region_of_interest, c("Gene.Name", "Distance.to.TSS")]
        for ( target in peaks$Gene.Name ){
            chomosome_location_cluster_assignment <- c(chomosome_location_cluster_assignment, clusters_and_expression[target,"fit.cluster"])
            }
        peaks_clusters <- cbind(peaks, chomosome_location_cluster_assignment)
        peaks_clusters <- na.omit(peaks_clusters)
        location_by_cluster <- factor(peaks_clusters$chomosome_location_cluster_assignment)
        assign(gene, peaks_clusters)
        write.table(peaks_clusters, file=paste(output_dir,"Epigenetics/",gene,network_file_path_for_peaks,upper_region_of_interest,"_to_-",lower_region_of_interest,base_pairs_from_TSS_extension,sep=""), row.names=FALSE, col.names=TRUE, quote=FALSE, sep='\t')

    }
}

############## PLOTS PER EPIGENETIC MARK ############################################
#   The total epigenetic data is within the +/-5kb region but includes all genes
total_epi_data <- list(na.omit(total_H3K27ac), na.omit(total_H3K4me3), na.omit(total_H3K4me1), na.omit(total_DNase_hypersensitivity), na.omit(total_H3K27me3), na.omit(total_H3K9me3))
names(total_epi_data) <- marks
#   The epigenetic data is for all genes in the network +/-5kb
epi_data <- list(H3K27ac, H3K4me3, H3K4me1, DNase_hypersensitivity, H3K27me3, H3K9me3)
names(epi_data) <- marks

postscript(file=paste(output_dir,epigenetic_folder,"All_marks_",upper_region_of_interest,"_to_-",lower_region_of_interest,base_pairs_from_TSS_plot_extension, sep=""), onefile=FALSE, horizontal=FALSE, width=15, height = 20, paper="special", family="Times")
par(mfrow=c(3,2))
    for ( mark in marks ){
        
        temp_data <- epi_data[[mark]]
        total_temp_data <- total_epi_data[[mark]]
        
        plot(density(as.numeric(total_temp_data[,distance_column])), ylim=c(0, 0.0012), xlim=c(upper_region_of_interest, -lower_region_of_interest), ylab=paste(mark," level", sep=""), xlab=xlab_distance_from_TSS, main=mark, cex.axis=1.5, cex.lab=1.5, cex.main=2.5, col=c("gray87"))

            counter <- 1
            for ( cluster in levels(cluster_assignment) ){
            
                if ( (length(temp_data[temp_data$chomosome_location_cluster_assignment==cluster, distance_column]) >= 2) == TRUE ){
                    lines(density(as.numeric(temp_data[temp_data$chomosome_location_cluster_assignment==cluster, distance_column])), col=plot_colours[counter])
                    counter <- counter + 1    
                }
        
            legend(legend_placement, legend=cov_cluster_legend_text, text.col=c(plot_colours, "gray87"))
        
            }
    }
dev.off()

##############  BIVALENT DOMAINS    ############################################
#------ENSEMBL IDS (there are 2126) FOR CAGE
#   Define the repressive marks
repressive_epigenetic_marks <- list(repressive_H3K27me3=total_H3K27me3[-1,], repressive_H3K9me3=total_H3K9me3[-1,])
ensembl_ids_repressed <- unique(c(repressive_epigenetic_marks[[1]][,"Nearest.Ensembl"], repressive_epigenetic_marks[[2]][,"Nearest.Ensembl"]))
ensembl_ids_repressed <- ensembl_ids_repressed[!is.element(ensembl_ids_repressed, c(""))]
#   Use the repressive marks to subset the active marks
ensembl_bivalent_1 <- total_DNase_hypersensitivity[total_DNase_hypersensitivity$Nearest.Ensembl %in% ensembl_ids_repressed,"Nearest.Ensembl"]
ensembl_bivalent_2 <- total_H3K27ac[total_H3K27ac$Nearest.Ensembl %in% ensembl_ids_repressed,"Nearest.Ensembl"]
ensembl_bivalent_3 <- total_H3K4me3[total_H3K4me3$Nearest.Ensembl %in% ensembl_ids_repressed,"Nearest.Ensembl"]
ensembl_bivalent_4 <- total_H3K4me1[total_H3K4me1$Nearest.Ensembl %in% ensembl_ids_repressed,"Nearest.Ensembl"]
#   Take only the unique identifiers
ensembl_ids_bivalent <- unique(c(ensembl_bivalent_1, ensembl_bivalent_2, ensembl_bivalent_3, ensembl_bivalent_4))
ensembl_ids_bivalent <- ensembl_ids_bivalent[!is.element(ensembl_ids_bivalent, c(""))]

#------OFFICIAL GENE SYMBOLS (there are 2126) FOR MICROARRAY
#   Define the repressive marks
ogs_ids_repressed <- unique(c(repressive_epigenetic_marks[[1]][,"Gene.Name"], repressive_epigenetic_marks[[2]][,"Gene.Name"]))
ogs_ids_repressed <- ensembl_ids_repressed[!is.element(ensembl_ids_repressed, c(""))]
#   Use the repressive marks to subset the active marks
ogs_bivalent_1 <- total_DNase_hypersensitivity[total_DNase_hypersensitivity$Nearest.Ensembl %in% ensembl_ids_repressed,"Gene.Name"]
ogs_bivalent_2 <- total_H3K27ac[total_H3K27ac$Nearest.Ensembl %in% ensembl_ids_repressed,"Gene.Name"]
ogs_bivalent_3 <- total_H3K4me3[total_H3K4me3$Nearest.Ensembl %in% ensembl_ids_repressed,"Gene.Name"]
ogs_bivalent_4 <- total_H3K4me1[total_H3K4me1$Nearest.Ensembl %in% ensembl_ids_repressed,"Gene.Name"]
#   Take only the unique identifiers
ogs_ids_bivalent <- unique(c(ogs_bivalent_1, ogs_bivalent_2, ogs_bivalent_3, ogs_bivalent_4))


#------Is the expression of all genes different to that of all bivalent genes?

#   Log2 the quantile normalised and background corrected dataset with all genes (both detected and undetected)
normalised_data <- log2(hESC_all_genes_normalised_data)
normalised_data_colour <- c("gray87")
normalised_data_legend <- c("All detected and\nundetected genes")

postscript(file=paste(output_dir,epigenetic_folder,"Background_combined_bivalent_non_bivalent.EPS", sep=""), onefile=FALSE, horizontal=FALSE, width=20, height=20, paper="special", family="Times")
par(mfrow=c(2,2))
    temp <- background_expression_data
    plot(density(temp[,"P7_Mean"]), xlim=c(2, 16), ylim=c(0, 0.53), col=fraction_cols[1], ylab=mean_density_axis_label, main=all_expressed_title, cex.axis=1.5, cex.lab=1.5, cex.main=2.5)
        lines(density(temp[,"P6_Mean"]), col=fraction_cols[2])
        lines(density(temp[,"P5_Mean"]), col=fraction_cols[3])
        lines(density(temp[,"P4_Mean"]), col=fraction_cols[4])
        lines(density(as.matrix(normalised_data)), col=normalised_data_colour)
        legend(legend_placement, legend=c(all_expressed_legend,normalised_data_legend), col=c(fraction_cols,normalised_data_colour), text.col=c(fraction_cols,normalised_data_colour), cex=2)
    temp <- background_expression_data[rownames(background_expression_data) %in% ogs_ids_bivalent,]
    plot(density(temp[,"P7_Mean"]), xlim=c(2, 16), ylim=c(0, 0.53), col=fraction_cols[1], ylab=mean_density_axis_label, main=all_bivalent_title, cex.axis=1.5, cex.lab=1.5, cex.main=2.5)
        lines(density(temp[,"P6_Mean"]), col=fraction_cols[2])
        lines(density(temp[,"P5_Mean"]), col=fraction_cols[3])
        lines(density(temp[,"P4_Mean"]), col=fraction_cols[4])
        lines(density(as.matrix(normalised_data)), col=normalised_data_colour)
        legend(legend_placement, legend=c(all_bivalent_legend,normalised_data_legend), col=c(fraction_cols,normalised_data_colour), text.col=c(fraction_cols,normalised_data_colour), cex=2)
    temp <- clusters_and_expression
    plot(density(temp[,"P7_Mean"]), col=fraction_cols[1], ylab=mean_density_axis_label, main=all_network_title, cex.axis=1.5, cex.lab=1.5, cex.main=2.5)
        lines(density(temp[,"P6_Mean"]), col=fraction_cols[2])
        lines(density(temp[,"P5_Mean"]), col=fraction_cols[3])
        lines(density(temp[,"P4_Mean"]), col=fraction_cols[4])
        legend(legend_placement, legend=all_network_legend, col=fraction_cols, text.col=fraction_cols, cex=2)
    temp <- clusters_and_expression[(rownames(clusters_and_expression) %in% bivalent_genes),]
    plot(density(temp[,"P7_Mean"]), col=fraction_cols[1], ylab=mean_density_axis_label, main=network_bivalent_title, cex.axis=1.5, cex.lab=1.5, cex.main=2.5)
        lines(density(temp[,"P6_Mean"]), col=fraction_cols[2])
        lines(density(temp[,"P5_Mean"]), col=fraction_cols[3])
        lines(density(temp[,"P4_Mean"]), col=fraction_cols[4])
        legend(legend_placement, legend=network_bivalent_legend, col=fraction_cols, text.col=fraction_cols, cex=2)
dev.off()

all_network_genes_KS <- as.matrix(clusters_and_expression[,c("P7_Mean", "P6_Mean", "P5_Mean", "P4_Mean")])
all_network_genes_KS <- as.numeric(all_network_genes_KS)
bivalent_network_genes_KS <- as.matrix(clusters_and_expression[(rownames(clusters_and_expression) %in% bivalent_genes),c("P7_Mean", "P6_Mean", "P5_Mean", "P4_Mean")])
bivalent_network_genes_KS <- as.numeric(bivalent_network_genes_KS)
#   Use a non-parametric Kolmogorov-Smirnov test used to test for significant differences between degree distributions
test <- ks.test(all_network_genes_KS, bivalent_network_genes_KS)
#   p 0.03984

#------Is the mean value forcing a relationship with the CoV?  Look at the standard deviation
postscript(file=paste(output_dir,epigenetic_folder,"Combined_bivalent_non_bivalent.EPS", sep=""), onefile=FALSE, horizontal=FALSE, width=20, height=20, paper="special", family="Times")
par(mfrow=c(2,2))
    median_expression <- median(background_expression_data[,1:12])
    temp <- clusters_and_expression[rownames(clusters_and_expression) %in% bivalent_genes,]
    plot(temp[,"P7_coVar"], temp[,"P7_Mean"], xlim=c(0,6), ylim=c(4, 16), pch=16, col=fraction_cols[1], xlab=cov_axis_label, ylab=mean_axis_label, main=bivalent_plot_title, cex.axis=1.5, cex.lab=1.5, cex.main=2.5)
        points(temp[,"P6_coVar"], temp[,"P6_Mean"],  pch=16, col=fraction_cols[2])
        points(temp[,"P5_coVar"], temp[,"P5_Mean"],  pch=16, col=fraction_cols[3])
        points(temp[,"P4_coVar"], temp[,"P4_Mean"],  pch=16, col=fraction_cols[4])
        abline(h=5, col=c("red"))
        abline(h=median_expression, col=c("red"))
        legend(legend_placement, legend=bivalent_pfract_legend, pch=pfract_points, col=fraction_cols, text.col=fraction_cols, cex=2)
    temp <- clusters_and_expression[!(rownames(clusters_and_expression) %in% bivalent_genes),]
    plot(temp[,"P7_coVar"], temp[,"P7_Mean"], xlim=c(0,6), ylim=c(4, 16), pch=16, col=fraction_cols[1], xlab=cov_axis_label, ylab=mean_axis_label, main=nonbivalent_plot_title, cex.axis=1.5, cex.lab=1.5, cex.main=2.5)
        points(temp[,"P6_coVar"], temp[,"P6_Mean"],  pch=16, col=fraction_cols[2])
        points(temp[,"P5_coVar"], temp[,"P5_Mean"],  pch=16, col=fraction_cols[3])
        points(temp[,"P4_coVar"], temp[,"P4_Mean"],  pch=16, col=fraction_cols[4])
        abline(h=5, col=c("red"))
        abline(h=median_expression, col=c("red"))
        legend(legend_placement, legend=nonbivalent_pfract_legend, pch=pfract_points, col=fraction_cols, text.col=fraction_cols, cex=2)
    temp <- clusters_and_expression[rownames(clusters_and_expression) %in% bivalent_genes,]
    plot(temp[,"P7_SD"], temp[,"P7_Mean"], xlim=c(0,1), ylim=c(4, 16), pch=8, col=fraction_cols[1], xlab=sd_axis_label, ylab=mean_axis_label, main=bivalent_plot_title, cex.axis=1.5, cex.lab=1.5, cex.main=2.5)
        points(temp[,"P6_SD"], temp[,"P6_Mean"],  pch=16, col=fraction_cols[2])
        points(temp[,"P5_SD"], temp[,"P5_Mean"],  pch=16, col=fraction_cols[3])
        points(temp[,"P4_SD"], temp[,"P4_Mean"],  pch=16, col=fraction_cols[4])
        abline(h=5, col=c("red"))
        abline(h=median_expression, col=c("red"))
        legend(legend_placement, legend=bivalent_pfract_legend, pch=pfract_points, col=fraction_cols, text.col=fraction_cols, cex=2)
    temp <- clusters_and_expression[!(rownames(clusters_and_expression) %in% bivalent_genes),]
    plot(temp[,"P7_SD"], temp[,"P7_Mean"], xlim=c(0,1), ylim=c(4, 16), pch=16, col=fraction_cols[1], xlab=sd_axis_label, ylab=mean_axis_label, main=nonbivalent_plot_title, cex.axis=1.5, cex.lab=1.5, cex.main=2.5)
        points(temp[,"P6_SD"], temp[,"P6_Mean"],  pch=16, col=fraction_cols[2])
        points(temp[,"P5_SD"], temp[,"P5_Mean"],  pch=16, col=fraction_cols[3])
        points(temp[,"P4_SD"], temp[,"P4_Mean"],  pch=16, col=fraction_cols[4])
        abline(h=5, col=c("red"))
        abline(h=median_expression, col=c("red"))
        legend(legend_placement, legend=nonbivalent_pfract_legend, pch=pfract_points, col=fraction_cols, text.col=fraction_cols, cex=2)
dev.off()


