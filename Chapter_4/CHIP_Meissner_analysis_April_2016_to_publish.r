#! /usr/bin/Rscript

#   First author : Lizzi Mason
#   Last edited 09-JUNE-2016

##############   LOAD LIBRARIES  ##############
require(stats)
require(limma)

##############   SOURCE REQUIRED FUNCTIONS  ##############
#   Regulatory network analysis functions
source(file="/home/lizzi/workspace/wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/scripts/Functions_for_regulatory_network_analysis.r")
#   Coexpression network analysis functions
source(file="/home/lizzi/workspace/wells_lab/2014_library/Network_Functions_for_R/Network_functions.r")

##############   SPECIFY FILEPATHS  ##############
base_dir <- '/home/lizzi/workspace/'
input_dir <- paste(base_dir,'data/2015/Meissner_CHIP_seq_processed/',sep="")
output_dir <- paste(base_dir,'wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/output/',sep="")

############## IMPORT NETWORKS AND PRE-PROCESSED ARRAY DATA ##################################
#   Network gene set
network_genes <- read.csv(paste(base_dir,'data/2015/Networks/sox2_oct4_minimal_network.txt',sep=""), header=FALSE, sep='\t')
network_genes <- as.character(network_genes[,1])
#   Network ensembl genome coordinates per gene
genome_coordinates_network <- read.csv(paste(base_dir,'data/2015/Networks/sox2_oct4_minimal_network_genome_coordinates.csv',sep=""), header=TRUE, sep='\t')
rownames(genome_coordinates_network) <- genome_coordinates_network$GENE
genome_coordinates_network <- genome_coordinates_network[,-1]

#   Assign HESC microarray data in files to variabiles of same name
files <- c("hESC_mapped_microarray_data_OGS", "phenotype_info")
for ( i in files ){
    assign(i, read.table(file=paste(base_dir,"data/2015/Microarray/",i,".txt",sep=""), header=TRUE, sep='\t'))
}
#   Rename the microarray gene expression data
mapped_expression_data <- hESC_mapped_microarray_data_OGS[!rownames(hESC_mapped_microarray_data_OGS) %in% "CKMT1B",]

#   Assign IPSC microarray data in files to variabiles of same name
assign("iPSC_array_data", read.table(file=paste(base_dir,"data/2015/Microarray/2016_04_15_iPSC_mapped_microarray_data_OGS.txt",sep=""), header=TRUE, sep='\t'))
#   Rename the microarray gene expression data
iPSC_array_data <- iPSC_array_data[!rownames(iPSC_array_data) %in% c("CKMT1B", "GATA4", "GATA6"),]
#   Build correlation network with iPSC array data
iPSC_array_data_network <- iPSC_array_data[rownames(iPSC_array_data) %in% network_genes,]
iPSC_cor <- cor(t(iPSC_array_data_network), method = 'pearson', use='pairwise.complete.obs')
directory_to_save <- paste(output_dir,'Correlation_network_analysis/iPSC_array_sox2_oct4_network_',sep="")
iPSC_network <- build_correlation_network(iPSC_cor, 0.6, -0.6, directory_to_save, "iPSC_array")

############## SPECIFY VARIABLES ##################################
#   Gene sets
cell_types <- c("hESC", "endo", "ecto", "meso")
gene_set <- network_genes
TFS <- c("NANOG", "POU5F1", "SOX2", "OTX2", "GATA4", "GATA6")
all_genes <- unlist(c(TFS, gene_set))
#   Plot parameters
plot_cols <- c("black", "blue", "red")
x_axis_label <- c("Distance from TSS in kbp")
#   Assign regions of interest
#   VARIABLES USED THROUGHOUT THE SCRIPTING
#   1.  SUBSET GENES IN HEATMAP CLUSTERS FOR HOMER ANALYSIS
    heatmap_file <- c("/home/lizzi/workspace/wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/input/heatmap_clusters.csv")
    cnames_heatmap_data <- c("Gene", "Cluster")
    motif_analysis_filename <- c('Motif_analysis/Gene_set_for_heatmap_cluster_')
#   2.  CO-EXPRESSION NETWORK ANALYSES OF P-FRACTIONS MICROARRAY DATA
    replicates <- c("A", "B", "C")
    samples_of_interest <- c("P7_A", "P7_B", "P7_C")
    positive.correlation.cutoff <- 0.8
    negative.correlation.cutoff <- -0.8
    directory_to_save <- paste(output_dir,'Correlation_network_analysis/',sep="")
    R_data_location <- paste(output_dir,'Correlation_network_analysis/P7_minimal_edges.RData',sep="")
    correlation_network_directory <- paste(output_dir,'Correlation_network_analysis/',sep="")
    network_name <- c("_no_threshold_P7_membrane_coexpression_network")
    pearson_correlation_table_colnames <- c("P4_Pearson", "P5_Pearson", "P6_Pearson", "P7_Pearson")
    PCA.cols <- rev(c("#0055D4","#5599FF", "#AACCFF", "#D7E3F4"))
    phenotype_labels <- c("P7", "P6", "P5", "P4")
    MDS_plot_title <- c("MDS (Metric) edge changes in POU5F1/SOX2 Network")
    correlation_heatmap_filepath <- c("Cluster_analysis/Heatmap_network_common_edges_clustering_edges.EPS")
    hierarchical_cluster_filepath <- c("Cluster_analysis/Hierarchical_cluster_network_common_edges_clustering_edges.EPS")
    hierarchical_cluster_title <- c("Coexpression Network Edge Clusters")
    heatmap_cluster_assignment_filepath <- c('Cluster_analysis/Heatmap_cluster_assignment.txt')
    heatmap_cluster_assignment_filepath_group <- c('Cluster_analysis/Gene_set_for_heatmap_cluster_')
    heatmap_cluster_groups_R_Data_filename <- c('Cluster_analysis/Heatmap_cluster_groups.RData')
#   3.  COVAR and K MEANS CLUSTER ANALYSES OF P-FRACTIONS MICROARRAY DATA
    CAGE_missing_genes_filename <- c('Differential_expression/Expression_of_genes_missing_in_FANTOM5_CAGE_gene_level_summary.EPS')
    CAGE_missing_genes_plot_y_lab <- c("Mean log(2) gene expression")
    CAGE_missing_genes_plot_x_lab <- c("Phenotype")
    CAGE_missing_genes_plot_title <- c("Expression of missing genes")
    COV_Kmeans_columns <- c("P7_coVar", "P6_coVar", "P5_coVar", "P4_coVar")
    COV_Kmeans_filepath <- c('Cluster_analysis/k_means_5_clustering_CoVar.txt')
    COV_Kmeans_plot_filepath <- c('Cluster_analysis/LINE_graph_k_means_5_clusters_CoVar.EPS')
    COV_Kmeans_filepath_4 <- c('Cluster_analysis/k_means_4_clustering_CoVar.txt')
    COV_Kmeans_plot_filepath_4 <- c('Cluster_analysis/LINE_graph_k_means_4_clusters_CoVar.EPS')
    Supplementary_info_filepath <- c('Differential_expression/Top_table_differential_expression_P7_P4_dataset_with_CoVar_clusters.txt')
#   4.  EXPORT MERGED CHIP-SEQ BED FILES FOR A GIVEN GENE SET
    CHIP_colnames <- c("MACS_peak", "Chr", "Start", "End", "Strand", "Peak.Score", "Focus.Ratio.Size", "Annotation", "Detailed.Annotation", "Distance.to.TSS", "Nearest.PromoterID", "Entrez.ID", "Nearest.Unigene", "Nearest.Refseq", "Nearest.Ensembl", "Gene.Name", "Gene.Alias", "Gene.Description", "Gene.Type")
    CHIP_cols_of_interest <- c("Chr", "Start", "End", "Strand", "Distance.to.TSS", "Entrez.ID", "Gene.Name")
#   5.  CREATE UNIQUE PROMOTER LEVEL GENE REGULATORY NETWORKS IN EACH CELL PHENOTYPE FOR THE GIVEN GENE SET
    promoter_upper_region_of_interest <- 50
    promoter_lower_region_of_interest <- 300
#   6.  CREATE UNIQUE ENHANCER LEVEL GENE REGULATORY NETWORKS IN EACH CELL PHENOTYPE FOR THE GIVEN GENE SET
    enhancer_upper_region_of_interest <- 200
    enhancer_lower_region_of_interest <- 200
    Enhancer_cluster_filename <- c("Enhancer_analysis/Enhancer_gene_regulatory_networks.bed")
    cnames <- c("ID", "Chr", "Start", "End", "Strand")
    Enhancer_TF_binding_output_filename <- c("Enhancer_analysis/TF_binding_at_expressed_enhancers_")
#   7.  CREATE DUAL NETWORKS TO VISUALISE EDGE DIFFERENCES BETWEEN 2 CONDITIONS
    file_name_1_A <- c("Cytoscape/pluripotent_hESC_to_")
    file_name_1_B <- c("Cytoscape/differentiated_hESC_to_")
    file_name_2 <- c("_regulatory_network_edge_attributes_")
    file_name_3 <- c("_bp_from_TSS_sox2_oct4_network.txt")
    
############## SUBSET GENES IN HEATMAP CLUSTERS FOR HOMER ANALYSIS ############## 
#   Import and subset
heatmap_clusters <- read.csv(heatmap_file, header=FALSE, sep='\t')
colnames(heatmap_clusters) <- cnames_heatmap_data
cluster_assignment <- factor(heatmap_clusters$Cluster)
unique_clusters <- vector()
for ( i in levels(cluster_assignment) ){
    temp <- subset(heatmap_clusters, heatmap_clusters$Cluster == i)
    write.table(temp, file=paste(output_dir,motif_analysis_filename,i,'.txt', sep=""),  quote=FALSE, row.names=FALSE, sep="\t")
}

############## DIFFERENTIAL EXPRESSION ANALYSES OF P-FRACTIONS MICROARRAY DATA ##################################
#   Import array data and phenotype data
all_genes <- unique(as.character(unlist(c(gene_set,TFS))))
expression_data <- mapped_expression_data[rownames(mapped_expression_data) %in% all_genes,]
expression_data <- expression_data[,rev(colnames(expression_data))]
cell.type <- factor(rev(phenotype_info$Cell_Pheno))
#   This uses the Limma Bioconductor package for R
#   Define the phenotypic contrasts
design <- model.matrix(~0 + cell.type)
colnames(design) <- levels(cell.type)
#	Fit the normalised expression data with a linear model
fit <- lmFit((expression_data), design)
contrast.matrix <- makeContrasts(P7-P4, levels = design)
fit2 <- contrasts.fit(fit, contrast.matrix)
fit3 <- eBayes(fit2)
tt <- topTable(fit3, adjust="BH", n=nrow(expression_data))
write.table(tt, file=paste(output_dir,'Differential_expression/Top_table_differential_expression_P7_P4_dataset.txt', sep=""),  quote = FALSE, sep="\t")

############## CO-EXPRESSION NETWORK ANALYSES OF P-FRACTIONS MICROARRAY DATA ##################################
#   Visualisation of minimal-network-edge changes between phenotypes
#   Cast P7 as phenotype of interest and use the edge list present in P7 (pluripotent cell type) to map minimal network edge changes in the other phenotypes
        #-----Define the co-expression network in P7
correlation_subset <- expression_data[,samples_of_interest]
correlation.matrix <- cor(t(correlation_subset))

P7_coexpression.network <- build_correlation_network(correlation.matrix, positive.correlation.cutoff , negative.correlation.cutoff, directory_to_save, "P7")
P7_minimal_edges <- rownames(P7_coexpression.network)
save(P7_minimal_edges, file=R_data_location)

        #-----Use this gene set to define changes to correlation strength in each phenotype
for (i in levels(cell.type) ){

    cnames <- vector()
    for ( j in 1:length(replicates) ){ 
        cnames <- c(cnames, paste(i,"_",replicates[j],sep=""))
    }
        #----Compute a Pearson correlation matrix
            #----Compute a Pearson correlation matrix
    temp <- subset(expression_data, select = cnames)
    correlation.matrix <- cor(t(temp), method = 'pearson', use='pairwise.complete.obs')
        #----Compute the edge changes
    minimal.coexpression.network.no.threshold <- build_correlation_network_no_thresholding(correlation.matrix, correlation_network_directory, i, P7_minimal_edges)
    assign(paste(i,network_name,sep=""), minimal.coexpression.network.no.threshold)
}

        #-----For every edge calculate the ratio of expression in each sample
        #   Abundance in P7 POU5F1 always denominator (relative to abundance)

edge_ratios <- P7_coexpression.network[,c("GENE_A", "GENE_B")]
samples <- phenotype_info[,"Sample_Name"]
gene_a <- edge_ratios[,"GENE_A"]
gene_b <- edge_ratios[,"GENE_B"]
cnames <- c("GENE_A", "GENE_B")

for ( j in samples ){
cnames <- c(cnames, paste(j,"Ratio",sep="_"), paste(j, "Inverse_Ratio",sep="_"))
sample_ratio <- vector()
inverse_sample_ratio <- vector()

    for ( i in 1:nrow(edge_ratios)  ){
            
            a <- expression_data[gene_a[i],j]
            b <- expression_data[gene_b[i],j]
            
            ratio <- a / b
            sample_ratio <- c(sample_ratio, ratio)

            inverse_ratio <- b / a
            inverse_sample_ratio <- c(inverse_sample_ratio, inverse_ratio)
            
            
    }

edge_ratios <- cbind(edge_ratios, sample_ratio, inverse_sample_ratio)

}

colnames(edge_ratios) <- cnames


#   Visualise the data in a MDS plot and a heatmap
        #----Generate a single data frame of Pearson R values for all phenotypes
pearson.matrix <- cbind(as.matrix(P4_no_threshold_P7_membrane_coexpression_network$PEARSON), as.matrix(P5_no_threshold_P7_membrane_coexpression_network$PEARSON), as.matrix(P6_no_threshold_P7_membrane_coexpression_network$PEARSON), as.matrix(P7_no_threshold_P7_membrane_coexpression_network$PEARSON))
colnames(pearson.matrix) <- pearson_correlation_table_colnames
rownames(pearson.matrix) <- rownames(P4_no_threshold_P7_membrane_coexpression_network)
pearson.matrix <- as.data.frame(pearson.matrix, stringsAsFactors=FALSE)
pearson.matrix <- data.matrix(pearson.matrix)
network_matrix <- cbind(pearson.matrix, edge_ratios)
write.table(network_matrix, file=paste(directory_to_save,"OCT4_SOX2_Network_edge_ratio_and_correlation_values", sep=""),  quote = FALSE, sep="\t")

for ( i in rownames(network_matrix) ){

bars <- rev(as.numeric(network_matrix[i,1:4]))
y <- rev(as.numeric(network_matrix[i,c(7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29)]))
inv_y <- rev(as.numeric(network_matrix[i,c(8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30)]))

postscript(file=paste(directory_to_save,i,'correlation_and_ratio.EPS', sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(bars, col=rev(PCA.cols), xlim=c(0,5), ylim=c(-1, 1), ylab=c("Pearson correlation"), main=paste("Correlation and ratio of",network_matrix[i,"GENE_A"],"and",network_matrix[i,"GENE_B"],sep=" "))
    x_lines <- seq(from=0.25, to=4.7, length.out=12)
    lines(x_lines, y-1, col=c("black"))
    points(x_lines, y-1, pch=16, col=c("black"))
    axis(side=4, at=seq(from=-1, to=1, by=0.5), labels=seq(from=0, to=2, by=0.5))
dev.off()

}

#   Transform the matrix so the distance matrix can be computed (this is row-wise)
temp <- t(pearson.matrix)
#   Compute the euclidean distances between the rows (in this case the samples) into a distance matrix called d
d <- dist(temp, method="euclidean")
        #----2 dimensional MDS plots (PCA)
#   Compute the multdimensional scaling, returning the eigenvectors (eig) and selecting 3 dimensions (k)
#   http://stat.ethz.ch/R-manual/R-devel/library/stats/html/cmdscale.html
MDS <- cmdscale(d, eig=TRUE, k=3)
#   Define plot colours per phenotype
#   Export the MDS plot
postscript(file=paste(output_dir,'Principal_components/MDS_1_2_Pearson_correlation_networks.EPS', sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    plot(MDS$points[,1], MDS$points[,2], pch=16, col=PCA.cols, cex=3, xlab="Component 1", ylab="Component 2", main=MDS_plot_title)
    legend("topright", phenotype_labels, cex=1.5, pch=16, col=PCA.cols, text.col=rev(PCA.cols))
dev.off()

postscript(file=paste(output_dir,'Principal_components/MDS_1_3_Pearson_correlation_networks.EPS', sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    plot(MDS$points[,1], MDS$points[,3], pch=16, col=PCA.cols, cex=3, xlab="Component 1", ylab="Component 3", main=MDS_plot_title)
    legend("topright", phenotype_labels, cex=1.5, pch=16, col=PCA.cols, text.col=rev(PCA.cols))
dev.off()

postscript(file=paste(output_dir,'Principal_components/MDS_2_3_Pearson_correlation_networks.EPS', sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    plot(MDS$points[,2], MDS$points[,3], pch=16, col=PCA.cols, cex=3, xlab="Component 2", ylab="Component 3", main=MDS_plot_title)
    legend("topright", phenotype_labels, cex=1.5, pch=16, col=PCA.cols, text.col=rev(PCA.cols))
dev.off()

        #----Hierarchical clustering and heatmap
#   Define the colour palette for the heatmap
hmcols<-colorRampPalette(c("midnightblue", "gold2"))(300)
#   The data needs to be transformed to a numeric matrix, a standard matrix does not work in this instance
h.clust.data.matrix <- data.matrix(pearson.matrix)
dat <- h.clust.data.matrix[,rev(colnames(h.clust.data.matrix))]

#   Export the heatmap of edge changes
postscript(file=paste(output_dir, correlation_heatmap_filepath, sep=""), onefile=FALSE, horizontal=FALSE, width= 22, height = 24, paper="special", family="Times")
    heatmap(x=dat, Colv=NA, distfun=dist, hclustfun = function(x) hclust(x,method = 'average'), scale=c("column"), na.rm=TRUE, keep.dendro=TRUE, 
    col=hmcols, margins=c(10, 10), labCol=NULL, labRow=NULL, ColSideColors=rev(PCA.cols))
    legend("topright",legend=c(colnames(dat)),fill=rev(PCA.cols), border=TRUE, bty="n", y.intersp=2, cex=0.7)
dev.off()

#   Define hierarchical clusters of edges and cut the tree at 8 groups
#   Using average rather than complete clustering (improves heatmap)
d <- dist(dat, method='euclidean')
heatmap_cluster_groups <- hclust(d, method='average')
postscript(file=paste(output_dir, hierarchical_cluster_filepath, sep=""), onefile=FALSE, horizontal=FALSE, width= 30, height = 15, paper="special", family="Times")
    plot(heatmap_cluster_groups, main=hierarchical_cluster_title, hang=-1)
    groups <- cutree(heatmap_cluster_groups, k=8)
    rect.hclust(heatmap_cluster_groups, k=8, border="red") 
dev.off()

#   Export cluster assignments for ChIP and histone analysis
write.table(groups, file=paste(output_dir,heatmap_cluster_assignment_filepath, sep=""),  quote = FALSE, sep="\t")
save(heatmap_cluster_groups, file=paste(output_dir, heatmap_cluster_groups_R_Data_filename, sep=""))

#   Export cluster assignments as individual lists for HOMER analysis
for ( cluster in 1:max(groups) ){
this_group <- subset(groups, groups==cluster)
write.table(this_group, file=paste(output_dir,heatmap_cluster_assignment_filepath_group, cluster, ".txt", sep=""),  quote = FALSE, sep="\t")
}

############## COVAR and K MEANS CLUSTER ANALYSES OF P-FRACTIONS MICROARRAY DATA ##################################
    #----COV Mean SD analysis
    #   Calculate the average and CoV for each phenotype and transform to matrix for operations
for ( i in rev(levels(cell.type)) ){
    expression_data[paste(i,"_coVar",sep="")] <- apply(expression_data[,paste(i, LETTERS[1:3], sep="_")], 1, co.var)
    expression_data[paste(i,"_SD",sep="")] <- apply(expression_data[,paste(i, LETTERS[1:3], sep="_")], 1, sd)
    expression_data[paste(i,"_Mean",sep="")] <- apply(expression_data[,paste(i, LETTERS[1:3], sep="_")], 1, mean)    
}
expression_data <- as.matrix(expression_data)
expression_data <- transform(expression_data, Fold_Change_P7_P4 = P7_Mean / P4_Mean)

#   Some genes are falling out of the accompanying CAGE-gene-summary analysis
#   Plot their expression in this dataset
missing_genes <- expression_data[rownames(expression_data) %in% c("POU5F1P4", "CD24P4", "CKMT1A", "MIR600HG"), grep("_Mean", colnames(expression_data))]
missing_genes_col <- rainbow(nrow(missing_genes))
postscript(file=paste(output_dir, CAGE_missing_genes_filename, sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    plot(x=NULL, y=NULL, xlim=c(1,4), ylim=c(6,max(missing_genes)), type="n", axes=FALSE, ylab=CAGE_missing_genes_plot_y_lab, xlab=CAGE_missing_genes_plot_x_lab, main=CAGE_missing_genes_plot_title)
    axis(1, at=1:ncol(missing_genes), lab=colnames(missing_genes))
    axis(2)
    box()
    counter <- 1
    for ( i in rownames(missing_genes) ){
        lines(missing_genes[i,], type="o", col=missing_genes_col[counter])
        counter <- counter+1
    }
    legend("bottomleft", legend=c(rownames(missing_genes)), fill=missing_genes_col, border=TRUE, bty="n")
dev.off()

    #----COV Kmeans cluster analysis
covar_data <- expression_data[,COV_Kmeans_columns]
#clusters_and_expression <- K_means_analysis(4, covar_data, COV_Kmeans_filepath_4, COV_Kmeans_plot_filepath_4)
clusters_and_expression <- K_means_analysis(5, covar_data, COV_Kmeans_filepath, COV_Kmeans_plot_filepath)
cluster_assignment <- factor(clusters_and_expression$fit.cluster)
rows_reorder_by_cluster_matrix <- rownames(clusters_and_expression)
#   Process the data into one data frame and export
rownames(tt) <- tt$ID
tt_reordered <- tt[rows_reorder_by_cluster_matrix,-1]
tt_reordered$Cluster <- clusters_and_expression$fit.cluster
write.table(tt_reordered, file=paste(output_dir, Supplementary_info_filepath, sep=""),  quote = FALSE, sep="\t")

############## EXPORT MERGED CHIP-SEQ BED FILES FOR A GIVEN GENE SET ############################################
for ( gene in TFS ){
    for ( cell in cell_types ) {
        #   List all files containing the TF and cell type of interest
        file_list <- list.files(paste(base_dir,'data/2015/Meissner_CHIP_seq_processed',sep=""), pattern=paste(gene,"_",cell,sep=""), full.names=T)
        #   Ensuring there is at least 1 file matching the query
        if ( (length(file_list) >= 1) == TRUE ){
            #-----Combine all files into a single data frame, add heading, and output as a single merged file
            temp_data <- do.call("rbind", lapply(file_list, read.csv, header = FALSE, sep='\t'))
            colnames(temp_data) <- CHIP_colnames
            temp_data <- temp_data[,CHIP_cols_of_interest]
            temp_data <- data.frame(lapply(temp_data, as.character), stringsAsFactors=FALSE)
            output_data <- temp_data[temp_data$Gene.Name %in% all_genes,]
            write.table(output_data, file=paste(output_dir,"Merged_bed_files/",gene,cell,"_sox2_oct4_network_binding_sites.txt",sep=""), row.names=FALSE, col.names=TRUE, quote = FALSE, sep='\t')
        }
    }
}
############## CREATE UNIQUE PROMOTER LEVEL GENE REGULATORY NETWORKS IN EACH CELL PHENOTYPE FOR THE GIVEN GENE SET  ##################################
#   Instantiate a new matrix from microarray data and cluster assignment
#   PROXIMAL PROMOTERS

ensembl_ids <- as.character(genome_coordinates_network$ENSEMBL)

clusters_expression_binding <- clusters_and_expression
for ( cell in cell_types ){
    #   Create a phenotype level matrix with the network nodes as rownames and TFs as column names
    assign("square_adjacency_matrix_promoter", matrix(0, ncol=length(all_genes), nrow=length(all_genes), dimnames=list(all_genes, all_genes)))
    assign("cytoscape_data_promoter", matrix(0, ncol=length(TFS), nrow=length(all_genes), dimnames=list(all_genes, TFS)))
    
    for ( gene in TFS ){
        #   Grab the data and identify the peaks per TF
        file_list <- list.files(paste(base_dir,'data/2015/Meissner_CHIP_seq_processed',sep=""), pattern=paste(gene,"_",cell,sep=""), full.names=T)
        if ( (length(file_list) >= 1) == TRUE ){
            
                temp_data <- do.call("rbind", lapply(file_list, read.csv, header = FALSE, sep='\t'))
                colnames(temp_data) <- CHIP_colnames
                temp_data <- data.frame(lapply(temp_data, as.character), stringsAsFactors=FALSE)
            #   Subset based on chromosome location
            temp_data <- temp_data[as.character(temp_data$Gene.Name) %in% as.character(rownames(genome_coordinates_network)) , ]
            #   Use a frequency table to count the number of peaks per gene
            peaks <- as.matrix(table(temp_data[as.numeric(temp_data$Distance.to.TSS) <= promoter_upper_region_of_interest & as.numeric(temp_data$Distance.to.TSS) >= -promoter_lower_region_of_interest,"Gene.Name"]))
            #   Use the peak data to fill the cytoscape_data table with the number of peaks
            #   To visualise edge differences make the values in the square adjacency matrix (N x N) == 0 or 1 for absence or presence of edge
            for ( i in rownames(peaks) ){
                square_adjacency_matrix_promoter[i,gene] <- 1
                cytoscape_data_promoter[i,gene] <- 1
            }
        }
    }

    #   Assign the matrix for each cell type:  This will be used downstream to create the dual networks
    assign(paste(cell,"_adjacency_promoter",sep=""), square_adjacency_matrix_promoter)
    assign(paste(cell,"_table_peaks",sep=""), cytoscape_data_promoter)

}

write.table(clusters_expression_binding, file=paste(output_dir,"Cluster_analysis/summarized_table_of_promoter_expression_clusters_and_binding.txt",sep=""), row.names=TRUE, quote=FALSE, sep='\t')

############## CREATE PIE CHARTS TO SEE EDGE DIFFERENCES BETWEEN 2 CONDITIONS ##################################
pluripotent_pie <- 2*(hESC_table_peaks) + meso_table_peaks
differentiated_pie <- 2*(meso_table_peaks) + hESC_table_peaks

write.table(pluripotent_pie, file=paste(output_dir,"Degree_analysis/pluripotent_hESC_to_meso.txt",sep=""), row.names=TRUE, quote=FALSE, sep='\t')
write.table(differentiated_pie, file=paste(output_dir,"Degree_analysis/differentiated_hESC_to_meso.txt",sep=""), row.names=TRUE, quote=FALSE, sep='\t')


postscript(file=paste(output_dir,'Degree_analysis/Diff_hESC_to_meso_pie.EPS', sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    pie(table(differentiated_pie, exclude=0), labels=c("None", "Unique", "Common"), col=c("lightgray", "darkgray", "black"), main=c("Mesoderm gene regulatory network"), cex=2)
dev.off()


postscript(file=paste(output_dir,'Degree_analysis/Pluri_hESC_to_meso_pie.EPS', sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    pie(table(pluripotent_pie, exclude=0), labels=c("None", "Unique", "Common"), col=c("lightgray", "darkgray", "black"), main=c("hESC gene regulatory network"), cex=2)
dev.off()


classifications <- read.csv(file=paste(output_dir,'Degree_analysis/pluripotent_hESC_to_meso_classifications.txt', sep=""), header=TRUE, sep='\t')
postscript(file=paste(output_dir,'Degree_analysis/Pluri_hESC_to_meso_pie_classification_based.EPS', sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
pie(table(classifications$CLASSIFICATION), labels=c("Never bound", "Same TFs", "Loss TF", "Gain TF", "Swap TF"), col=c("darkgray", "black", "mediumpurple1", "lightslateblue", "darkslateblue"), cex=2)
dev.off()

unbound <- c("Late","Late","Late","Late","Late","Early","Early","Early","Early","Early","Early","Late","Late", "Late","Late","Stable","Stable","Stable")
loss <- c("Late","Late","Late","Late","Late","Late","Late","Late","Late","Late","Early","Early","Early","Early","Early","Early","Early","Stable","Stable","Stable","Stable","Stable","Stable","Stable","Stable")
gain <- c("Late","Late","Late","Late","Early","Stable","Stable","Stable")
swap <- c("Late","Late","Late","Late","Late","Late","Late","Late","Late","Early","Early","Early","Stable","Stable","Stable","Stable","Stable","Stable","Stable","Stable")

total <- c(unbound, loss, gain, swap)


#-----CHI SQUARE GOODNESS OF FIT TEST
#   Test of independence
#   Testing whether enhancers with a particular pattern of expression show expected proportion of membership to CoV clusters, or whether there are significant differences between them
#   In order of late, early, stable
expected_ratio_of_cov <- c(45, 24, 31)
unbound_percentage <- c(50, 33, 17)
loss_percentage <- c(40, 28, 32)
gain_percentage <- c(50, 12.5, 37.5)
swap_percentage <- c(45, 15, 40)
barplot_with_background <- cbind(expected_ratio_of_cov, unbound_percentage, loss_percentage, gain_percentage, swap_percentage)
colour_by_cov <- c('steelblue3', 'salmon3', 'gray47')

#   Stacked bar plot with CoV background
colnames(barplot_with_background) <- c('Background', '1', '2', '3')
postscript(file=paste(output_dir,"Degree_analysis/COV_Stacked_bar_plot_with_background.EPS", sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(barplot_with_background, col=rev(colour_by_cov), ylim=c(0,100), ylab="% belonging to CoV Cluster", xlab="TF binding changes")
dev.off()

goodness.of.fit <- chisq.test(x=unbound_percentage, p=expected_ratio_of_cov/100, correct=FALSE, rescale.p=FALSE, simulate.p.value=FALSE)
#   UNBOUND:  X-squared = 10.2531, df = 2, p-value = 0.005937
goodness.of.fit <- chisq.test(x=loss_percentage, p=expected_ratio_of_cov/100, correct=FALSE, rescale.p=FALSE, simulate.p.value=FALSE)
#   LOSS:  X-squared = 1.2545, df = 2, p-value = 0.5341
goodness.of.fit <- chisq.test(x=gain_percentage, p=expected_ratio_of_cov/100, correct=FALSE, rescale.p=FALSE, simulate.p.value=FALSE)
#   GAIN:  X-squared = 7.4289, df = 2, p-value = 0.02437
goodness.of.fit <- chisq.test(x=swap_percentage, p=expected_ratio_of_cov/100, correct=FALSE, rescale.p=FALSE, simulate.p.value=FALSE)
#   SWAP:  X-squared = 5.9879, df = 2, p-value = 0.05009

#   BOOTSTRAPPING CHI-SQUARE RESULT
#   Sample the background (which is ALL enhancers) at N=20 genes per sample
#   Do this 50x and plot the density of goodness.of.fit statistic

chi_sq_results <- vector()
for ( i in 1:50 ){

    random_sample <- sample(x=total, size=20, replace=FALSE)
    
    late <- (length(which(random_sample == "Late")) / length(random_sample)) *100
    early <- (length(which(random_sample == "Early")) / length(random_sample)) *100
    stable <- (length(which(random_sample == "Stable")) / length(random_sample)) *100
    
    observed_random_sample <- as.numeric(c(late, early, stable))

    goodness.of.fit <- chisq.test(x=observed_random_sample, p=expected_ratio_of_cov/100, correct=FALSE, rescale.p=FALSE, simulate.p.value=FALSE)

    chi_sq_results <- c(chi_sq_results, goodness.of.fit$p.value)

}

postscript(file=paste(output_dir,"Degree_analysis/CoV_stacked_barplot_Chi_square_50_random_samples.EPS", sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
plot(density(chi_sq_results), main=c("Chi square p value 50x"), xlab=c("p value"))
abline(v=0.05)
dev.off()

print(quantile(chi_sq_results))

############## CREATE DUAL NETWORKS TO VISUALISE EDGE DIFFERENCES BETWEEN 2 CONDITIONS ##################################
#   Note that the create_sif_from_adjacency function will only extract the edges that are present
#   With ChIP data, if a target is not bound by at least one of the TFs of interest it will be excluded
#   These nodes can be manually added in via Cytoscape if they are of further interest

#   PROMOTERS
# ------ hESC to endoderm
pluripotent <- 2*(hESC_adjacency_promoter) + endo_adjacency_promoter
network <- create_special_sif_from_adjacency(pluripotent)
write.table(network, file=paste(output_dir, file_name_1_A, "endo", file_name_2, promoter_upper_region_of_interest, "_to_-", promoter_lower_region_of_interest, file_name_3, sep=""), row.names=FALSE, col.names=FALSE, quote = FALSE, sep='\t')

differentiated <- 2*(endo_adjacency_promoter) + hESC_adjacency_promoter
network <- create_special_sif_from_adjacency(differentiated)
write.table(network, file=paste(output_dir, file_name_1_B, "endo", file_name_2, promoter_upper_region_of_interest, "_to_-", promoter_lower_region_of_interest, file_name_3, sep=""), row.names=FALSE, col.names=FALSE, quote = FALSE, sep='\t')

# ------ hESC to ectoderm
pluripotent <- 2*(hESC_adjacency_promoter) + ecto_adjacency_promoter
network <- create_special_sif_from_adjacency(pluripotent)
write.table(network, file=paste(output_dir, file_name_1_A, "ecto", file_name_2, promoter_upper_region_of_interest, "_to_-", promoter_lower_region_of_interest, file_name_3, sep=""), row.names=FALSE, col.names=FALSE, quote = FALSE, sep='\t')

differentiated <- 2*(ecto_adjacency_promoter) + hESC_adjacency_promoter
network <- create_special_sif_from_adjacency(differentiated)
write.table(network, file=paste(output_dir, file_name_1_B, "ecto", file_name_2, promoter_upper_region_of_interest, "_to_-", promoter_lower_region_of_interest, file_name_3, sep=""), row.names=FALSE, col.names=FALSE, quote = FALSE, sep='\t')

# ------ hESC to mesoderm
pluripotent <- 2*(hESC_adjacency_promoter) + meso_adjacency_promoter
network <- create_special_sif_from_adjacency(pluripotent)
write.table(network, file=paste(output_dir, file_name_1_A, "meso", file_name_2, promoter_upper_region_of_interest, "_to_-", promoter_lower_region_of_interest, file_name_3, sep=""), row.names=FALSE, col.names=FALSE, quote = FALSE, sep='\t')

differentiated <- 2*(meso_adjacency_promoter) + hESC_adjacency_promoter
network <- create_special_sif_from_adjacency(differentiated)
write.table(network, file=paste(output_dir, file_name_1_B, "meso", file_name_2, promoter_upper_region_of_interest, "_to_-", promoter_lower_region_of_interest, file_name_3, sep=""), row.names=FALSE, col.names=FALSE, quote = FALSE, sep='\t')

############## CREATE UNIQUE ENHANCER LEVEL GENE REGULATORY NETWORKS IN EACH CELL PHENOTYPE FOR THE GIVEN GENE SET  ##################################
#   Instantiate a new matrix from microarray data and cluster assignment
#   PROXIMAL PROMOTERS

#   ENHANCERS
#   Import the coordinates (BED-6 files processed using CAGE_analysis.r script)
all_enhancer_clusters <- read.csv(paste(output_dir,Enhancer_cluster_filename,sep=""), header=TRUE, sep='\t')
enhancer_ID <- as.character(all_enhancer_clusters$ID)
expressed_enhancers_by_cluster <- all_enhancer_clusters

for ( cell in cell_types ){
    #   Create a phenotype level matrix with the network nodes as rownames and TFs as column names
    assign("binding_data_enhancer", matrix(0, ncol=length(TFS), nrow=length(enhancer_ID), dimnames=list(enhancer_ID, TFS)))

    for ( gene in TFS ){
        
        temp_table_of_TF_peaks_at_enhancers <- matrix(0, ncol=7, nrow=1)
        colnames(temp_table_of_TF_peaks_at_enhancers) <- c("TF_peak_ID", "Chr", "Start", "End", "Distance.to.TSS", "Gene.Name", "Enhancer_ID")
        
        #   Grab the data and identify the peaks per TF
        file_list <- list.files(paste(base_dir,'data/2015/Meissner_CHIP_seq_processed',sep=""), pattern=paste(gene,"_",cell,sep=""), full.names=T)
        if ( (length(file_list) >= 1) == TRUE ){
            temp_data <- do.call("rbind", lapply(file_list, read.csv, header = FALSE, sep='\t'))
            colnames(temp_data) <- CHIP_colnames
            temp_data <- data.frame(lapply(temp_data, as.character), stringsAsFactors=FALSE)
            temp_data <- temp_data[,c("Chr", "Start", "End", "Distance.to.TSS", "Gene.Name")]
            ID <- paste(temp_data$Start, temp_data$End, sep='-')
            temp_data$TF_peak_ID <- paste(temp_data$Chr, ID, sep=':')
            temp_data <- temp_data[,c("TF_peak_ID", "Chr", "Start", "End", "Distance.to.TSS", "Gene.Name")]
            
            #   For ever expressed enhancer, find associated peaks in the TF-binding ChIP data
            for ( enhancer in 1:nrow(expressed_enhancers_by_cluster)){
                
                #   Define the enhancer coordinates
                enhancer_chromosome <- as.character(expressed_enhancers_by_cluster[enhancer,"Chr"])
                enhancer_start <- expressed_enhancers_by_cluster[enhancer,"Start"]
                enhancer_end <- expressed_enhancers_by_cluster[enhancer,"End"]
                
                #   Subset the TF-binding peak data based on the coordinates and extended boundary
                TF_peaks <- temp_data[ which(temp_data$Chr == enhancer_chromosome & temp_data$Start <= enhancer_start + enhancer_upper_region_of_interest & temp_data$End >= enhancer_end + enhancer_lower_region_of_interest),]
                #   Add the enhancer_ID to ensure the peak is associated with the enhancer
                TF_peaks$Enhancer_ID <- rep(as.character(expressed_enhancers_by_cluster[enhancer,"ID"]), nrow(TF_peaks))
                #   Ensure that rows are added only when there is a peak
                if (nrow(TF_peaks) != 0) {
                    temp_table_of_TF_peaks_at_enhancers <- rbind(temp_table_of_TF_peaks_at_enhancers, TF_peaks)
                }
            }
            
            #   Generate a table which contains the number of peaks per enhancer
            #   BUT THE ID NEEDS TO BE FROM THE ENHANCER TABLE NOT THE CHIP TABLE OTHERWISE THEY WON'T MATCH
            TF_peaks_at_enhancers <- as.matrix(table(temp_table_of_TF_peaks_at_enhancers[  , "Enhancer_ID"]))
            
            #   Coerce from matrix to data frame
            binding_data_enhancer <- as.data.frame(binding_data_enhancer)

            #   Use the peak data to fill the cytoscape_data table with the number of peaks
            for ( i in rownames(TF_peaks_at_enhancers) ){
                #   THE FIRST ROW IS ZERO?????
                if ( i !=0 ){
                    binding_data_enhancer[i,gene] <- TF_peaks_at_enhancers[i,]
                }
            }
        }
    }

new_col_names <- paste(TFS, cell, sep="_")
colnames(binding_data_enhancer) <- new_col_names
assign(paste(cell,"_result",sep=""), binding_data_enhancer)
write.table(binding_data_enhancer, file=paste(output_dir, Enhancer_TF_binding_output_filename, cell, ".txt",sep=""), col.names=TRUE, quote = FALSE, sep='\t')

}

expressed_enhancers_by_cluster <- cbind(expressed_enhancers_by_cluster, hESC_result, ecto_result, endo_result, meso_result)
write.table(expressed_enhancers_by_cluster, file=paste(output_dir, "Enhancer_analysis/enhancer_expression_and_TF_binding.txt", sep=""), row.names=TRUE, col.names=TRUE, quote=FALSE, sep='\t')



