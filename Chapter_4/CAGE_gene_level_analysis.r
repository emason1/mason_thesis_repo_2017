#! /usr/bin/Rscript

#   This script was written to construct a correlation (coexpression) network from gene level CAGE data
#   Data is the 2 control donors of the iPSC FANTOM 5 neural differentiation series day 00 and day 06
#   This data has been RLE normalized - this approach turns the raw tag count to TPMs based on the library size (edgeR)
#   The data is in tags per million (TPM) format for every gene, and requires filtering for undetected genes

########## Author:  Lizzi Mason  

########## Last edited:  Thursday 11-Feb-2016

########## SOURCE FUNCTIONS ##########
#   Coexpression network analysis functions
source(file="/home/lizzi/workspace/wells_lab/2014_library/Network_Functions_for_R/Network_functions.r")

########## SCRIPT STARTS HERE   ##########

       #-----Load the library script containing the network functions
require(limma)
require(stats)

       #-----Assign base directories
base_dir <- '/home/lizzi/workspace/'
input_dir <- paste(base_dir,'data/2015/FANTOM5_CAGE/',sep="")
output_dir <- paste(base_dir,'wells_lab/analysis/',sep="")


       #-----Import expression data
expression_data <- read.table(file=paste(input_dir,"iPSC_neuronal_differentiation_00_to_06_gene_level_CAGE_TPM.csv",sep=""), header=TRUE, sep='\t')
rownames(expression_data) <- expression_data[,1]
expression_data <- expression_data[,-1]
        #----Subset expression data based on the network genes

#missing_genes <- expression_data[c("POU5FLC1", "CD24P4", "CKMT1A", "CKMT1B", "NCRNA00287"),]

network_genes <- read.csv(paste(base_dir,'data/2015/Networks/sox2_oct4_minimal_network.txt',sep=""), header=FALSE, sep='\t')
network_genes <- as.character(network_genes[-1,1])
expression_data <- expression_data[rownames(expression_data) %in% network_genes,]

        #----Subset again into 1 matrix per phenotype
pluripotent_data <- expression_data[,c("Day_00_rep_1", "Day_00_rep_2", "Day_00_rep_3", "Day_00_rep_4", "Day_00_rep_5", "Day_00_rep_6")]
diff_data <- expression_data[,c("Day_06_rep_1", "Day_06_rep_2", "Day_06_rep_3", "Day_06_rep_4", "Day_06_rep_5", "Day_06_rep_6")]

        #----Compute 2 Pearson correlation matrixes
pluri_cor <- cor(t(pluripotent_data), method = 'pearson', use='pairwise.complete.obs')
diff_cor <- cor(t(diff_data), method='pearson', use='pairwise.complete.obs')
        #----Where standard deviation is zero the correlation values will be NaN so convert to zero
diff_cor[is.na(diff_cor)] <- 0

#   Import the network
positive.correlation.cutoff <- 0.8
negative.correlation.cutoff <- -0.8
directory_to_save_pluripotent <- paste(output_dir,'2015_12_Regulatory_Network_Finalisation/output/Correlation_network_analysis/CAGE_gene_level_pluripotent_',sep="")
directory_to_save_committed <- paste(output_dir,'2015_12_Regulatory_Network_Finalisation/output/Correlation_network_analysis/CAGE_gene_level_committed_',sep="")

pluripotent_edges <- build_correlation_network(pluri_cor, positive.correlation.cutoff, negative.correlation.cutoff, directory_to_save_pluripotent, "iPSC_00")
differentiation_edges <- build_correlation_network(diff_cor, positive.correlation.cutoff, negative.correlation.cutoff, directory_to_save_committed, "iPSC_06")

heatmap_data <- cbind(as.numeric(pluripotent_edges[,"PEARSON"]), as.numeric(differentiation_edges[,"PEARSON"]))
rownames(heatmap_data) <- rownames(pluripotent_edges)
colnames(heatmap_data) <- c("Day_00", "Day_06")
heatmap_data <- as.matrix(heatmap_data)

        #----Hierarchical clustering and heatmap
#   Define the colour palette for the heatmap
hmcols<-colorRampPalette(c("midnightblue", "gold2"))(300)
#   The data needs to be transformed to a numeric matrix, a standard matrix does not work in this instance
dat <- heatmap_data
#   Import the hierarchical cluster from the P-fractions data to force the row dendrogram
load(file=paste(output_dir,'2015_12_Regulatory_Network_Finalisation/output/Cluster_analysis/Heatmap_cluster_groups.RData',sep=""))
force_rowv <- as.dendrogram(heatmap_cluster_groups)
#   Export the heatmap of edge changes
postscript(file=paste(output_dir,"2015_12_Regulatory_Network_Finalisation/output/Cluster_analysis/CAGE_gene_level_Heatmap_network_common_edges_clustering_edges.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width= 22, height = 24, paper="special", family="Times")
    heatmap(x=dat, Colv=NA, distfun=dist, hclustfun = hclust, scale=c("column"), na.rm=TRUE, keep.dendro=TRUE, 
    col=hmcols, margins=c(10, 10), labCol=NULL, labRow=NULL)
    legend("topright",legend=c(colnames(dat)), border=TRUE, bty="n", y.intersp=2, cex=0.7)
dev.off()

#   Define hierarchical clusters of edges and cut the tree at 8 groups
d <- dist(dat, method='euclidean')
heatmap_cluster_groups <- hclust(d, method='average')
postscript(file=paste(output_dir,"2015_12_Regulatory_Network_Finalisation/output/Cluster_analysis/CAGE_gene_level_Hierarchical_cluster_network_common_edges_clustering_edges.EPS",sep=""), onefile=FALSE, horizontal=FALSE, width= 30, height = 15, paper="special", family="Times")
    plot(heatmap_cluster_groups, main=c("Coexpression Network Edge Clusters"), hang=-1)
    groups <- cutree(heatmap_cluster_groups, k=8)
    rect.hclust(heatmap_cluster_groups, k=8, border="red") 
dev.off()

#   Export cluster assignments for ChIP and histone analysis
write.table(groups, file=paste(output_dir,'2015_12_Regulatory_Network_Finalisation/output/Cluster_analysis/CAGE_gene_level_Heatmap_cluster_assignment.txt', sep=""),  quote = FALSE, sep="\t")


