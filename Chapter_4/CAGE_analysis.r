#! /usr/bin/Rscript

#   First Author : Lizzi Mason
#   Acknowledgments:  Albin Sandelin, Robin Anderssen, Nick Matigian, Suzy Butcher, Othmar Korn
#   Last edited 04-08-2016
#   This script is part of a Unix-executable-pipeline for CAGE data analysis
#   Primary goal is to look at enhancer promoter correlations and how they change between phenotypes
#-----
#   The files of interest for this analysis are:
#-----
#       All CAGE defined peaks in TPM (RLE normalised) with annotations:  iPSC_neuronal_differentiation_00_to_18_HG19_peaks_TPM.txt
#       All CAGE defined enhancer peaks in TPM (RLE normalised) with annotations:  iPSC_neuronal_differentiation_00_to_18_enhancers_TPM.txt
#       CAGE defined enhancer locations: human_permissive_enhancers_phase_1_and_2.bed
#       Samples of interest with FANTOM5 codes:  samples_CAGE.csv
#       Gene set of interest:  sox2_oct4_minimal_network.txt
#-----
#   Enhancer location readme:
#-----
#   The enhancer BED file is in BED12 format and the two blocks specify the two divergent regions of transcription initiation. 
#   Use the midpoint of the enhancers given by columns 7 and 8 as focus points when overlapping with TF binding data. 
#   Most often, TF binding as well as DNase hypersensitivity is centered in the middle and covered by the midpoint +/- 200 bp or so.
#   Note that the score does not correspond to the total number of reads in the samples, so it should not be used as an expression value
#   CAGE_ID refers to the enhancer ID (coordinate) which will map to the first column in the expression matrices
#   Import the enhancer locations in BED12 format
#-----
#   Enhancer promoter correlation readme:
#-----
#   Predicted based on Pearson correlations between all pairs of enhancers and promoters within 500kb
#   Associations have FDR <1e-5 (permissive, further thresholding on distance and R can be done
#   Negative distance indicates the enhancer is upstream of the promoter
#   Only robust DPI tag clusters near a 5'end of an annotated transcript were considered
#-----
#   Data can be found online at http://fantom.gsc.riken.jp/5/datafiles/latest/extra/CAGE_peaks/
#-----

##############  DEFINE NEW FUNCTIONS    ############## 
#   Functions specific to CAGE data analysis can be found in the CAGE_functions.r file
#   Functions specific to constructing GRNs can be found in Functions_for_regulatory_network_analysis.r file

##############  SPECIFY VARIABLES   ##############
#   Define input, output and library file paths
base_dir <- '/home/lizzi/workspace/'
input_dir <- paste(base_dir,'data/2015/FANTOM5_CAGE/',sep="")
output_dir <- paste(base_dir,'wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/output/Enhancer_analysis/',sep="")
cluster_files_dir <- paste(base_dir,'wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/output/Cluster_analysis/Gene_set_for_cov_cluster_',sep="")
filenames <- c("samples_CAGE.csv", "human.associations.hdr.txt", "human_permissive_enhancers_phase_1_and_2_expression_tpm_matrix.txt")
#   1.  Import data
enhancer_location_input_file <- c("human_permissive_enhancers_phase_1_and_2.bed.txt")
enhancer_location_colnames <- c("Chromosome", "Start_CAGE", "End_CAGE", "CAGE_ID", "Score", "Strand", "Start_TSS", "End_TSS", "RGB_string", "blockCount", "blockSizes", "BlockStarts")
enhancer_location_output_file <- c('HG19_all_CAGE_defined_enhancer_locations_BED12.txt')
enhancer_expression_output_file <- c('iPSC_neuronal_differentiation_00_to_18_all_enhancers_TPM.txt')
#   2.  Network analysis
cols.to.add <- c("Day_00_C11_rep_1", "Day_00_C11_rep_2", "Day_00_C11_rep_3","Day_00_C32_rep_1", "Day_00_C32_rep_2", "Day_00_C32_rep_3", "Day_06_C11_rep_1", "Day_06_C11_rep_2", "Day_06_C11_rep_3", "Day_06_C32_rep_1","Day_06_C32_rep_2", "Day_06_C32_rep_3")
#   3.  CoV defined clusters
colour_by_cov <- c('steelblue3', 'salmon3', 'gray47')
network_genes_enhancer_promoter_correlation_output <- c('combined_enhancer_expression_and_promoter_correlations_network_genes.txt')
plot_enhancers_variable_1 <- c('expression_of_enhancers_correlated_with_genes_in_cov_cluster_')
plot_enhancers_variable_2 <- c("Enhancer expression CoV cluster ")
plot_enhancers_variable_3 <- c('enhancer_expression_cov_cluster_')
enhancer_expression_by_cov_output <- c('enhancer_coordinates_for_expressed_enhancers_correlated_with_genes_in_cov_cluster_')
columns_for_homer_analysis <- c("expressed_enhancers", "V1", "V2", "V3", "strand")
homer_analysis_output_filename <- c('BED_6_enhancer_coordinates_for_expressed_enhancers_correlated_with_genes_in_cov_cluster_')
#   4.  Kmeans enhancer analysis
enhancer_kmeans_phenotype <- c("Day_00", "Day_06")
enhancer_kmeans_variable_3 <- c("Clustering of Enhancer Expression")
enhancer_kmeans_variable_3_median <- c("Clustering of Median Enhancer Expression")
enhancer_kmeans_variable_4 <- c("enhancer_k_means_4.EPS")
enhancer_kmeans_variable_4_median <- c("enhancer_k_means_4_median.EPS")
#   5.  Proportionality of cluster analysis
rnames <- c("CoV_Cluster_1_4", "CoV_Cluster_2_3", "CoV_Cluster_5")
cnames <- c("1", "2", "3")

##############   LOAD LIBRARIES  ##############
require(stats)
require(gplots)
require(Biobase)

##############   SOURCE REQUIRED FUNCTIONS  ##############
source(file="/home/lizzi/workspace/wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/scripts/Functions_for_regulatory_network_analysis.r")
source(file="/home/lizzi/workspace/wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/scripts/CAGE_functions.r")

############## IMPORT NETWORKS AND COVAR CLUSTER ASSIGNMENTS ##################################
network_genes <- read.csv(paste(base_dir,'data/2015/Networks/sox2_oct4_minimal_network.txt',sep=""), header=FALSE, sep='\t')
network_genes <- as.character(network_genes[,1])
#   Remove the genes that are not mapped between microarray and CAGE
network_genes <- network_genes[network_genes != c("CD24P4", "MIR600HG", "POU5F1P4", "CKMT1A")]

#   Import the clusters and remove unmapped genes
cov_cluster_1 <- read.csv(paste(cluster_files_dir,'1_4.csv',sep=""), header=FALSE, sep='\t')
cov_cluster_1 <- as.character(cov_cluster_1[,1])
cov_cluster_1 <- cov_cluster_1[cov_cluster_1 != "MIR600HG"]

cov_cluster_2 <- read.csv(paste(cluster_files_dir,'2_3.csv',sep=""), header=FALSE, sep='\t')
cov_cluster_2 <- as.character(cov_cluster_2[,1])
cov_cluster_2 <- cov_cluster_2[cov_cluster_2 != c("CKMT1A", "CD24P4", "POU5F1P4")]

cov_cluster_5 <- read.csv(paste(cluster_files_dir,'5.csv',sep=""), header=FALSE, sep='\t')
cov_cluster_5 <- as.character(cov_cluster_5[,1])

all_cov_clusters <- list(cov_cluster_1, cov_cluster_2, cov_cluster_5)

############## IMPORT DATA ##################################
#-----Import and process the data
#   Sample and phenotype information, and network of interest
for ( i in filenames ){
    assign(i, read.table(file=paste(input_dir,i,sep=""), header=TRUE, sep='\t'))
}
#-----Import the enhancer location files
enhancers_location <-  read.delim(paste(input_dir,enhancer_location_input_file,sep=""), sep="\t", header=FALSE)
colnames(enhancers_location) <-  enhancer_location_colnames
write.table(enhancers_location, file=paste(output_dir,enhancer_location_output_file, sep=""),  quote = FALSE, sep="\t")
#-----Rename enhancer promoter associations
enhancer_promoter_correlation <- human.associations.hdr.txt
#-----Rename enhancer expression file
enhancers_expressed <- human_permissive_enhancers_phase_1_and_2_expression_tpm_matrix.txt
#   Make the CAGE_ID the rownames
rownames(enhancers_expressed) <- enhancers_expressed$Id
enhancers_expressed <- enhancers_expressed[,-1]

#   Reorder the columns and relabel
sample_info_all <- samples_CAGE.csv
reorder_by_sample <- as.character(sample_info_all$Donor)
enhancers_expressed <- enhancers_expressed[,reorder_by_sample]
colnames(enhancers_expressed) <- as.character(sample_info_all$Sample_ID)
#   Export the data
write.table(enhancers_expressed, file=paste(output_dir,enhancer_expression_output_file, sep=""),  quote = FALSE, sep="\t")

############## NETWORK ANALYSIS ##################################
#-----WHOLE NETWORK (this will go into supplementary information table)
#-----Subset the enhancer expression data based on enhancer-promoter correlations of all network genes
#-----For all genes
promoters <- as.character(enhancer_promoter_correlation[,"promoter"])
network_promoters <- vector()
for (gene in network_genes){
    for (promoter in promoters){
        if (grepl(gene, promoter, fixed=TRUE) == TRUE) {
            network_promoters <- c(network_promoters, promoter)
        }
    }
}
#   Use this to subset the matrix of correlations
network_enhancer_promoter_correlated <- subset(enhancer_promoter_correlation, promoter %in% network_promoters )
#   Identify the enhancer IDs (chromosomal location)
network_enhancers <- network_enhancer_promoter_correlated$enhancer
#   Use this to subset the expression data
network_enhancers_expression <- subset(enhancers_expressed, rownames(enhancers_expressed) %in% network_enhancers)
#   Craft a table with both enhancer expression and promoter-enhancer correlations in the samples of interest for the supplementary information
for ( enhancer in network_enhancer_promoter_correlated$enhancer ){
    for ( samples in cols.to.add ){
        network_enhancer_promoter_correlated[samples] <- network_enhancers_expression[enhancer,samples]
    }
}
write.table(network_enhancer_promoter_correlated, file=paste(output_dir,network_genes_enhancer_promoter_correlation_output, sep=""),  quote = FALSE, sep="\t")

#-----COV DEFINED CLUSTERS (this forms the main part of the analysis)
#-----Subset the enhancer expression data based CoV clusters
#-----For all enhancers whose expression is strongly correlated with genes in our network cov clusters
#   HOMER will accept a BED6 file of enhancer coordinates in the following format
#    Column1: Unique Peak ID
#    Column2: chromosome
#    Column3: starting position
#    Column4: ending position
#    Column5: Strand (+/- or 0/1, where 0="+", 1="-")

#   1 Locations of the enhancers which are strongly correlated with promoters of genes in cluster X
#   2 Identify those enhancers expressed in my dataset
#   3 Plot the expression of these enhancers

#   Instantiate counter
counter <- 1

for (cluster in all_cov_clusters) {
    promoters <- as.character(enhancer_promoter_correlation[,"promoter"])
    network_promoters <- vector()

    for (gene in cluster){
        for (promoter in promoters){
            if (grepl(gene, promoter, fixed=TRUE) == TRUE) {
                network_promoters <- c(network_promoters, promoter)
            }
        }
    }

    #   Use this to subset the matrix of correlations
    network_enhancer_promoter_correlated <- subset(enhancer_promoter_correlation, promoter %in% network_promoters )
    #   Identify the enhancer IDs (chromosomal location)
    network_enhancers <- network_enhancer_promoter_correlated$enhancer
    #   Subset the expression matrix based on the enhancer IDS
    enhancers_in_cov_cluster <- network_enhancers_expression[rownames(network_enhancers_expression) %in% network_enhancers,cols.to.add]
    #   Plot the expression of these enhancers
    plot_enhancers(enhancers_in_cov_cluster, paste(plot_enhancers_variable_1,counter,sep=""), paste(plot_enhancers_variable_2,counter,sep=""), paste(plot_enhancers_variable_3,counter,sep=""), colour_by_cov[counter])
    #   Export all data based on CoV clusters
    #write.table(enhancers_in_cov_cluster, file=paste(output_dir,enhancer_expression_by_cov_output,counter,'.txt', sep=""),  quote = FALSE, sep="\t")

    #   Create BED6 file from the expressed enhancer coordinates and export (for use in HOMER/PANTHER analysis)
    expressed_enhancers <- rownames(enhancers_in_cov_cluster)
    temp_data <- as.data.frame(do.call('rbind', strsplit(expressed_enhancers, ':|-')))
    temp_data <- cbind(temp_data, expressed_enhancers)
    temp_data$strand <- rep('+', nrow(temp_data))
    temp_data <- temp_data[,columns_for_homer_analysis]
    write.table(temp_data, file=paste(output_dir,homer_analysis_output_filename,counter,'.bed', sep=""),  row.names=FALSE, col.names=FALSE, quote = FALSE, sep="\t")
    
    counter <- counter + 1
}

#----------   Work in progress: Enhancers of bivalent genes -----------
bivalent_genes <- c("TMEM125", "OTX2", "DNMT3B", "PDZD4")
promoters <- as.character(enhancer_promoter_correlation[,"promoter"])
bivalent_network_promoters <- vector()
for (gene in bivalent_genes){
    for (promoter in promoters){
        if (grepl(gene, promoter, fixed=TRUE) == TRUE) {
            bivalent_network_promoters <- c(bivalent_network_promoters, promoter)
        }
    }
}
 #   Use this to subset the matrix of correlations
bivalent_network_enhancer_promoter_correlated <- subset(enhancer_promoter_correlation, promoter %in% bivalent_network_promoters )
#   Identify the enhancer IDs (chromosomal location)
bivalent_network_enhancers <- bivalent_network_enhancer_promoter_correlated$enhancer
#   Subset the expression matrix based on the enhancer IDS
bivalent_enhancers_in_cov_cluster <- network_enhancers_expression[rownames(network_enhancers_expression) %in% bivalent_network_enhancers,cols.to.add]
#   Plot the expression of these enhancers
plot_enhancers(bivalent_enhancers_in_cov_cluster, paste("bivalent",plot_enhancers_variable_1,counter,sep=""), paste("bivalent",plot_enhancers_variable_2,counter,sep=""), paste("bivalent",plot_enhancers_variable_3,counter,sep=""), "black")
#   Enhancer expression shows no obvious pattern

#---------- 

#-----KMEANS CLUSTER ANALYSIS OF ALL EXPRESSED ENHANCERS
#   Subset the expression matrix based on the enhancer IDS
enhancers_for_k_means <- network_enhancers_expression[, cols.to.add]
#   Use the mean expression of each phenotype rather than using each data point (this makes the plot more interpretable without changing the results - checked)
expression_data <- cbind(rowMeans(enhancers_for_k_means[,1:6]), rowMeans(enhancers_for_k_means[,7:12]))
colnames(expression_data) <- enhancer_kmeans_phenotype
#   Run K-means cluster analysis
k_means_of_enhancers <- K_means_enhancer_analysis(4, expression_data, enhancer_kmeans_variable_3, enhancer_kmeans_variable_4)
enhancer_cluster_assignment <- as.factor(k_means_of_enhancers$fit.cluster)

#   Use the median expression of each phenotype rather than using each data point (this makes the plot more interpretable without changing the results - checked)
median_enhancer_expression_data <- cbind(rowMedians(as.matrix(enhancers_for_k_means[,1:6])), rowMedians(as.matrix(enhancers_for_k_means[,7:12])))
colnames(median_enhancer_expression_data) <- enhancer_kmeans_phenotype
#   Run K-means cluster analysis
median_k_means_of_enhancers <- K_means_enhancer_analysis(4, median_enhancer_expression_data, enhancer_kmeans_variable_3_median, enhancer_kmeans_variable_4_median)

#-----PLOT PROPORTION OF ENHANCERS BELONGING TO COV CLUSTERS
#   Stacked bar plot used although venn diagram would also be suitable
#   Instantiate empty matrix with CoV clusters as rows and Enhancer clusters as columns
bar_plot_data <- matrix(nrow=3, ncol=3, dimnames=list(rnames, cnames))

k_means_of_enhancers$fit.cluster[k_means_of_enhancers$fit.cluster == 4] <- 2

write.table(k_means_of_enhancers, file=paste(output_dir,"Enhancer_gene_regulatory_networks.bed", sep=""),  row.names=TRUE, col.names=TRUE, quote = FALSE, sep="\t")

#   Fill the matrix using the proportions of expressed enhancers (per cluster) which belong to CoV clusters
for ( j in 1:3 ){
    #   Idenfity enhancers in the enhancer clusters
    enhancers_in_cluster <- rownames(k_means_of_enhancers[enhancer_cluster_assignment==j,])
    #   Use this to subset the matrix of correlations
    enhancer_promoter_table <- subset(enhancer_promoter_correlation, enhancer %in% enhancers_in_cluster )
    #   Remember that all enhancers multimap to >1 promoter so list will increase
    promoters <- as.character(enhancer_promoter_table[,"promoter"])
    promoter_genes <- vector()
    for (promoter in promoters){
    promoter_genes <- c(promoter_genes, gsub(".*@","", promoter))
    }
    
    #   Identify all the promoters associated with the enhancers in this cluster
    network_promoter_genes <- promoter_genes[promoter_genes %in% unlist(all_cov_clusters)]
    
    #   What proportion of these promoters are associated with each CoV cluster
    #   Each promoter MUST be associated with a CoV cluster
    for ( i in 1:3 ){
    bar_plot_data[i,j] <-  ( length(network_promoter_genes[network_promoter_genes %in% unlist(all_cov_clusters[[i]])]) / length(network_promoter_genes) ) * 100
    }
}
#   Reverse the order of columns to make plot more interpretable
bar_plot_data <- bar_plot_data[rev(rownames(bar_plot_data)),]
#   Plot the stacked bar graph
postscript(file=paste(output_dir,"Stacked_bar_plot.EPS", sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(bar_plot_data, col=rev(colour_by_cov), ylim=c(0,100), ylab="% belonging to CoV Cluster", xlab="Enhancer clusters")
dev.off()

#-----CHI SQUARE GOODNESS OF FIT TEST
#   Test of independence
#   Testing whether enhancers with a particular pattern of expression show expected proportion of membership to CoV clusters, or whether there are significant differences between them
total_clusters <- length(cov_cluster_1) + length(cov_cluster_2) + length(cov_cluster_5)
cluster_1_percentage <- length(cov_cluster_1)/total_clusters
cluster_2_percentage <- length(cov_cluster_2)/total_clusters
cluster_5_percentage <- length(cov_cluster_5)/total_clusters
expected_cov_ratio <- c(cluster_5_percentage, cluster_2_percentage, cluster_1_percentage)

#   Stacked bar plot with CoV background
barplot_with_background <- cbind(expected_cov_ratio*100, bar_plot_data)
colnames(barplot_with_background) <- c('Background', '1', '2', '3')
postscript(file=paste(output_dir,"Stacked_bar_plot_with_background.EPS", sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
    barplot(barplot_with_background, col=rev(colour_by_cov), ylim=c(0,100), ylab="% belonging to CoV Cluster", xlab="Enhancer clusters")
dev.off()


#	Expected ratio = full network, Observed ratio = Clusters 1 - 4
#   Percentage change in the proportion of enhancers associated with genes in CoV clusters are significantly different from the expected CoV proportions

#   Enhancer Cluster 1
observed_enhancer_cluster_1 <- bar_plot_data[,1]
goodness.of.fit <- chisq.test(x=observed_enhancer_cluster_1, p=expected_cov_ratio, correct=FALSE, rescale.p=FALSE, simulate.p.value=FALSE)
#   Cluster 1:  X-squared = 0.6309, df = 2, p-value = 0.7294

observed_enhancer_cluster_2 <- bar_plot_data[,2]
goodness.of.fit <- chisq.test(x=observed_enhancer_cluster_2, p=expected_cov_ratio, correct=FALSE, rescale.p=FALSE, simulate.p.value=FALSE)
#   Cluster 2:  X-squared = 14.551, df = 2, p-value = 0.0006923

observed_enhancer_cluster_3 <- bar_plot_data[,3]
goodness.of.fit <- chisq.test(x=observed_enhancer_cluster_3, p=expected_cov_ratio, correct=FALSE, rescale.p=FALSE, simulate.p.value=FALSE)
#   Cluster 3:  X-squared = 43.2362, df = 2, p-value = 4.087e-10



#   BOOTSTRAPPING CHI-SQUARE RESULT
#   Sample the background (which is ALL enhancers) at N=21 enhancers per sample

#   Do this 21x and plot the density of goodness.of.fit statistic
#   Need to automate this sampling

chi_sq_results <- vector()

for ( i in 1:50 ){

random_sample_of_enhancers <- sample(x=rownames(k_means_of_enhancers), size=21, replace=FALSE)
enhancer_promoter_table <- subset(enhancer_promoter_correlation, enhancer %in% random_sample_of_enhancers )
#   Remember that all enhancers multimap to >1 promoter so list will increase
promoters <- as.character(enhancer_promoter_table[,"promoter"])
promoter_genes <- vector()
for (promoter in promoters){
    promoter_genes <- c(promoter_genes, gsub(".*@","", promoter))
}
temp <- vector()
for ( i in 1:3 ){
temp <- c(temp, (length(promoter_genes[promoter_genes %in% unlist(all_cov_clusters[[i]])]) / length(promoter_genes) ) * 100)
}
observed_random_sample <- rev(temp)
goodness.of.fit <- chisq.test(x=observed_random_sample, p=expected_cov_ratio, correct=FALSE, rescale.p=FALSE, simulate.p.value=FALSE)

chi_sq_results <- c(chi_sq_results, goodness.of.fit$p.value)

}

postscript(file=paste(output_dir,"Chi_square_50_random_samples.EPS", sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
plot(density(chi_sq_results), main=c("Chi square p value 50x"), xlab=c("p value"))
dev.off()
