#   First author:  Elizabeth Mason

#   Last edited:  Monday 08-Aug-2016

#-----------------------------------

#   HOMER analysis for enhancers associated with all genes in the re-derived network

#   Gene sets per cluster are unique Gene IDs only (duplicates removed)

#   Usage:  findMotifsGenome.pl <peak/bed file> <genome> <output directory> -size [options]

#   For all [options] specific to this analysis: http://homer.salk.edu/homer/ngs/peakMotifs.html

cd /home/lizzi/workspace/wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/output/Motif_analysis/

#   Make HOMER executable if not already (problems with this)

PATH=$PATH:/home/lizzi/homer/bin/

#-----------------------------------

#   Enhancers

#   BED files should have at minimum 6 columns (separated by TABs, additional columns will be ignored)

#    Column1: chromosome
#    Column2: starting position
#    Column3: ending position
#    Column4: Unique Peak ID
#    Column5: not used
#    Column6: Strand (+/- or 0/1, where 0="+", 1="-")


file01="BED_6_enhancer_coordinates_for_expressed_enhancers_correlated_with_genes_in_cov_cluster_1.bed"
file02="BED_6_enhancer_coordinates_for_expressed_enhancers_correlated_with_genes_in_cov_cluster_2.bed"
file03="BED_6_enhancer_coordinates_for_expressed_enhancers_correlated_with_genes_in_cov_cluster_3.bed"
file04="BED_6_enhancer_coordinates_for_expressed_enhancers_correlated_with_genes_in_cov_cluster_4.bed"
file05="BED_6_enhancer_coordinates_for_expressed_enhancers_correlated_with_genes_in_cov_cluster_5.bed"

cat $file01 $file02 $file03 $file04 $file05 > "BED_6_enhancer_coordinates_merged.bed"


findMotifsGenome.pl HOMER_input_files/BED_6_enhancer_coordinates_merged.bed hg19 HOMER_output_files/ -size 200 -mask


findMotifsGenome.pl HOMER_input_files/BED_6_expressed_enhancers.bed hg19 HOMER_output_files/ -size 200 -mask
