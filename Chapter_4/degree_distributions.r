#! /usr/bin/Rscript

#   First author : Lizzi Mason
#   Last edited 03-AUGUST-2016

##############   LOAD LIBRARIES  ##############
require(stats)

##############   SOURCE REQUIRED FUNCTIONS  ##############
quantfun <- function(x) {as.integer(cut(x, quantile(x, include.lowest=TRUE)))}

##############   SPECIFY FILEPATHS  ##############
base_dir <- '/home/lizzi/workspace/wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/'
input_dir <- paste(base_dir,'input/',sep="")
output_dir <- paste(base_dir,'output/Degree_analysis/',sep="")

##############   SPECIFY VARIABLES  ##############
degree_distribution_output_filename <- paste(output_dir,'Degree_frequency_distributions.EPS',sep="")

############## IMPORT FILES ##################################
degree_distribution_table <- read.csv(paste(input_dir,'Degree_distributions.txt',sep=""), header=TRUE, sep='\t')
rownames(degree_distribution_table) <- as.character(degree_distribution_table[,1])
degree_distribution_table <- as.matrix(degree_distribution_table[,-1])

############## DENSITY PLOTS ##################################
#-----  Plot density plots to display the range of the data per phenotype excluding all NA values
plot_titles <- as.character(colnames(degree_distribution_table))
postscript(file=degree_distribution_output_filename, onefile=FALSE, horizontal=FALSE, width=12, height=12, paper="special", family="Times")
par(mfrow=c(2,2), cex=1.3)
    for ( i in 1:ncol(degree_distribution_table) ){
    plot(density(degree_distribution_table[,i]), main=plot_titles[i], ylab=c("Density"), xlab=c("Degree"))
    }
dev.off()

############## RANK ANALYSIS ##################################
#   Rank of each gene per network
table_of_ranks <- apply(degree_distribution_table, 2, function(x){rank(x, ties.method=c("max"))})
#   Specify the quantiles for each gene in each network
genes_quantiles <- apply(table_of_ranks, 2, quantfun)
rownames(genes_quantiles) <- rownames(table_of_ranks)
colnames(genes_quantiles) <- colnames(table_of_ranks)
#   Make NA values zero
genes_quantiles[is.na(genes_quantiles)] <- 0 

write.table(genes_quantiles, file=paste(output_dir,"table_of_quantiles.csv",sep=""), row.names=TRUE, col.names=TRUE, quote = FALSE, sep='\t')
write.table(table_of_ranks, file=paste(output_dir,"table_of_ranks.csv",sep=""), row.names=TRUE, col.names=TRUE, quote = FALSE, sep='\t')
