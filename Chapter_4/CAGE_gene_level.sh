
#   Subset based on column names where the list of CNhs IDs are present using grep and also take the 00Annotation and short_description

ids_to_keep="gene|CNhs14045|CNhs13822|CNhs14049|CNhs14046|CNhs13823|CNhs14050|CNhs14047|CNhs13824|CNhs14051|CNhs13916|CNhs13825|CNhs13917|CNhs13826|CNhs13839|CNhs14052|CNhs13827|CNhs13840|CNhs14053|CNhs13828|CNhs13841|CNhs14054|CNhs13829|CNhs13842|CNhs14055"

cutrange=`head -1 tc_expr.human.gene.csv | sed -r -e 's/\t/\n/'g | grep -n -P $ids_to_keep | sed -r -e 's/^([0-9]+).*$/\1/g' | tr [:cntrl:] ',' | sed -r -e 's/\,$//g'`
cut -f $cutrange tc_expr.human.gene.csv > iPSC_neuronal_differentiation_00_to_18_gene_level_CAGE_TPM.txt


ids_to_keep="gene|CNhs14045|CNhs13822|CNhs14049|CNhs14046|CNhs13823|CNhs14050|CNhs13826|CNhs13839|CNhs14052|CNhs13827|CNhs13840|CNhs14053"

cutrange=`head -1 tc_expr.human.gene.csv | sed -r -e 's/\t/\n/'g | grep -n -P $ids_to_keep | sed -r -e 's/^([0-9]+).*$/\1/g' | tr [:cntrl:] ',' | sed -r -e 's/\,$//g'`
cut -f $cutrange tc_expr.human.gene.csv > iPSC_neuronal_differentiation_00_to_06_gene_level_CAGE_TPM.txt
