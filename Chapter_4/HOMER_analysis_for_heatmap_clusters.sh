#   First author:  Elizabeth Mason

#   Last edited:  Tuesday 09-Feb-2016

#-----------------------------------

#   HOMER analysis for the clusters defined by P-fractions correlation heatmap and CoVar Kmeans cluster analysis

#   Gene sets per cluster are unique Gene IDs only (duplicates removed)

#   Usage:  findMotifs.pl <inputfile.txt> <promoter set> <output directory> [options]

#   For all [options] specific to this analysis: http://homer.salk.edu/homer/microarray/index.html

cd /home/lizzi/workspace/wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/output/Motif_analysis/

#   Make HOMER executable if not already (problems with this)

#-----------------------------------

#   Proximal Promoter Regions

#   Default settings -300 to +50

PATH=$PATH:/home/lizzi/homer/bin/

clusters="
1
2
3
4
5
6
7
8
"

cmd1="findMotifs.pl HOMER_input_files/Gene_set_for_heatmap_cluster_"
cmd2=".txt human HOMER_output_files/HOMER_Proximal_Promoter_Heatmap_Cluster_"

for cluster in $clusters
    do $cmd1$cluster$cmd2$cluster"/"
done

#   For COV clusters

clusters="
1
2
3
4
5
"

cmd1="findMotifs.pl HOMER_input_files/Gene_set_for_cov_cluster_"
cmd2=".csv human HOMER_Proximal_Promoter_CoV_Cluster_"

for cluster in $clusters
    do $cmd1$cluster$cmd2$cluster"/"
done


#-----------------------------------

#   ENHANCER REGIONS

#   Add the option <Start> -200 <End> 200

#   Make HOMER executable if not already (problems with this)

PATH=$PATH:/home/lizzi/homer/bin/

#   Usage:  findMotifsGenome.pl <peak/BED file> <genome> <output directory> [options]

#   For all [options] specific to this analysis: http://homer.salk.edu/homer/ngs/peakMotifs.html

cd /home/lizzi/workspace/wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/output/

cmd1="findMotifsGenome.pl Enhancer_analysis/BED_6_enhancer_coordinates_for_expressed_enhancers_correlated_with_genes_in_cov_cluster_"
cmd2=".bed hg19 Motif_analysis/HOMER_Enhancer_CoV_Clusters/Cluster_"
cmd3="/ -size 200"

#   For enhancer clusters

clusters="
1
2
3
4
"

for cluster in $clusters
    do $cmd1$cluster$cmd2$cluster$cmd3
done
