#   First author:  Elizabeth Mason

#   Last edited:  Thursday 26-May-2016

#-----------------------------------

#   HOMER analysis for the clusters defined by P-fractions correlation heatmap and CoVar Kmeans cluster analysis

#   Gene sets per cluster are unique Gene IDs only (duplicates removed)

#   Usage:  findMotifs.pl <inputfile.txt> <promoter set> <output directory> [options]

#   For all [options] specific to this analysis: http://homer.salk.edu/homer/microarray/index.html

cd /home/lizzi/workspace/wells_lab/analysis/2015_12_Regulatory_Network_Finalisation/output/Motif_analysis/HOMER_full_networks

#   Make HOMER executable if not already (problems with this)

#-----------------------------------

#   Proximal Promoter Regions

#   Default settings -300 to +50

PATH=$PATH:/home/lizzi/homer/bin/

networks="
sox2_oct4_minimal_network
correlation_network_node_list_ens63
"

cmd1="findMotifs.pl /home/lizzi/workspace/data/2015/Networks/"
cmd2=".txt human HOMER_Proximal_Promoter_"

for network in $networks
    do $cmd1$network$cmd2$network"/"
done

#   Adding a custom background file (this is all detected genes in the microarray data)

cmd1="findMotifs.pl /home/lizzi/workspace/data/2015/Networks/"
cmd2=".txt human HOMER_Proximal_Promoter_custom_background_"
cmd3=" -bg /home/lizzi/workspace/data/2015/Microarray/detected_genes_list.csv"
for network in $networks
    do $cmd1$network$cmd2$network"/"$cmd3
done
