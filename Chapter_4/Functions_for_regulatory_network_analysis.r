#! /usr/bin/Rscript

#   First author : Lizzi Mason
#   Last edited 30-MARCH-2016

##############   DEFINE ALL FUNCTIONS  ##############
matrixFromList <- function(listX) t(sapply(listX, function(x, n) c(x, rep(NA, n))[1:n], n = max(sapply(listX, length)))) 
co.var <- function(x)(100*sd(x)/mean(x))

#   Transform adjacency matrix into Cytoscape readable sif file format
create_sif_from_adjacency <- function(adjacency_matrix){
temp <- list()
cols <- colnames(adjacency_matrix)
rows <- rownames(adjacency_matrix)
for ( i in 1:nrow(adjacency_matrix) ) {
    for ( j in 1:ncol(adjacency_matrix) ) {
        if ( adjacency_matrix[i,j] != 0 ) {
        temp[[length(temp) + 1]]= c(rows[i], cols[j], adjacency_matrix[i,j])
        }
    }
}
network <- matrixFromList(temp)
return(network)
}

#   Transform adjacency matrix into Cytoscape readable sif file format but include all edges (do not discard edges = 0)
create_special_sif_from_adjacency <- function(adjacency_matrix){
temp <- list()
cols <- colnames(adjacency_matrix)
rows <- rownames(adjacency_matrix)
for ( i in 1:nrow(adjacency_matrix) ) {
    for ( j in 1:ncol(adjacency_matrix) ) {
        temp[[length(temp) + 1]]= c(rows[i], cols[j], adjacency_matrix[i,j])
    }
}
network <- matrixFromList(temp)
return(network)
}


#   Transform adjacency matrix into Cytoscape readable edge list
create_edge_list_from_adjacency <- function(adjacency_matrix){
temp <- list()
cols <- colnames(adjacency_matrix)
rows <- rownames(adjacency_matrix)
for ( i in 1:nrow(adjacency_matrix) ) {
    for ( j in 1:ncol(adjacency_matrix) ) {
        temp[[length(temp) + 1]]= c(rows[i], cols[j], adjacency_matrix[i,j])
    }
}
network <- matrixFromList(temp)
return(network)
}

#   Standard K-means cluster analysis
K_means_analysis <- function(seed, dataset, file_name, plot_file_name){
#---Define the number of clusters required
    set.seed(seed)
    fit <- kmeans(dataset, seed)
    cluster_size <- fit$size
#---Calculate means
    agg.means <- aggregate(dataset,by=list(fit$cluster),FUN=mean)
    my.data <- agg.means[,-seq_len(1)]
#---Append cluster assignment
    mydata <- data.frame(dataset, fit$cluster)
    write.table(mydata, file=paste(output_dir, file_name, sep=""), quote = FALSE, row.names=TRUE, col.names=TRUE, sep="\t")
#---Line graph of aggregate means
    x <- c(1:ncol(my.data))
    xrange <- range(x)
    yrange <- range(my.data)
    plot.colours <-  c('lightsteelblue', 'lightsalmon',  'salmon3', 'steelblue3', 'grey47')
    postscript(file=paste(output_dir,plot_file_name, sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
        plot(xrange, yrange, type="n", axes=FALSE, ylab="Aggregate Mean of CoV" )
        axis(1, at=x, lab = colnames(my.data))
        axis(2)
        box()
        for (i in 1:nrow(my.data)){
        lines(x, my.data[i,], type="b", lwd=2, col=plot.colours[i])
        }
        legend("topright", legend=c(cluster_size), lty=c(1), lwd=(2), col=plot.colours, text.col=plot.colours, title=c("Cluster Sizes"))
    dev.off()
#---Return the cluster assignment in a data frame
return(mydata)
}

#   Plot aggregates of K-means clusters
bar.plot.my.aggregates <- function(mydata){
barplot(t(as.matrix(my.data)), beside=TRUE, axes=FALSE, xlab = "Clusters", ylab = "Aggregate Mean", ylim=c(0,(max(my.data)+0.5)), col=c(p4.col, p5.col, p6.col, p7.col))
    axis(1, at = c(3, 8), lab = 1:nrow(my.data))
    axis(2)
    box()
}

#  Calculate transcription factor binding at enhancers
tf_binding_of_enhancers <- function(TF_binding, enhancer_start_list){
#   This will generate a list of enhancers which are expressed in a ChIP bound region for a single TF
#   Requires a ChIP dataset, and a list of enhancer start locations
tf_bound_enhancers <- vector()

for ( enhancer in enhancer_start_list ){
    for ( i in TF_binding ){
            chr_peak <- strsplit(i, "[:-]")[[1]][1]
            chr_enhancer <- strsplit(enhancer, "[:]")[[1]][1]
            if ( chr_peak == chr_enhancer ){
                start_peak <- as.numeric(strsplit(i, "[:-]")[[1]][2])
                end_peak <- as.numeric(strsplit(i, "[:-]")[[1]][3])
                start_enhancer <- as.numeric(strsplit(enhancer, "[:-]")[[1]][2])
                if ( start_enhancer >= start_peak & start_enhancer <= end_peak ){
                    tf_bound_enhancers <- c(tf_bound_enhancers, enhancer)
                }
            }
    }
}
return(tf_bound_enhancers)
}



#plot_enhancers <- function(which_network_enhancer_expression, plot_filename, plot_title, table_filename){
#   This function plots the mean enhancer expression (3 replicates combined) between Day00 and Day06

#   Threshold the enhancer expression data based on a total of 3TPM being detected per enhancer
#    which_network_enhancer_expression <- which_network_enhancer_expression[rowSums(which_network_enhancer_expression) > 3,]
#    write.table(which_network_enhancer_expression, file=paste(output_dir,table_filename,'_plotted_enhancers_expression.txt', sep=""),  quote = FALSE, sep="\t")

#   Set plot parameters
#    x.lab <- c("Phenotype")
#    y.lab <- c("Enhancer expression TPM")
#    day.00 <- c("Day_00_C11_rep_1", "Day_00_C11_rep_2", "Day_00_C11_rep_3","Day_00_C32_rep_1", "Day_00_C32_rep_2", "Day_00_C32_rep_3")
#    day.06 <- c("Day_06_C11_rep_1", "Day_06_C11_rep_2", "Day_06_C11_rep_3", "Day_06_C32_rep_1","Day_06_C32_rep_2", "Day_06_C32_rep_3")
#    plot.title <- c(plot_title)
#    x1 <- 1
#    x2 <- 2
#    xrange <- range(1:2)
#    yrange <- range(which_network_enhancer_expression)
#    plot.cols <- rainbow(nrow(which_network_enhancer_expression))
#    counter <- 1
#    legend.names <- vector()

#   Generate the plot
#    postscript(file=paste(output_dir,plot_filename,'.EPS', sep=""), onefile=FALSE, horizontal=FALSE, width=10, height = 8, paper="special", family="Times")
#        par(mar=c(8, 8, 4.1, 2.1), oma=c(3, 3, 3, 3))
#        plot(xrange, yrange, type="n", axes=FALSE, xlab=x.lab, ylab=y.lab, cex.lab=2, cex.axis=2, cex.main=3, font=2, main=plot.title)
#        axis(1, at=c(1:2), lab =c("Day_00", "Day_06"))
#        axis(2)
#        box()
#            for ( i in 1:nrow(which_network_enhancer_expression) ){
#                y1 <- which_network_enhancer_expression[i,day.00]
#                y2 <- which_network_enhancer_expression[i,day.06]
#                points(x1, mean(as.numeric(y1)), col=plot.cols[i])
#                points(x2, mean(as.numeric(y2)), col=plot.cols[i])
#                segments(x0=x1, y0=mean(as.numeric(y1)), x1=x2, y1=mean(as.numeric(y2)), col=plot.cols[i])
#                legend.names <- c(legend.names, rownames(which_network_enhancer_expression[i,]))
#            }
#        legend("topright", legend=legend.names, text.col=plot.cols[1:nrow(which_network_enhancer_expression)], ncol=4, cex=0.5)
#    dev.off()
#
#   Export tables of locations and promoters separately
#    plotted.enhancers <- subset(network_enhancer_promoter_correlated, enhancer %in% legend.names)
#    write.table(plotted.enhancers, file=paste(output_dir,table_filename,'_enhancers_plotted_promoter_correlations.txt', sep=""),  quote = FALSE, sep="\t")
#}
